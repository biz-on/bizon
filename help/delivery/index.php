<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
?><p>
</p>
<h2>Передвижение товаров внутри ТЯК Москва</h2>
<p>
	 Оптовая Миля бесплатно осуществляет перевозку товаров внутри ТЯК Москва. Мы привезем ваш заказ в любую точку назначения на территории ярмарочного комплекса. Вам будет достаточно дать точную информацию вашему менеджеру.
</p>
<h2>Отправка грузов по маршрутам дальнего следования</h2>
<p>
	 Из ТЯК Москва выезжают регулярные маршрутные грузовые рейсы по городам России. Часто это грузовые автобусы или фуры. Мы можем так же отправить ваш товар такими рейсами. Все что нужно - выбрать маршрут следования и указать это в заказе.
</p>
<p>
	 Список маршрутов... (название, маршруты следования, контакты)
</p>
<h2>Доставка Транспортной Компанией</h2>
<p>
	 На территории ТЯК Москва представлено множество транспортных компаний. Вы можете выбрать любую из них для перевозки ваших товаров. Мы так же бесплатно доставим ваши товары до точек сбора данных транспортных компаний.
</p>
<p>
	 Список компаний... (название, маршруты следования, контакты)
</p>
 <br>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>