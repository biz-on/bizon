<?
$aMenuLinks = [
    [
        "Главная",
        SITE_DIR."partner/",
        [],
        ['ICON_CLASS' => 'icon-home4'],
        ""
    ],
	[
		"Персональные данные",
        SITE_DIR."partner/personal/",
		[],
        [],
		""
	],
    [
        "Заказы",
        SITE_DIR."partner/orders/",
        [],
        [],
        ""
    ],
    [
        "Документы",
        SITE_DIR."partner/documents/",
        [],
        [],
        ""
    ]
];

if(\Bitrix\Main\Loader::includeModule('support')) {
    $aMenuLinks[] = [
        "Техническая поддержка",
        SITE_DIR."partner/support/",
        [],
        [],
        ""
    ];
}
?>