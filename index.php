<?php

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Хотите купить модную одежду оптом от производителя дёшево в Москве? В Оптовой Миле вещи с маркировкой в опт по низкой цене. Закупка со склада!");
$APPLICATION->SetPageProperty("title", "Маркированная одежда оптом: \"Оптовая Миля\" - цены от производителя в Москве купить");
$APPLICATION->SetTitle("Одежда, обувь оптом от производителя");

  $APPLICATION->IncludeComponent('sotbit:block.include','',['PART' => 'main_'.SITE_ID],null,['HIDE_ICONS' => 'Y']);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

