<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Нужно настроить работу под маркировку товаров Честный ЗНАК? GetMark это решение которое поможет вам упростить работу с маркированной одеждой и обувью.");
$APPLICATION->SetPageProperty("title", "Маркировка товаров на сайте Оптовая миля");
$APPLICATION->SetTitle("Маркировка товаров с GetMark");
?>

	<div class="content">
		


		<div class="welcome getmark cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/getmark_1.png" alt="getmark_1" >
			<h1><strong>Маркировка</strong> товаров быстро и надежно</h1>
			<p class="txt">Наш партнер GetMark поможет в вопросе  маркировки товаров. Гос.стандарт регистрации.</p>
		</div>
		
		<div class="getmark_logo">
			<div class="img">
				<img src="<?=SITE_TEMPLATE_PATH?>/i/getmark_2.png" alt="getmark_2" >
			</div>
			<div class="txt">
				<p class="title">GetMark — наш партнер-оператор маркировки для регистрации продукции в системе «Честный ЗНАК». </p>
				<p class="small">Подробности на сайте <a href="http://getmark.ru">getmark.ru</a></p>
			</div>
		</div>
		
		<div class="getmarkwhy">
			<h2>Для чего необходимо маркировать товары?</h2>
			<div class="flex">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/getmark_3.png" alt="getmark_3"  >
				</div>
				<div class="col">
				<p class="ttl">C 1 июля <br>2020 года</p>
				Запрещено производство, импорт, оптовая и розничная продажа немаркированной обуви 
				</div>
				<div class="col">
				<p class="ttl">C 1 января <br>2021 года</p>
				Будет запрещено производство, импорт, оптовая и розничная продажа немаркированной одежды 
				</div>
			</div>
		</div>
		
		<div class="getmark_video">
			<div class="txt">
				<strong>ГетМарк</strong> — общее решение для управления товарами и работы с кодами маркировки 
			</div>
			<div class="the_video">
				<iframe width="500" height="281" src="https://www.youtube.com/embed/-gaf40MUZD0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
			
		<div class="specialprice">
			<div class="img">
				<img src="<?=SITE_TEMPLATE_PATH?>/i/getmark_4.png" alt="getmark_4"  >
			</div>
			<div class="txt">
				<p>Для всех поставщиков ОптовойМили действует специальное предложение от ГетМарк - бесплатный полный доступ к продукту <strong>до 31 декабря 2020 года</strong></p>
				<p><strong>Условия участия</strong> - стать поставщиком ОптовойМили и отгрузить товар на склады</p>
			</div>
		</div>
		





<?
use Sotbit\Origami\Helper\Config;
global $APPLICATION, $settings;

CModule::IncludeModule('form');
$FORM_SID = "LIKEOFFER";
$rsForm = CForm::GetBySID($FORM_SID);
$arForm = $rsForm->Fetch();
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "sotbit_webform_1",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "",
        "AJAX_MODE" => 'Y',
        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => array(
            "RESULT_ID" => "RESULT_ID",
            "WEB_FORM_ID" => "WEB_FORM_ID"
        ),
        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
    )
);
?>	

</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>