<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
\Bitrix\Main\Loader::includeModule('sotbit.origami');
$aMenuLinks = Array(
	Array(
		"Доставка", 
		"about/delivery/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Оплата", 
		"about/payment/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Как оформить заказ", 
		"about/howtobuy/", 
		Array(), 
		Array(), 
		"" 
	),	
	Array(
		"Поставщикам", 
		"about/suppliers/", 
		// "/b2bcabinet/",
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Интернет-магазинам",
		"about/for_im/",
		Array(),
		Array(),
		""
	),
	Array(
		"Розничным магазинам",
        "about/for_rm/",
		Array(),
		Array(),
		""
	),
	//Array(
		//"Вопрос/Ответ",
        // \Sotbit\Origami\Helper\Config::get('PERSONAL_PAGE'),
		//"about/qa/",
		//Array(),
		//Array(),
		// "CUser::IsAuthorized()"
	//""
	//),
);
?>


