<?

//use Bitrix\Main\Page\Asset;

//Asset::getInstance()->addCss(SITE_DIR . "include/sotbit_origami/files/btn_error-share/style.css");
//Asset::getInstance()->addJs(SITE_DIR . "include/sotbit_origami/files/btn_error-share/script.js");
?>
<div class="btn_error-share__overlays-wrapper">
	<div class="btn_error-share">
		<div class="btn_error-share__wrapper">
			<div class="btn_error-share__content">
				<div class="btn_error-share__content-icons-wrapper">
					<div class="btn_error-share__icon-wrapper">
					</div>
					<div class="btn_error-share__icon-wrapper">
					</div>
				</div>
				<div class="btn_error-share__content-icon-close">
				</div>
			</div>
			<div class="btn_error-share__background-opacity">
			</div>
		</div>
		<div class="btn_error-share__share" onclick="callSubscribePopup('<span id=" title="Код PHP: &lt;?= SITE_DIR ?&gt;">
			 <?= SITE_DIR ?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP</span></span>', '<?= SITE_ID ?>' , this)" data-address="<?=$APPLICATION->GetCurPage();?>"&gt;
		</div>
		<div class="btn_error-share__error" onclick="foundError('<span id=" title="Код PHP: &lt;?= SITE_DIR ?&gt;">
			 <?= SITE_DIR ?><span class="bxhtmled-surrogate-inner"><span class="bxhtmled-right-side-item-icon"></span><span class="bxhtmled-comp-lable" unselectable="on" spellcheck="false">Код PHP</span></span>', '<?= SITE_ID ?>', this)" title="Нашли ошибку?"&gt;
		</div>
		 <script>
            (function () {
                initBtnErrorShare();
            })();
        </script>
	</div>
	<div class="btn_error-share__overlay overlay-black">
	</div>
	<div class="btn_error-share__overlay overlay-white">
	</div>
</div>
 <br>