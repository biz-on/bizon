<h1>Оптовая миля: оптовый B2B интернет-магазин</h1>
<div style="font-size: 14px;padding-bottom:18px;padding-top:14px;">
 <b>Оптовая миля</b>&nbsp;— это новый формат оптовой торговли в России.<br>
 <br>
	 Проект является&nbsp;стартапом&nbsp;по размещения одежды, обуви, аксессуаров и т.п в оборудованном шоуруме для оптовых поставщиков. Без издержек на персонал, реклама и прочее для самих поставщиков.<br>
</div>
<div style="padding-bottom:5px;">
 <b>Ключевые особенности:</b>
</div>
<ul class="fonts__small_text" style="padding-left:15px;">
	<li>Без издержек на персонал<br>
 </li>
	<li>Просторное помещение</li>
	<li>Без издержек на рекламу</li>
	<li>Широкий территориальный охват проекта</li>
</ul>
 <a class="about__more" href="<?=SITE_DIR?>about/contacts/">Узнать больше <i class="fas fa-angle-right about__fas"></i></a>