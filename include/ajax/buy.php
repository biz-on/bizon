<?php
define("STOP_STATISTICS", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('sotbit.origami');
$moduleIncluded = false;
try {
    $moduleIncluded = \Bitrix\Main\Loader::includeModule('sotbit.origami');
} catch (\Bitrix\Main\LoaderException $e) {
}
$params = json_decode($params, true);

\Bitrix\Main\Loader::includeModule('catalog');

$Buy = new \Sotbit\Origami\Sale\Basket\Buy();
if (!$params['props']) {
    $params['props'] = [];
}


// print_r();

// $ar = array(28873, 28874, 28875);

foreach ($params['id'] as $product_id) {

$Buy->setId($product_id);

if ($params['props']) {
    $props = unserialize(base64_decode($params['props']));
}

if (!is_array($props)) {
    $props = [];
}


$arFilter = Array("IBLOCK_ID"=>26, "ID"=>$product_id);
$res = CIBlockElement::GetList(Array(), $arFilter);
if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // поля элемента
    //echo "<pre>";
    //var_dump($arFields["PREVIEW_PICTURE"]);
    //echo "</pre>";
                        
    $picture = (CFile::GetPath($arFields["PREVIEW_PICTURE"]));
    $arProps = $ob->GetProperties(); // свойства элемента
    $article = $arProps["ARTICLE"]["VALUE"];
   }


$props = ["NAME" => $arFields['NAME']];


$Buy->setProps($props);
$Buy->setPrice($params['price']);
if($params['qnt'] > 0)
    $Buy->setQnt($params['qnt']);
if ($params['action'] == 'add') {
    $result = $Buy->add();
} else {
    $result = $Buy->remove();
}

}

if (!$result) {
    echo json_encode(['STATUS' => 'ERROR','MESSAGE' => 'Error basket']);
}
else{
    echo json_encode(['STATUS' => 'OK']);
}