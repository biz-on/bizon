<?
$MESS['STAFF_TITLE'] = 'Сотрудники';
$MESS['STAFF_GROUP_TITLES'] = 'Заголовки';
$MESS['STAFF_GROUP_SETTINGS'] = 'Настройки';

$MESS['STAFF_FIELD_TITLE'] = 'Заголовок блока';
$MESS['STAFF_FIELD_TITLE_VALUE'] = 'Наши сотрудники';

$MESS["STAFF_FIELD_IBORD1"] = "Поле для первой сортировки";
$MESS["STAFF_FIELD_IBBY1"] = "Направление для первой сортировки";
$MESS["STAFF_FIELD_IBORD2"] = "Поле для второй сортировки";
$MESS["STAFF_FIELD_IBBY2"] = "Направление для второй сортировки";

$MESS["STAFF_DESC_FID"] = "ID";
$MESS["STAFF_DESC_FNAME"] = "Название";
$MESS["STAFF_DESC_FACT"] = "Дата начала активности";
$MESS["STAFF_DESC_FSORT"] = "Сортировка";
$MESS["STAFF_DESC_FTSAMP"] = "Дата последнего изменения";

$MESS["STAFF_DESC_ASC"] = "По возрастанию";
$MESS["STAFF_DESC_DESC"] = "По убыванию";
?>