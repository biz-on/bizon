<?
$MESS['ADDRESSES_TITLE'] = 'Адреса магазинов';
$MESS['ADDRESSES_GROUP_TITLES'] = 'Заголовки';
$MESS['ADDRESSES_GROUP_SETTINGS'] = 'Настройки';

$MESS['ADDRESSES_FIELD_TITLE'] = 'Заголовок блока';
$MESS['ADDRESSES_FIELD_TITLE_VALUE'] = 'Адреса магазинов';

$MESS['ADDRESSES_FIELD_CONTACTS_URL'] = 'Адрес страницы контактов';
$MESS['ADDRESSES_FIELD_CONTACTS_URL_VALUE'] = '/about/contacts/';
?>