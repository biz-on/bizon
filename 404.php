<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
define("HIDE_SIDEBAR", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

	<div class="content page_about"><!-- можно page_pay вынести в div class="page"  -->
			
		<div class="error">
			<div class="pic">
				<img src="<?=SITE_TEMPLATE_PATH?>/i/404.png" alt="404"  >
			</div>
			<div class="text">
				<h2>Ошибка 404!</h2>
				<p class="txt">Приносим свои извенения. Данная страница находится на реконструкции. Но у нас есть интересные страницы, на которых вы найдете необходимую информацию</p>
				<a href="/" class="btn bg_orange">Перейти на главную страницу</a>
			</div>
		</div>
				
	</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>