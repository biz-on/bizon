<?

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", 'Y');
define("NO_AGENT_STATISTIC",'Y');
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$APPLICATION->IncludeComponent(
    "my:test.synergiya",
    ".default",
    [
    	"IBLOCK_ID"   => 25,
        "ID_ELEMENTS" => [30270, 30272, 29129],
        "HL_ID"       => 9
    ],
);
