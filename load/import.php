<?
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", 'Y');
define("NO_AGENT_STATISTIC",'Y');
define("NO_AGENT_CHECK", true);
define("DisableEventsCheck", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


$query = new Bitrix\Main\Entity\Query(

    Bitrix\Iblock\ElementTable::getEntity()

);


$query->setSelect(array('ID', 'CODE', 'NAME', 'PREVIEW_TEXT'))
      ->setFilter(array('IBLOCK_ID' => 16))
      ->setOrder(array('ID' => 'ASC'))
      ->setLimit(10);

// echo $query->getQuery();


$result = $query->exec();

// выводим результат
while ($row = $result->fetch()) { 
	$str = explode("JCCatalogSection(" ,$row['PREVIEW_TEXT']);
	$finStr = substr($str[1],0,-2);
	$finStr = str_replace("'", '"', $finStr);

	//echo $finStr;
    $arJson = json_decode($finStr);

    print_r($arJson);
    //echo $row['PREVIEW_TEXT'];//print_r($array);
}

