<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$arParams['SITE'] = 'mi';
if ($arParams['SITE']) {
    if (strpos($arParams['SITE'], 'http') !== 0) {
        if (!empty($_SERVER['HTTPS'])
            && strtolower($_SERVER['HTTPS']) !== 'off'
        ) {
            $arParams['SITE'] = 'https://'.$arParams['SITE'];
        } else {
            $arParams['SITE'] = 'http://'.$arParams['SITE'];
        }
    }
}