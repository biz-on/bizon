<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Нужен определенный вид одежды, обуви и аксессуаров для покупки на Оптовой Миле? Это возможно! Оставьте заявку нашим менеджерам.");
$APPLICATION->SetPageProperty("title", "Страница заказа определенного товара на Оптовой Миле");
$APPLICATION->SetTitle("Запрос товара");
?>

	<div class="content">
		
		<div class="welcome photostd cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/ZT_ico0.svg" alt="photostd"  >
			
			<h1>Подберем для вас, <strong>интересующий товар</strong></h1>
			<p class="txt">На сайте представлен не весь ассортимент, товарная матрица постоянно пополняется. Даже если вы не нашли нужный вам товар, мы сможем найти для вас его по запросу.</p>
		</div>
		
		
		<div class="photostd_details">
			<div class="flex">
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/ZT_ico1.svg" alt="photostd_p1" ></div>
					Выслать нам фото нужных товаров и мы постараемся их найти
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/ZT_ico2.svg" alt="photostd_p2" ></div>
					Узнать точную стоимость товара
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/ZT_ico3.svg" alt="photostd_p3" ></div>
					Выслать фотографии напрямую в WhatsApp
				</div>
			</div>
		</div>

<?
use Sotbit\Origami\Helper\Config;
global $APPLICATION, $settings;

CModule::IncludeModule('form');
$FORM_SID = "LIKEOFFER";
$rsForm = CForm::GetBySID($FORM_SID);
$arForm = $rsForm->Fetch();
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "sotbit_webform_1",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "",
        "AJAX_MODE" => 'Y',
        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => array(
            "RESULT_ID" => "RESULT_ID",
            "WEB_FORM_ID" => "WEB_FORM_ID"
        ),
        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
    )
);
?>	

</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>