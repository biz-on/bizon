<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "На сайте оптовая \"Оптовая Миля\" выгодные условия оплаты за товар! Покупайте и оплачивайте любым удобным для вас способом. Подробнее об оплате на этой странице");
$APPLICATION->SetPageProperty("title", "Условия оплаты на сайте Оптовая Миля за товары");
$APPLICATION->SetTitle("Оплата");
?>

		<div class="content page_pay"><!-- можно page_pay вынести в div class="page"  -->
		
		
		<div class="welcome cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/payonline.png" alt="payonline"  >
			<h1><strong>Оплачивайте</strong> онлайн там, где вам удобно</h1>
			<p class="txt">Мы предоставляем варианты оплаты удобные для физических и юредических лиц.</p>
		</div>
		
		
		<div class="payby">
			<h2>Оплата он лайн</h2>
			<div id="payby_online" class="row nohide cl">
				<div class="col">
					<p class="aftertitle">Воспользуйтесь простой формой онлайн оплаты банковской картой с Альфа Банк. Для этого:</p>
					<img src="<?=SITE_TEMPLATE_PATH?>/i/payby_alfa.svg" alt="payby_alfa" >
				</div>
				
				<div class="col double">
					<div class="pay_steps alfa">
						<div class="item">
							<p class="num">01</p>
							В разделе оплаты выберите<br>оплату банковской картой
						</div>
						<div class="item">
							<p class="num">03</p>
							Введите данные и<br>завершите оплату
						</div>
						<div class="item">
							<p class="num">02</p>
							Вам откроется безопасная<br>страница оплаты
						</div>
					</div>
				</div>
			</div>
			
			<h2>Для физических лиц</h2>
			<p class="aftertitle">Чтобы сделать перевод нашему юридическому лицу с физического то алгоритм действий следующий:</p>
			
			<div class="payby_toggle">
				<a href="#payby_sber" class="active"><img src="<?=SITE_TEMPLATE_PATH?>/i/payby_sber.svg" alt="payby_sber" ></a>
				<a href="#payby_alfa"><img src="<?=SITE_TEMPLATE_PATH?>/i/payby_alfa.svg" alt="payby_alfa" ></a>
				<a href="#payby_tf"><img src="<?=SITE_TEMPLATE_PATH?>/i/payby_tf2.png" alt="payby_tf" ></a>
			</div>
			
			
			<div id="payby_sber" class="row cl">
				<div class="col">
					<p class="lbl">Оплата через приложение </p>
					<img src="<?=SITE_TEMPLATE_PATH?>/i/payby_sber.svg" alt="payby_sber" >
				</div>
				<div class="col double">
					<div class="pay_steps sber">
						<div class="item">
							<p class="num">01</p>
							Перейдите в раздел <br><strong>«Платежи и переводы»</strong>
						</div>
						<div class="item">
							<p class="num">04</p>
							Если вы все правильно ввели, то вы увидите наименование нашей организации <strong>ООО «Алтея-технолоджис»</strong>
						</div>
						<div class="item">
							<p class="num">02</p>
							Найдите вкладку <br><strong>«Платеж по реквизитам»</strong>
						</div>
						<div class="item">
							<p class="num">05</p>
							Укажите в назначении платежа номер вашего заказа, который можно найти в карточке товара
						</div>
						<div class="item">
							<p class="num">03</p>
							Введите наши реквизиты <br>
							ИНН: <strong>7704490322</strong><br>
							КПП:  <strong>770401001</strong> <br>
							Расч. счет: <strong>40702810302330003328</strong> <br>
							БИК Банка: <strong>044525593</strong>
						</div>
					</div>
				</div>
			</div><!-- row -->
			
			<div id="payby_alfa" class="row cl">
				<div class="col">
					<p class="lbl">Оплата через приложение </p>
					<img src="<?=SITE_TEMPLATE_PATH?>/i/payby_alfa.svg" alt="payby_alfa" >
				</div>
				<div class="col double">
					<div class="pay_steps alfa">
						<div class="item">
							<p class="num">01</p>
							Перейдите в раздел <br><strong>«Оплатить»</strong>
						</div>
						<div class="item">
							<p class="num">03</p>
							Введите наши реквизиты <br>
							ИНН: <strong>7704490322</strong><br>
							КПП:  <strong>770401001</strong> <br>
							Расч. счет: <strong>40702810302330003328</strong> <br>
							БИК Банка: <strong>044525593</strong>
						</div>
						<div class="item">
							<p class="num">02</p>
							Выберите <strong>«В другой банк»</strong>  и в всплывающем окне выберите <strong>«Перевод со счета на счет»</strong>	
						</div>
						<div class="item">
							<p class="num">04</p>
							Укажите в назначении платежа номер вашего заказа, который можно найти в карточке товара
						</div>
					</div>
				</div>
			</div><!-- row -->
			
			<div id="payby_tf" class="row cl">
				<div class="col">
					<p class="lbl">Оплата через приложение </p>
					<img src="<?=SITE_TEMPLATE_PATH?>/i/payby_tf.png" alt="payby_tf" >
				</div>
				<div class="col double">
					<div class="pay_steps tf">
						<div class="item">
							<p class="num">01</p>
							Перейдите в раздел <br><strong>«Платежи → Переводы»</strong>
						</div>
						<div class="item">
							<p class="num">03</p>
							Введите наши реквизиты <br>
							ИНН: <strong>7704490322</strong><br>
							КПП:  <strong>770401001</strong> <br>
							Расч. счет: <strong>40702810302330003328</strong> <br>
							БИК Банка: <strong>044525593</strong>
						</div>
						<div class="item">
							<p class="num">02</p>
							Выберите <br><strong>«По номеру счета»</strong>
						</div>
					</div>
				</div>
			</div><!-- row -->
			
			<h2>Для юридических лиц</h2>
			<div id="payby_firms" class="row nohide cl">
				<div class="col">
					<p class="aftertitle">Если вы представляете юридическое лицо, то следуйте инструкциям:</p>
				</div>
				
				<div class="col double">
					<div class="pay_steps firms">
						<div class="item">
							<p class="num">01</p>
							В разделе оплаты выберите<br><strong>«Выставить счет»</strong>
						</div>
						<div class="item">
							<p class="num">03</p>
							Завершите<br>оформление заказа
						</div>
						<div class="item">
							<p class="num">02</p>
							Заполните необходимые<br>поля с реквизитами
						</div>
						<div class="item">
							<p class="num">04</p>
							Оплатите счет, который<br>придет на указанный вами<br>электронный адрес
						</div>
					</div>
				</div>
			</div>
			
		</div>	
<?
use Sotbit\Origami\Helper\Config;
global $APPLICATION, $settings;

CModule::IncludeModule('form');
$FORM_SID = "LIKEOFFER";
$rsForm = CForm::GetBySID($FORM_SID);
$arForm = $rsForm->Fetch();
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "sotbit_webform_1",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "",
        "AJAX_MODE" => 'Y',
        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => array(
            "RESULT_ID" => "RESULT_ID",
            "WEB_FORM_ID" => "WEB_FORM_ID"
        ),
        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
    )
);
?>			

	</div>
	
	    <style>
    .thumb-wrap {
      position: relative;
      padding-bottom: 56.25%; /* задаёт высоту контейнера для 16:9 (если 4:3 — поставьте 75%) */
      height: 0;
      overflow: hidden;
    }
    .thumb-wrap iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      border-width: 0;
      outline-width: 0;
    }
    </style>
    <div class="thumb-wrap">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/wY0GeJLabew?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>  
			

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>