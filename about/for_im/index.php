<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Не знаете как расширить ассортимент вашего интернет-магазина, быстро и легко найти поставщика? Для онлайн торговли существует удобный маркетплейс \"Оптовая миля\", закупайте товары по лучшей цене!");
$APPLICATION->SetPageProperty("title", "Как расширить ассортимент интернет-магазинам найти поставщика");
$APPLICATION->SetTitle("Интернет-магазинам");
?>
<div class="content">
		
		<div class="welcome forim cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/for_im1.png" alt="for_im1" >
			
			<h1><strong>Расширьте</strong> ассортимент вашего интернет-магазина</h1>
			<p class="txt">На нашем маркетплейсе широкий выбор одежды, обуви и аксессуаров. Качественные фото товаров уже готовые к выгрузке на ваш сайт.</p>
		</div>
		
		
		<div class="features forim">
			<h2>Чем мы можем быть полезны<br>интернет-магазинам?</h2>
			<div class="flex">
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_feat1.png" alt=""  ></div>
					Мы привезем доставляем заказы по всей России, удобным для вас способом
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_feat2.png" alt=""  ></div>
					Все наши товары имеют сертификаты соответствия качеству
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_feat3.png" alt=""  ></div>
					Поддержка на каждом этапе сотрудничества в Telegram, Viber, Whatsapp
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_feat4.png" alt=""  ></div>
					Новинки появляются на нашем маркетплейсе каждую неделю
				</div>
			</div>
		</div>
		
		<div class="orangebor_3col forim">
			<p class="ttl"><strong>Преимущества</strong> работы<br>с маркетплейсом ОптоваяМиля</p>
			<p class="txt">Специально для вас, мы <br>совместили оффлайн и онлайн </p>
			
			<div class="flex">
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_info1.png" alt="" ></div>
					<p>Все товары промаркированны<br>в соответствии с законом от <br>1 июля 2020 года</p>
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_info2.png" alt="" ></div>
					<p>Экономия времени на<br>поиске товаров в офлайне</p>
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_info3.png" alt="" ></div>
					<p>Все товары фотографируются<br>в профиссиональной студии</p>
				</div>
			</div>
			
		</div>
		
			
			
		<div class="specialprice">
			<div class="img">
				<img src="<?=SITE_TEMPLATE_PATH?>/i/getmark_4.png" alt="getmark_4"  >
			</div>
			<div class="txt">
				<p>На первый заказ для вас будет действовать скидка 10%. Предложение действует <strong>до 31 декабря 2020 года</strong></p>
				<p><strong>Условия для получения скидки</strong> — зарегистрироваться на сайте, сделать заказ и оплатить его. </p>
			</div>
		</div>
		
		

		    <style>
    .thumb-wrap {
      position: relative;
      padding-bottom: 56.25%; /* задаёт высоту контейнера для 16:9 (если 4:3 — поставьте 75%) */
      height: 0;
      overflow: hidden;
    }
    .thumb-wrap iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      border-width: 0;
      outline-width: 0;
    }
    </style>
    <div class="thumb-wrap">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/wY0GeJLabew?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>  
				
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>