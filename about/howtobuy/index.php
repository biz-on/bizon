<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Хотите узнать как оформить заказ вещей на сайте \"Оптовая Миля\"? На данной странице вы можете узнать как заказать выбранный вами товар. Это легко!");
$APPLICATION->SetPageProperty("title", "Как оформить заказ на сайте \"Оптовая миля\"");
$APPLICATION->SetTitle("Как оформить заказ");
?>
	<div class="content">
		
		<div class="welcome faq cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy1.png" alt="howtobuy1" >
			
			<h1><strong>Оптовые</strong> закупки стали удобнее</h1>
			<p class="txt">Наш маркетплейс сочетает в себе широкий 
ассортимент одежды обуви и акссесуаров. 
Вместе с понятной логикой оформления заказа</p>
		</div>
		
		<div class="howtobuyorange">
			<p class="ttl"><strong>Оптовая Миля</strong> создала для вас маркетплейс с удобным интерфейсом</p>
			<p class="txt">С нами вы можете расширить свою товарную матрицу. На сайте представленно более 5000 SKU. Для совершения покупки, необходимо пройти несколько не сложных шагов</p>
			
		</div>
		
		<div class="howtobuy_list cl">
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">Советуем <a href="#"><strong>зарегистироваться</strong></a>, это займет не более 2 минут. Зарегистрированные пользователи могут получать выгодные предложения, отслеживать состояние заказа, просматривать историю заказов и не только</p>
			</div>
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">Заходите в раздел <strong>«Каталог»</strong>, выбирайте необходимый раздел и подраздел</p>
			</div>
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">В карточке товара выберите необходимый размерный ряд и количество. После нажмите на кнопку <strong>«В корзину»</strong>  </p>
			</div>
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">После выбора товаров и количества, они появится в корзине. Переходите 
в корзину</p>
			</div>
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">На странице с корзиной, вы сможете редактировать кол-во товара или отложить его в избранное</p>
			</div>
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">Нажмите на кнопку <strong>«Перейти к оформлению»</strong>. При необходимости отредактируйте личные данные, способ доставки, спосб оплаты. После нажмите завершите заказ</p>
			</div>
			<div class="item">
				<div class="img">
					<img src="<?=SITE_TEMPLATE_PATH?>/i/howtobuy_num.svg" alt="" >
				</div>
				<p class="txt">Поздравляем! Вас заказ сформирован. Менеджер свяжется с вами для уточнения деталей</p>
			</div>
		</div>
				
				
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>