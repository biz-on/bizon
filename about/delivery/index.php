<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Оптовая Миля доставляет вещи по всей России удобным способом и по выгодной цене! Узнайте все условия доставки на сайте Оптовая Миля.");
$APPLICATION->SetPageProperty("title", "Условия доставки вещей с сайта \"Оптовая Миля\"");
$APPLICATION->SetTitle("Доставка");
?>
<div class="content">
	
	<div class="welcome delivery cl">
		<img src="<?=SITE_TEMPLATE_PATH?>/i/delivery.png" alt="delivery" >
		
		<h1>Доставим груз <strong>удобным</strong> для вас способом</h1>
		<p class="txt">Ниже представлены варианты доставки. Мы постоянно расширяем зону доставки.</p>
	</div>
	
	<div class="tyak cl">
		<p class="ttl">Передвижение товаров внутри ТЯК Москва</p>
		<div class="txt">Оптовая Миля бесплатно осуществляет перевозку товаров внутри ТЯК Москва. </div>
		<div class="flex">
			<div class="col">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/delivery_tyak1.png" alt=""  ></div>
				Мы привезем ваш заказ в любую точку назначения на территории ярмарочного комплекса 
			</div>
			<div class="col">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/delivery_tyak2.png" alt=""  ></div>
				Вам будет достаточно оговорить с вашим менеджером, день, время и место доставки
			</div>
			<div class="col">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/delivery_tyak3.png" alt=""  ></div>
				Товар будет доставлен к вам, сэкономив время и силы на доставке самостоятельно
			</div>
		</div>
	</div>
	
	
	<div class="orangebor_3col">
		<p class="ttl">Отправка грузов по маршрутам дальнего следования</p>
		<p class="txt">Из ТЯК Москва выезжают регулярные <br>маршрутные грузовые рейсы по городам России</p>
		
		<div class="flex">
			<div class="col">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/deliv_inf1.png" alt="deliv_inf1" ></div>
				<p>Укажите маршрут<br>следования в заказе</p>
			</div>
			<div class="col">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/deliv_inf2.png" alt="deliv_inf2" ></div>
				<p>Отправляем грузовыми<br>автобусами или фурами</p>
			</div>
			<div class="col">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/deliv_inf3.png" alt="deliv_inf3" ></div>
				<p>Получите свой груз в<br>оговоренные день и время</p>
			</div>
		</div>
		
	</div>
	
	<div class="delivery_tk cl">
		<p class="ttl">Доставка Транспортной Компанией</p>
		<div class="txt">На территории ТЯК Москва представлено множество транспортных компаний. Вы можете выбрать любую из них для перевозки ваших товаров. Мы так же бесплатно доставим ваши товары до точек сбора заказов транспортных компаний.</div>
		
		<div class="tk_list">
			<div class="item">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/tk_sdek.png" alt="tk_sdek" ></div>
				<div class="area">
					Регионы доставки<br>
					<strong>Вся Россия</strong>
				</div>
				<span class="btn green" onclick="window.location.href='https://www.cdek.ru/ru/calculate';" >Узнать детали и стоимость</span>
			</div>
			<div class="item">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/tk_pek.png" alt="tk_pek" ></div>
				<div class="area">
					Регионы доставки<br>
					<strong>Вся Россия</strong>
				</div>
				<span class="btn blue" onclick="window.location.href='https://pecom.ru/'" >Узнать детали и стоимость</span>
			</div>
			<div class="item dell">
				<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/tk_dell.png" alt="tk_dell" ></div>
				<div class="area">
					Регионы доставки<br>
					<strong>Вся Россия</strong>
				</div>
				<span class="btn black" onclick="window.location.href='https://www.dellin.ru/requests/';" >Узнать детали и стоимость</span>
			</div>
		</div>
	</div>


<?
use Sotbit\Origami\Helper\Config;
global $APPLICATION, $settings;

CModule::IncludeModule('form');
$FORM_SID = "LIKEOFFER";
$rsForm = CForm::GetBySID($FORM_SID);
$arForm = $rsForm->Fetch();
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "sotbit_webform_1",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "",
        "AJAX_MODE" => 'Y',
        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => array(
            "RESULT_ID" => "RESULT_ID",
            "WEB_FORM_ID" => "WEB_FORM_ID"
        ),
        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
    )
);
?>				
</div>

    <style>
    .thumb-wrap {
      position: relative;
      padding-bottom: 56.25%; /* задаёт высоту контейнера для 16:9 (если 4:3 — поставьте 75%) */
      height: 0;
      overflow: hidden;
    }
    .thumb-wrap iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      border-width: 0;
      outline-width: 0;
    }
    </style>
    <div class="thumb-wrap">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/wY0GeJLabew?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>  

    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>