<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Не знаете как расширить ассортимент для розницы, быстро и легко найти поставщика? Для оффлайн торговли существует удобный маркетплейс \"Оптовая миля\", закупайте товары для розничных продаж в магазине по лучшей цене!");
$APPLICATION->SetPageProperty("title", "Как расширить ассортимент в рознице найти поставщика одежды и обуви");
$APPLICATION->SetTitle("Розничным магазинам");
?>

<div class="content">
		
		<div class="welcome for_rm cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm1.png" alt="for_im1" >
			
			<h1><strong>Закупки</strong> стали <br>ближе и удобнее</h1>
			<p class="txt">Чтобы получить доступ к широкому ассортименту 
товаров, необходимо только оформить заказ. 
Физическое присутствие не требуется</p>
		</div>
		
		
		<div class="features for_rm">
			<h2>Мы поможем вашему бизнесу<br>развиваться уверено</h2>
			<div class="flex">
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm_feat1.png" alt=""  ></div>
					Заказывайте товары не выходя из дома. Доставляем по всей России
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm_feat2.png" alt=""  ></div>
					В нашем маркетплейсе все товары имеют необходимые сертификаты качества
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm_feat3.png" alt=""  ></div>
					Поддержка на каждом этапе сотрудничества в Telegram, Viber, Whatsapp
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm_feat4.png" alt=""  ></div>
					Новинки появляются на нашем маркетплейсе каждую неделю
				</div>
			</div>
		</div>
		
		<div class="orangebor_3col for_rm">
			<p class="ttl"><strong>Преимущества</strong> работы<br>с маркетплейсом ОптоваяМиля</p>
			<p class="txt">Мы используем свой более чем пяти летний опыт в оффлайн продажах для улучшение маркетплейса</p>
			
			<div class="flex">
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm_info1.png" alt="" ></div>
					<p>Мы практики из оффлайн продаж и знаем тонкости работы розничных магазинов </p>
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_rm_info2.png" alt="" ></div>
					<p>Вам не нужно приезжать на рынок для отбора товара. Достаточно сделать заказ на нашем сайте</p>
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/for_im_info3.png" alt="" ></div>
					<p>Профессиональные фотографии товаров помогут с выбором</p>
				</div>
			</div>
			
		</div>
		
			
			
		<div class="specialprice">
			<div class="img">
				<img src="<?=SITE_TEMPLATE_PATH?>/i/getmark_4.png" alt="getmark_4"  >
			</div>
			<div class="txt">
				<p>На первый заказ для вас будет действовать скидка 10%. Предложение действует <strong>до 31 декабря 2020 года</strong></p>
				<p><strong>Условия для получения скидки</strong> — зарегистрироваться на сайте, сделать заказ и оплатить его. </p>
			</div>
		</div>
		
		<?
use Sotbit\Origami\Helper\Config;
global $APPLICATION, $settings;

CModule::IncludeModule('form');
$FORM_SID = "LIKEOFFER";
$rsForm = CForm::GetBySID($FORM_SID);
$arForm = $rsForm->Fetch();
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "sotbit_webform_1",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "",
        "AJAX_MODE" => 'Y',
        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => array(
            "RESULT_ID" => "RESULT_ID",
            "WEB_FORM_ID" => "WEB_FORM_ID"
        ),
        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
    )
);
?>
						
	</div>


    <style>
    .thumb-wrap {
      position: relative;
      padding-bottom: 56.25%; /* задаёт высоту контейнера для 16:9 (если 4:3 — поставьте 75%) */
      height: 0;
      overflow: hidden;
    }
    .thumb-wrap iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      border-width: 0;
      outline-width: 0;
    }
    </style>
    <div class="thumb-wrap">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/wY0GeJLabew?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>  
    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>