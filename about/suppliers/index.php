<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Как поставщику продать одежду на Оптовой Миле? Это легко! Оставьте заявку на нашем сайте на размещение. Контакты на этой странице.");
$APPLICATION->SetPageProperty("title", "Поставщику продать одежду и обувь на Оптовой Миле");
$APPLICATION->SetTitle("Поставщикам");
?>
<div class="content">
		
		<div class="welcome cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/ordermade.png" alt="ordermade"  >
			
			<h1><strong>Маркетплейс</strong> для Оптовых Поставщиков</h1>
			<p class="txt">Размещайте товары и продавайте оптом онлайн и офлайн</p>
<!-- 			<a class="btn bg_orange">Регистрация</a>
			<a class="btn bor_orange">Поддержка</a> -->
		</div>
		
		<div class="suppliers_features">
			<p>
				<img src="<?=SITE_TEMPLATE_PATH?>/i/feature1.png" alt="Уникальный формат" >
				<span>Уникальный формат</span>
			</p>
			<p>
				<img src="<?=SITE_TEMPLATE_PATH?>/i/feature2.png" alt="Готовая клиентская база" >
				<span>Готовая клиентская база</span>
			</p>
			<p>
				<img src="<?=SITE_TEMPLATE_PATH?>/i/feature3.png" alt="Без лишних затрат на продвижение" >
				<span>Без лишних затрат на продвижение</span>
			</p>
			<p>
				<img src="<?=SITE_TEMPLATE_PATH?>/i/feature4.png" alt="Прозрачные условия сотрудничества" >
				<span>Прозрачные условия сотрудничества</span>
			</p>
		</div>
		
		<div class="forsuppliers2">
			<div class="img_wrap"><img src="<?=SITE_TEMPLATE_PATH?>/i/mixbig.png" alt="mixbig"  ></div>
			<div class="text_wrap">
				<h2>«Оптовая миля» — это маркетплейс <strong>оптовой торговли</strong>. Размещайте свои товары и продавайте на самых выгодных условиях по всей стране</h2><!-- или p class=h2 -->
				
				<p class="txt">Разместите свою продукцию у нас в крупнейшем центре оптовой торговли страны "ТЯК Москва". Для вашей компании это возможность показать товар тысячам оптовым покупателям прямо на месте их физического присутствия и не платить аренду. В вашем распоряжении 1000 м2 шоурума, складские помещения, а в ближайшем будущем и интернет-магазин с доставкой по России, Беларуси и Казахстану.</p>
			</div>
			
		</div>
		
		<div class="suppliers_steps cl">
			<p class="h2">С нами ваши продажи повысятся</p><!-- / или h2 -->
			<div class="row">
				<div class="st1">
					<p><span>Разместите товары в онлайн магазине и получайте оптовые заказы со всей страны.</span></p>
				</div>
				<div class="st2">
					<p><span>Начните продавать<br>оптом онлайн</span></p>
				</div>
			</div>
			<div class="row">
				<div class="st3">
					<p><span>Разместите товары<br>в оф-лайн шоуруме.</span></p>
				</div>
				<div class="st4">
					<p><span>Покажите клиентам свои товары в оборудованном шоуруме на территории крупнейшего оптового центра ТЯК "Москва".</span></p>
				</div>
			</div>
		</div>
		
		<div class="suppliers_wedo">
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo1.svg" alt="wedo1" ></span>
				<p>Сделаем фотографии товаров профессиональной фотостудии</p>
			</div>
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo2.svg" alt="wedo1" ></span>
				<p>Выставим товар в нашем интернет-магазине optovayamilya.ru</p>
			</div>
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo3.svg" alt="wedo1" ></span>
				<p>Выставим товары в офлайн шоуруме в ТЯК Москва (м.Люблино)</p>
			</div>
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo4.svg" alt="wedo1" ></span>
				<p>Возьмем на себя все операционные расходы (перевозка, хранение, обслуживание)</p>
			</div>
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo5.svg" alt="wedo1" ></span>
				<p>Проведем маркировку любого товара</p>
			</div>
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo6.svg" alt="wedo1" ></span>
				<p>Организуем продажу</p>
			</div>
			<div>
				<span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo7.svg" alt="wedo1" ></span>
				<p>Выдадим всю необходимую бухгалтерию (у нас все по белому)</p>
			</div>
			
		</div>
			<?
				use Sotbit\Origami\Helper\Config;
				global $APPLICATION, $settings;

				CModule::IncludeModule('form');
				$FORM_SID = "LIKEOFFER";
				$rsForm = CForm::GetBySID($FORM_SID);
				$arForm = $rsForm->Fetch();
				$APPLICATION->IncludeComponent(
				    "bitrix:form.result.new",
				    "sotbit_webform_1",
				    array(
				        "CACHE_TIME" => "3600",
				        "CACHE_TYPE" => "A",
				        "CHAIN_ITEM_LINK" => "",
				        "CHAIN_ITEM_TEXT" => "",
				        "COMPOSITE_FRAME_MODE" => "A",
				        "COMPOSITE_FRAME_TYPE" => "AUTO",
				        "EDIT_URL" => "",
				        "AJAX_MODE" => 'Y',
				        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
				        "IGNORE_CUSTOM_TEMPLATE" => "N",
				        "LIST_URL" => "",
				        "SEF_MODE" => "N",
				        "SUCCESS_URL" => "",
				        "USE_EXTENDED_ERRORS" => "N",
				        "VARIABLE_ALIASES" => array(
				            "RESULT_ID" => "RESULT_ID",
				            "WEB_FORM_ID" => "WEB_FORM_ID"
				        ),
				        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
				    )
				);
			?>	
	</div>

	    <style>
    .thumb-wrap {
      position: relative;
      padding-bottom: 56.25%; /* задаёт высоту контейнера для 16:9 (если 4:3 — поставьте 75%) */
      height: 0;
      overflow: hidden;
    }
    .thumb-wrap iframe {
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      border-width: 0;
      outline-width: 0;
    }
    </style>
    <div class="thumb-wrap">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/wY0GeJLabew?autoplay=1&rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>  
    
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>