function moremenu_wrap_pos(){
	var lastElin1row = 0;
	$("#topmenu li").each(function(){if ($(this).position().top < 21) {lastElin1row = $(this).index();}});
	var lastElin1rowPosL = $("#topmenu li").eq(lastElin1row).find("a").position().left;
	var lastElin1rowWidth = $("#topmenu li").eq(lastElin1row).find("a").outerWidth();
	$(".moremenu_wrap").css("left",(lastElin1rowPosL+lastElin1rowWidth)+"px");
}


function stopScrolling (e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
}

window.onload = function() {
moremenu_wrap_pos();$(".moremenu_wrap").show();
};

$(document).ready(function() {
	
	if ($(window).width() > 1100) { 
		$("#cat_slider").slick({ //карусель с адаптивностью
			slidesToShow: 4, slidesToScroll: 1, infinite: false,dots: false,responsive: [
			{breakpoint: 1250,settings: { slidesToShow: 3}},
			{breakpoint: 1100,settings: "unslick"}]
		});
	}
	// $('.open_popup').click(function(e) {e.preventDefault(); 
	// 	var ppid = $(this).attr("data-popupid");
	// 	$('.popup').hide(); $('.popup_layout').show(); $('#'+ppid).show();//.css("top",$(window).scrollTop()+"px");
	// });	
	
	$(".prods_mini_slider").on('init', function() {
		$(this).find(".slick-list").css("height",$(this).outerHeight()+"px");
		$(this).find(".rest").css("display","none");
	});
	
	if ($(window).width() > 960) { 
		$(".prods_mini_slider").slick({ //карусель с адаптивностью
			slidesToShow: 7, slidesToScroll: 1, infinite: false,dots: false,responsive: [
			{breakpoint: 1400,settings: { slidesToShow: 6}},
			{breakpoint: 1238,settings: { slidesToShow: 5}},
			{breakpoint: 1138,settings: { slidesToShow: 4}},
			{breakpoint: 960,settings: "unslick"}]
		});
	} else {
		$(".prods_minislider_wr").not(".mob_grid").find(".prods_mini_slider a.item").css("height",$(".prods_mini_slider").outerHeight()+"px");
	}

	
	$('.phonemask').mask("+7 (999) 999-99-99");
	
	$("#topmenu_toggle").click(function(){
		$(".moremenu").toggle();
	});
	
	$("#show_mini_clbck").click(function(){
		$(this).hide();$("#mini_clbck").show();
	});
	
	$("#top_phone").hover(function(){$("#ddown_phones").show();}, function(){$("#ddown_phones").hide();});
	$("#catalog_wrapper").hover(function(){$("#dd_catalog").show();}, function(){$("#dd_catalog").hide();});
	
	$("#show_mob_menu").click(function(){
		$("#mmenu").addClass("shown");$("#mm_layout").fadeIn(300);
		$('#mm_layout').on('scroll mousewheel touchmove', stopScrolling);
		$('body').css('overflow', 'hidden');
	});
	$("#mm_close,#mm_layout").click(function(){
		$("#mmenu").removeClass("shown");$("#mm_layout").fadeOut(300);
		$('body').css('overflow', 'auto');
	});

	
	// ТОП ИКОНКИ
	if ($(window).width() < 751) {
		$("#htop .btn_wa").detach().prependTo("#mobiletop");
		$("#hmid .btn_login,#hmid .btn_cart,#hmid .btn_special_offers,#hmid .btn_fav").detach().appendTo("#mobiletop");
		$("#htop #topmenu, .mainmenu .btn_status,.mainmenu .btn_request,.mainmenu .btn_getmark,.mainmenu .btn_photo").detach().prependTo("#mmenu");
		$("#ddown_phones").detach().insertAfter("#mmenu .btn_photo");
	}else {
		$("#mobiletop .btn_special_offers,#mobiletop .btn_fav, #mobiletop .btn_cart,#mobiletop .btn_login").detach().appendTo("#hmid");
		$("#mobiletop .btn_wa").detach().appendTo("#htop");
		$("#mmenu #topmenu").detach().insertBefore(".moremenu_wrap");
		$("#mmenu .btn_status,#mmenu.btn_request,#mmenu .btn_getmark,#mmenu .btn_photo").detach().appendTo(".mainmenu");
	};
	
	 function getHiddenItems() {
		var isOneLine = true, topPosition = false, invisibleItems = $('<ul/>');
		$('#topmenu li').each(function () {
			var thisPosition = $(this).position();
			if (topPosition === false) { topPosition = thisPosition.top; } else if (thisPosition.top > topPosition) {
				invisibleItems.append($(this).clone());isOneLine = false;
			};
		});
		return isOneLine ? false : invisibleItems;
	};

	function checkMenu() {
		$('#topmenu_toggle').hide();
		var hiddenItems = getHiddenItems();
		if (!hiddenItems) {$('.moremenu').html('');} else {
			$('#topmenu_toggle').show(); hiddenItems = getHiddenItems();
			$('.moremenu').html('<ul>' + hiddenItems.html() + '</ul>');
		};
	};
	$('#topmenu_toggle').on('click', function (e) {
		e.preventDefault();
		$('#topmenu_toggle').toggleClass('moremenu_opened');
		$('.moremenu').toggleClass('active');
	});
	
	$(window).on('resize', checkMenu);
	checkMenu();
	
	
	$(".payby_toggle a").click(function(e){
		e.preventDefault();
		$(".payby_toggle a").removeClass("active"); $(this).addClass("active");
		$(".payby .row").hide(); $(".payby .row"+$(this).attr("href")).show();
		var apos =	(Math.ceil($(this).position().left)+$(".payby_toggle").scrollLeft()),//выравнивание активного таба
		halfwt = Math.ceil($(this).width()/2),halfw = Math.ceil($(window).width()/2), midpos = (halfw - halfwt);
		$(".payby_toggle").animate({scrollLeft: (apos-midpos)}, 100)
	});
	
	
	$(".main_cats_toggle a").click(function(e){
		e.preventDefault();
		$(".main_cats_toggle a").removeClass("active"); $(this).addClass("active");
		$(".main_cats_content .item").hide(); $(".main_cats_content .item"+$(this).attr("href")).show();
		var apos =	(Math.ceil($(this).position().left)+$(".main_cats_toggle").scrollLeft()),//выравнивание активного таба
		halfwt = Math.ceil($(this).width()/2),halfw = Math.ceil($(window).width()/2), midpos = (halfw - halfwt);
		$(".main_cats_toggle").animate({scrollLeft: (apos-midpos)}, 100)
	});
	
	$(".mychbx label:not('.disabled')").click( function() {
		$(this).toggleClass("active");
	});
	$("div.radios label").click(function(){
	    $(this).closest("div.radios").find("label").removeClass('active');
	    $(this).addClass('active');
	});
	
	
	$("div.popup_pays_toggle label").click(function(){
	    $(".popup_pays_content .block").removeClass("active");
	    $(".popup_pays_content .block#"+$(this).attr("data-open")).addClass("active");
	    
	});
	
	$(".show_filters_mob").click(function(){
		$(".ctlg_fltrs").show(); $(".popup_layout").show();
	});
	$("#show_sort_popup").click(function(){
		$(this).closest(".catalog_sort_mob").find(".sort_popup").show(); $(".popup_layout").show();
	});
	$(".sort_popup a").click(function(){
		$(this).closest(".sort_popup").hide();$(".popup_layout").hide();
	});
	
	$(".popup_layout").click(function(){
		$(".popup,.ctlg_fltrs").hide();$(this).hide();
	});
	$(".close_popup").click(function(){
		$(".popup,.ctlg_fltrs,.popup_layout").hide();
	});
	
	
	if ($(window).width() > 600) { //слайдер для галереии продукта
		$('.product_images_big').slick({ 
		  slidesToShow: 1,slidesToScroll: 1,arrows: false,fade: true,asNavFor: '.product_images_thumbs'
		});
		$('.product_images_thumbs').slick({slidesToShow: 10,slidesToScroll: 1,asNavFor: '.product_images_big',dots: false,focusOnSelect: true,vertical:true
		});
	} else {
		$('.product_images_big').slick({ 
		  slidesToShow: 1,slidesToScroll: 1,arrows: false,dots: true,
		});
	};
	
	$(".lk_biznes .lk_biznes_current").click(function(){
		$(this).parent().find(".dd").toggle();
		$(this).toggleClass("opened");
	});
	
	
	$(".lk_biznes_rest .addgroup").click(function(){
		$(this).parent().find(".groups_dd").toggleClass("opened");
	});

	$(".faq_content .item .qu").click(function(){
		$(this).closest(".item").toggleClass("active").find(".an").slideToggle();
	});

	// ввод кода из смс
	var codebody = $('#codeinputs');
	function goToNextInput(e) {
		var key = e.which,t = $(e.target), sib = t.next('input');
		if (key != 9 && (key < 48 || key > 57)) {e.preventDefault(); return false;}
		if (key === 9) {return true;}
		if (!sib || !sib.length) {}
		sib.select().focus();
	}
	function onKeyDown(e) {
		var key = e.which; if (key === 9 || (key >= 48 && key <= 57)) { return true;} e.preventDefault();return false;
	}
	function onFocus(e) {$(e.target).select();}
	codebody.on('keyup', 'input', goToNextInput);
	codebody.on('keydown', 'input', onKeyDown);
	codebody.on('click', 'input', onFocus);
	//-----------------
	
	
})	
	



$(window).resize(function() {
moremenu_wrap_pos();
	// ТОП ИКОНКИ
	if ($(window).width() < 751) {
		$("#htop .btn_wa").detach().prependTo("#mobiletop");
		$("#hmid .btn_login,#hmid .btn_cart,#hmid .btn_fav,#hmid .btn_special_offers").detach().appendTo("#mobiletop");$("#htop #topmenu, .mainmenu .btn_status,.mainmenu .btn_request,.mainmenu .btn_getmark,.mainmenu .btn_photo,#ddown_phones").detach().prependTo("#mmenu");
		$("#ddown_phones").detach().prependTo("#mmenu");
	}else {
		$("#mobiletop .btn_special_offers,#mobiletop .btn_cart,#mobiletop .btn_fav,#mobiletop .btn_login").detach().appendTo("#hmid");
		$("#mobiletop .btn_wa").detach().appendTo("#htop");
		$("#mmenu #topmenu").detach().insertBefore(".moremenu_wrap");
		$("#mmenu .btn_status,#mmenu .btn_request,#mmenu .btn_getmark,#mmenu .btn_photo").detach().appendTo(".mainmenu");
	};
	
		if ($(window).width() > 600) { //слайдер для галереии продукта
		$('.product_images_big').slick({ 
		  slidesToShow: 1,slidesToScroll: 1,arrows: false,fade: true,asNavFor: '.product_images_thumbs'
		});
		$('.product_images_thumbs').slick({slidesToShow: 10,slidesToScroll: 1,asNavFor: '.product_images_big',dots: false,focusOnSelect: true,vertical:true
		});
	} else {
		$('.product_images_big').slick({ 
		  slidesToShow: 1,slidesToScroll: 1,arrows: false,dots: true,
		});
	};
	

})
