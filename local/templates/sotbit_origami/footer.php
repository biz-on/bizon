<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Sotbit\Origami\Helper\Config;
use Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

$page = \SotbitOrigami::getCurrentPage();

if(!\SotbitOrigami::needShowFullWidth($page)) {
    if(\SotbitOrigami::needShowSide($page)) {
    ?>
        </div>
    </div>

    <?} else {
        ?>
        </div>
    </div>
        <?
    }
}
?>



<!-- footer -->
<?
include $_SERVER['DOCUMENT_ROOT'].'/'.\SotbitOrigami::footersDir.'/'.Config::get('FOOTER').'/content.php';
if (file_exists($_SERVER['DOCUMENT_ROOT'].'/'.\SotbitOrigami::footersDir.'/'
    .Config::get('FOOTER').'/style.css')
) {
    Asset::getInstance()->addCss(\SotbitOrigami::footersDir.'/'
        .Config::get('FOOTER').'/style.css');
}
?>

<?
//Schema org breadcrumb
if( \Bitrix\Main\Loader::includeModule('sotbit.schemaorg') && (strpos($APPLICATION->GetCurPage(), "bitrix") === false) ) {
    Sotbit\Schemaorg\EventHandlers::makeContent($APPLICATION->GetCurPage(false), 'breadcrumblist');

    $data = SchemaMain::getData();
    if($data) {
        foreach ($data as $k => &$dat) {
            if ($dat['@type'] == 'breadcrumblist') {
                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

                if(!empty($APPLICATION->arAdditionalChain)) {
                    $arChain = $APPLICATION->arAdditionalChain;
                    foreach ($arChain as $key => $item) {
                        unlink($dat['itemListElement'][$key]);
                        $dat['itemListElement'][$key]['@type'] = "ListItems";
                        $dat['itemListElement'][$key]['name'] = $item['TITLE'];
                        $dat['itemListElement'][$key]['item'] = $protocol . $_SERVER['SERVER_NAME'] . $item['LINK'];
                        $dat['itemListElement'][$key]['position'] = $key + 1;
                    }
                }

                SchemaMain::setData($data);
            }
        }
    }
}
?>

<!-- end footer -->


<!-- btn go top -->
<?$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => SITE_DIR."include/sotbit_origami/btn_go_top/btn_top.php"
    )
);?>



<!-- end btn go top -->

<?
if (Config::get('BASKET_TYPE') == 'origami_top_without_basket') {
    Asset::getInstance()->addJs(SITE_DIR . "local/react/sotbit/rightpanel.basket/dist/main.js");
    Asset::getInstance()->addCss(SITE_DIR . "local/react/sotbit/rightpanel.basket/dist/main.css");
}
?>

<div class="popup_layout"></div>
<div class="popup" id="p_auth">
               <? $APPLICATION->IncludeComponent(
                "smsc:auth",
                "",
                array(
                ),
                false
               );?>    

</div>


</body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving=true >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
   ym(73440262, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/73440262" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</html>
