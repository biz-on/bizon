var ajaxResponse = false;
var ajaxForSliderTracks = false;

function JCSmartFilter(ajaxURL, viewMode, params, isNewsHeader) {
    this.ajaxURL = ajaxURL;
    this.form = null;
    this.timer = null;
    this.cacheKey = '';
    this.cache = [];
    this.popups = [];
    this.viewMode = viewMode;
    this.inputValues = [];
    this.params = params;
    this.isNewHeader = isNewsHeader;

    if (params && params.SEF_SET_FILTER_URL) {
        this.bindUrlToButton('set_filter', params.SEF_SET_FILTER_URL);
        this.sef = true;
    }

    if (params && params.SEF_DEL_FILTER_URL) {
        this.bindUrlToButton('del_filter', params.SEF_DEL_FILTER_URL);
    }
}

JCSmartFilter.prototype.keyup = function (input) {
    if (!!this.timer) {
        clearTimeout(this.timer);
    }
    this.timer = setTimeout(BX.delegate(function () {
        this.reload(input);
    }, this), 100);
};

JCSmartFilter.prototype.click = function (checkbox) {
    if (!!this.timer) {
        clearTimeout(this.timer);
    }

    this.timer = setTimeout(BX.delegate(function () {
        this.reload(checkbox);
    }, this), 100);
};

JCSmartFilter.prototype.reload = function (input) {
    if (this.cacheKey !== '') {
        //Postprone backend query
        if (!!this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(BX.delegate(function () {
            this.reload(input);
        }, this), 100);
        return;
    }
    this.cacheKey = '|';

    this.position = BX.pos(input, true);
    this.form = BX.findParent(input, {'tag': 'form'});

    if (this.form) {
        const values = [];

        values[0] = {name: 'ajax', value: 'y'};
        this.gatherInputsValues(values, BX.findChildren(this.form, {'tag': new RegExp('^(input|select)$', 'i')}, true));

        this.inputValues = [];

        for (let prop in values) {
            if ((values[prop].name.indexOf("searchFilter") !== -1)
                || (values[prop].name.indexOf("brandFilter") !== -1)
                || values[prop].name.indexOf("arrFilter") !== -1) {

                let tempName = values[prop].name;

                if (values[prop].value !== 'Y'
                    && values[prop].value !== ''
                    && values[prop].name.indexOf("MIN") === -1
                    && values[prop].name.indexOf("MAX") === -1) {
                    tempName += '_' + values[prop].value;
                }

                this.inputValues[tempName] = values[prop].value;
            }
        }

        for (let i = 0; i < values.length; i++) {
            this.cacheKey += values[i].name + ':' + values[i].value + '|';
        }

        if (this.cache[this.cacheKey]) {
            this.curFilterinput = input;
            this.postHandler(this.cache[this.cacheKey], true);
        } else {
            if (this.sef) {
                document.querySelector(".set_filter").disabled = true;
            }

            this.curFilterinput = input;
            BX.ajax.loadJSON(
                this.ajaxURL,
                this.values2post(values),
                BX.delegate(this.postHandler, this)
            );

        }
    }
};

JCSmartFilter.prototype.updateItem = function (PID, arItem) {
    if (arItem.PROPERTY_TYPE === 'N' || arItem.PRICE) {

        const trackBar = (arItem.ENCODED_ID)
            ? window['trackBar' + arItem.ENCODED_ID]
            : window['trackBar' + PID];

        if (trackBar && arItem.VALUES) {
            if (arItem.VALUES.MIN) {
                const minVal = (arItem.VALUES.MIN.FILTERED_VALUE)
                    ? arItem.VALUES.MIN.FILTERED_VALUE
                    : arItem.VALUES.MIN.VALUE;

                trackBar.setMinFilteredValue(minVal);
            }

            if (arItem.VALUES.MAX) {
                const maxVal = (arItem.VALUES.MAX.FILTERED_VALUE)
                    ? arItem.VALUES.MAX.FILTERED_VALUE
                    : arItem.VALUES.MAX.VALUE;

                trackBar.setMaxFilteredValue(maxVal);
            }
        }

    } else if (arItem.VALUES) {
        for (let i in arItem.VALUES) {
            if (arItem.VALUES.hasOwnProperty(i)) {
                const value = arItem.VALUES[i];
                const control = BX(value.CONTROL_ID);

                if (!!control) {
                    let label = document.querySelector('[data-role="label_' + value.CONTROL_ID + '"]');

                    if (value.DISABLED) {
                        if (label) {
                            label.classList.add('disabled');
                        } else {
                            control.parentNode.classList.add('disabled');
                            control.parentNode.querySelector('input').disabled = true;
                        }
                    } else {
                        if (label) {
                            label.classList.remove('disabled');
                        } else {
                            control.parentNode.classList.remove('disabled');
                            control.parentNode.querySelector('input').disabled = false;
                        }
                    }

                    if (value.hasOwnProperty('ELEMENT_COUNT')) {
                        label = document.querySelector('[data-role="count_' + value.CONTROL_ID + '"]');
                        if (label)
                            label.innerHTML = value.ELEMENT_COUNT;
                    }
                }
            }
        }
    }
};

JCSmartFilter.prototype.postHandler = function (result, fromCache) {

    BX.closeWait();

    let hrefFILTER, url, curProp;
    const modef = BX('modef');
    const modef_num = BX('modef_num');

    if (!!result && !!result.ITEMS) {
        for (let popupId in this.popups) {
            if (this.popups.hasOwnProperty(popupId)) {
                try {
                    this.popups[popupId].destroy();
                } catch (e) {
                }
            }
        }
        this.popups = [];

        for (let PID in result.ITEMS) {
            if (result.ITEMS.hasOwnProperty(PID)) {
                this.updateItem(PID, result.ITEMS[PID]);
            }
        }

        if (!!modef && !!modef_num) {
            modef_num.innerHTML = result.ELEMENT_COUNT;
            hrefFILTER = BX.findChildren(modef, {tag: 'A', class: "set_filter"}, true); // !!!

            if (result.FILTER_URL && hrefFILTER) {
                hrefFILTER[0].href = BX.util.htmlspecialcharsback(result.FILTER_URL);
            }

            if (result.FILTER_AJAX_URL && result.COMPONENT_CONTAINER_ID) {
                BX.unbindAll(hrefFILTER[0]);
                BX.bind(hrefFILTER[0], 'click', function (e) {
                    url = BX.util.htmlspecialcharsback(result.FILTER_AJAX_URL);
                    BX.ajax.insertToNode(url, result.COMPONENT_CONTAINER_ID);
                    return BX.PreventDefault(e);
                });
            }

            if (result.INSTANT_RELOAD && result.COMPONENT_CONTAINER_ID) {
                url = BX.util.htmlspecialcharsback(result.FILTER_AJAX_URL);
                BX.ajax.insertToNode(url, result.COMPONENT_CONTAINER_ID);
            } else if (this.params.FILTER_MODE === "AJAX_MODE") {
                url = BX.util.htmlspecialcharsback(result.FILTER_AJAX_URL);
                url = BX.util.add_url_param(url, {'ajaxFilter': 'Y'});
                if (!this.isNewHeader) {
                    if (!window.matchMedia("(max-width: 991px)").matches) {
                        BX.ajax.insertToNode(url, "comp_catalog_content");
                    }
                } else {
                    if (window.innerWidth <= 1023) {
                        document.body.style.overflow = "";

                        if (modef.style.display === 'none') {
                            modef.style.display = 'flex';
                        }

                        if (this.viewMode === "VERTICAL") {
                            curProp = BX.findChild(
                                BX.findParent(this.curFilterinput, {'class': 'bx_filter_parameters_box'}),
                                {'class': 'bx_filter_container_modef'},
                                true,
                                false
                            );

                            if (curProp) {
                                curProp.appendChild(modef);
                            }
                        }

                        if (result.SEF_SET_FILTER_URL) {
                            this.bindUrlToButton('set_filter', result.SEF_SET_FILTER_URL);
                        }

                    } else {
                        document.body.style.overflow = "";
                        BX.ajax.insertToNode(url, "comp_catalog_content");
                    }

                }
            } else {

                if (modef.style.display === 'none') {
                    modef.style.display = 'flex';
                }

                if (this.viewMode === "VERTICAL") {
                    curProp = BX.findChild(
                        BX.findParent(this.curFilterinput, {'class': 'bx_filter_parameters_box'}),
                        {'class': 'bx_filter_container_modef'},
                        true,
                        false
                    );

                    if (curProp) {
                        curProp.append(modef);
                    }
                }

                if (result.SEF_SET_FILTER_URL) {
                    this.bindUrlToButton('set_filter', result.SEF_SET_FILTER_URL);
                }
            }
        }

        for (let codeProp in result.ITEMS) {
            const item = result.ITEMS[codeProp];

            for (let valueProp in item.VALUES) {
                if (this.inputValues.hasOwnProperty(item.VALUES[valueProp].CONTROL_ID)) {

                    this.inputValues[item.VALUES[valueProp].CONTROL_ID] = (valueProp !== "MIN" && valueProp !== "MAX")
                        ? item.VALUES[valueProp].VALUE
                        : BX.Currency.currencyFormat(item.VALUES[valueProp].HTML_VALUE, item.VALUES[valueProp].CURRENCY, true);
                }

            }
        }
    }

    if (this.sef) {
        const set_filter = document.querySelector(".set_filter");
        set_filter.disabled = false;
    }

    if (!fromCache && this.cacheKey !== '') {
        this.cache[this.cacheKey] = result;
    }

    this.cacheKey = '';

    printValues();

    if ((typeof mql !== 'undefined') && mql.matches) {
        window.sortFilterMainMenuItems();
        window.displayApplyNumber();
    }

};

JCSmartFilter.prototype.insertToNode = function (url, node) {
    node = BX(node);

    if (!!node) {
        let show = BX.showWait(node);

        return BX.ajax.get(url, function (data) {
            node.innerHTML = data;
            BX.closeWait(node, show);
        });
    }
};

JCSmartFilter.prototype.bindUrlToButton = function (buttonId, url) {
    const buttons = document.getElementsByName(buttonId);

    if (buttons) {
        for (let i = 0; i < buttons.length; ++i) {
            let proxy = function (j, func) {
                return function () {
                    return func(j);
                }
            };

            if (buttons[i].type === 'submit') {
                buttons[i].type = 'button';
            }

            BX.bind(buttons[i], 'click', proxy(url, function (url) {
                window.location.href = url;
                return false;
            }));
        }
    }
};

JCSmartFilter.prototype.gatherInputsValues = function (values, elements) {
    if (elements) {
        for (let i = 0; i < elements.length; i++) {
            let el = elements[i];

            if (el.disabled || !el.type)
                continue;

            switch (el.type.toLowerCase()) {
                case 'text':
                case 'textarea':
                case 'password':
                case 'hidden':
                case 'select-one':
                    if (el.value.length)
                        values[values.length] = {name: el.name, value: el.value};
                    break;
                case 'radio':
                case 'checkbox':
                    if (el.checked)
                        values[values.length] = {name: el.name, value: el.value};
                    break;
                case 'select-multiple':
                    for (let j = 0; j < el.options.length; j++) {
                        if (el.options[j].selected)
                            values[values.length] = {name: el.name, value: el.options[j].value};
                    }
                    break;
                default:
                    break;
            }
        }
    }
};

JCSmartFilter.prototype.values2post = function (values) {
    let post = [];
    let current = post;
    let i = 0;

    while (i < values.length) {
        let p = values[i].name.indexOf('[');

        if (p === -1) {
            current[values[i].name] = values[i].value;
            current = post;
            i++;
        } else {
            const name = values[i].name.substring(0, p);
            const rest = values[i].name.substring(p + 1);
            const pp = rest.indexOf(']');

            if (!current[name])
                current[name] = [];

            if (pp === -1) {
                //Error - not balanced brackets
                current = post;
                i++;
            } else if (pp === 0) {
                //No index specified - so take the next integer
                current = current[name];
                values[i].name = '' + current.length;
            } else {
                //Now index name becomes and name and we go deeper into the array
                current = current[name];
                values[i].name = rest.substring(0, pp) + rest.substring(pp + 1);
            }
        }
    }
    return post;
};

JCSmartFilter.prototype.hideFilterProps = function (element) {
    const obj = element.parentNode,
        filterBlock = obj.querySelector("[data-role='bx_filter_block']"),
        propAngle = obj.querySelector("[data-role='prop_angle']");

    if ((typeof mql !== 'undefined') && mql.matches) { // if filter is mobile

        const fb = $(filterBlock);
        const isCheckboxesWithPictures = fb.find(".checkboxes_with_pictures").length;

        if (isCheckboxesWithPictures)
            fb.find(".checkboxes_with_pictures span.bx-color-sl").each(function (i, elem) {
                $(this).after('<span class="color_value">' + $(this).find("span.bx-filter-btn-color-icon").attr("title") + '</span>');
            });

        fb.prepend('<div class="properties_block_title"><span>' + $(element).find("span.item_name").text() + '</span></div>').show("slide", {direction: "right"});

    } else {

        if (BX.hasClass(obj, "active")) {
            if (BX.easing) {
                new BX.easing({
                    duration: 300,
                    start: {opacity: 1, height: filterBlock.offsetHeight},
                    finish: {opacity: 0, height: 0},
                    step: function (state) {
                        filterBlock.style.opacity = state.opacity;
                        filterBlock.style.height = state.height + "px";
                    },
                    complete: function () {
                        filterBlock.setAttribute("style", "");
                        BX.removeClass(obj, "active");
                    }
                }).animate();
            } else {
                filterBlock.style.opacity = 0;
                filterBlock.style.height = 0 + "px";
                filterBlock.setAttribute("style", "");
                BX.removeClass(obj, "active");
            }

            propAngle.classList.add("fa-angle-down");
            propAngle.classList.remove("fa-angle-up");

        } else {
            filterBlock.style.display = "block";
            filterBlock.style.opacity = 0;
            filterBlock.style.height = "auto";

            const obj_children_height = filterBlock.offsetHeight;

            filterBlock.style.height = 0;

            if (BX.easing) {
                new BX.easing({
                    duration: 300,
                    start: {opacity: 0, height: 0},
                    finish: {opacity: 1, height: obj_children_height},
                    transition: BX.easing.transitions.quart,
                    step: function (state) {
                        filterBlock.style.opacity = state.opacity;
                        filterBlock.style.height = state.height + "px";
                    },
                    complete: function () {
                    }
                }).animate();
            } else {
                filterBlock.style.opacity = 1;
                filterBlock.style.height = obj_children_height + "px";
            }

            obj.classList.add("active");
            propAngle.classList.add("fa-angle-up");
            propAngle.classList.remove("fa-angle-down");


        }

    }
};

JCSmartFilter.prototype.showDropDownPopup = function (element, popupId) {
    const contentNode = element.querySelector('[data-role="dropdownContent"]');
    const dropDownWidth = element.clientWidth;

    this.popups["smartFilterDropDown" + popupId] = BX.PopupWindowManager.create("smartFilterDropDown" + popupId, element, {
        autoHide: true,
        offsetLeft: 0,
        offsetTop: 3,
        overlay: false,
        draggable: {restrict: true},
        closeByEsc: true,
        className: 'filter-drop-down-popup',
        content: BX.clone(contentNode)
    });

    this.popups["smartFilterDropDown" + popupId].show();

    this.popups.resizeId = "smartFilterDropDown" + popupId;

    resizeDropDown(this.popups.resizeId);

    function resizeDropDown(id) {
        this.id = id;

        setDropdownPosition();

        window.addEventListener("resize", () => {
            setDropdownPosition();
        });

        function setDropdownPosition() {
            if (document.getElementById(id)) {

                const thisDropDown = document.getElementById(id);

                thisDropDown.style.width = dropDownWidth + 'px';

                if (mql.matches) {
                    const clientHeight = document.querySelector(".bx_filter").clientHeight;

                    thisDropDown.style.position = "fixed";
                    thisDropDown.style.top = "110px";
                    thisDropDown.style.left = "21px";
                    thisDropDown.style.overflowY = "auto";

                    if (thisDropDown.clientHeight > (clientHeight - 110)) {
                        thisDropDown.style.height = "calc(100% - 143px)";
                    }

                }
            }
        }
    }
};

JCSmartFilter.prototype.selectDropDownItem = function (element, controlId) {
    this.keyup(BX(controlId));

    const wrapContainer = BX.findParent(BX(controlId), {className: "bx-filter-select-container"}, false);
    const currentOption = wrapContainer.querySelector('[data-role="currentOption"]');

    currentOption.innerHTML = element.innerHTML;
    BX.PopupWindowManager.getCurrentPopup().close();
};

BX.namespace("BX.Iblock.SmartFilter");
BX.Iblock.SmartFilter = (function () {
    /** @param {{
			leftSlider: string,
			rightSlider: string,
			tracker: string,
			trackerWrap: string,
			minInputId: string,
			maxInputId: string,
			minPrice: float|int|string,
			maxPrice: float|int|string,
			curMinPrice: float|int|string,
			curMaxPrice: float|int|string,
			fltMinPrice: float|int|string|null,
			fltMaxPrice: float|int|string|null,
			precision: int|null,
			colorUnavailableActive: string,
			colorAvailableActive: string,
			colorAvailableInactive: string
		}} arParams
     */
    const SmartFilter = function (arParams) {
        if (typeof arParams === 'object') {
            this.leftSlider = BX(arParams.leftSlider);
            this.rightSlider = BX(arParams.rightSlider);
            this.tracker = BX(arParams.tracker);
            this.trackerWrap = BX(arParams.trackerWrap);

            this.minInput = BX(arParams.minInputId);
            this.maxInput = BX(arParams.maxInputId);

            this.minPrice = parseFloat(arParams.minPrice);
            this.maxPrice = parseFloat(arParams.maxPrice);

            this.curMinPrice = parseFloat(arParams.curMinPrice);
            this.curMaxPrice = parseFloat(arParams.curMaxPrice);

            this.fltMinPrice = arParams.fltMinPrice
                ? parseFloat(arParams.fltMinPrice)
                : parseFloat(arParams.curMinPrice);

            this.fltMaxPrice = arParams.fltMaxPrice
                ? parseFloat(arParams.fltMaxPrice)
                : parseFloat(arParams.curMaxPrice);

            this.precision = arParams.precision || 0;

            this.priceDiff = this.maxPrice - this.minPrice;

            this.leftPercent = 0;
            this.rightPercent = 0;

            this.fltMinPercent = 0;
            this.fltMaxPercent = 0;

            this.colorUnavailableActive = BX(arParams.colorUnavailableActive);//gray
            this.colorAvailableActive = BX(arParams.colorAvailableActive);//blue
            this.colorAvailableInactive = BX(arParams.colorAvailableInactive);//light blue

            this.isTouch = false;

            this.init();

            this.mess = {};
            this.mess.price_from = arParams.price_filter_from;
            this.mess.price_to = arParams.price_filter_to;

            if ('ontouchstart' in document.documentElement) {
                this.isTouch = true;

                BX.bind(this.leftSlider, "touchstart", BX.proxy(function (event) {
                    this.onMoveLeftSlider(event)
                }, this));

                BX.bind(this.rightSlider, "touchstart", BX.proxy(function (event) {
                    this.onMoveRightSlider(event)
                }, this));
            } else {
                BX.bind(this.leftSlider, "mousedown", BX.proxy(function (event) {
                    this.onMoveLeftSlider(event)
                }, this));

                BX.bind(this.rightSlider, "mousedown", BX.proxy(function (event) {
                    this.onMoveRightSlider(event)
                }, this));
            }

            BX.bind(this.minInput, "keyup", BX.proxy(function (event) {
                this.onInputChange();
            }, this));

            BX.bind(this.maxInput, "keyup", BX.proxy(function (event) {
                this.onInputChange();
            }, this));
        }
    };

    SmartFilter.prototype.init = function () {
        let priceDiff;

        if (this.curMinPrice > this.minPrice) {
            priceDiff = this.curMinPrice - this.minPrice;
            this.leftPercent = (priceDiff * 100) / this.priceDiff;
            this.leftSlider.style.left = this.leftPercent + "%";
            this.colorUnavailableActive.style.left = this.leftPercent + "%";
        }

        this.setMinFilteredValue(this.fltMinPrice);

        if (this.curMaxPrice < this.maxPrice) {
            priceDiff = this.maxPrice - this.curMaxPrice;
            this.rightPercent = (priceDiff * 100) / this.priceDiff;
            this.rightSlider.style.right = this.rightPercent + "%";
            this.colorUnavailableActive.style.right = this.rightPercent + "%";
        }

        this.setMaxFilteredValue(this.fltMaxPrice);
    };

    SmartFilter.prototype.setMinFilteredValue = function (fltMinPrice) {
        this.fltMinPrice = parseFloat(fltMinPrice);

        if (this.fltMinPrice >= this.minPrice) {
            const priceDiff = this.fltMinPrice - this.minPrice;
            this.fltMinPercent = (priceDiff * 100) / this.priceDiff;

            if (this.colorAvailableActive) {
                this.colorAvailableActive.style.left = (this.leftPercent > this.fltMinPercent)
                    ? this.leftPercent + "%"
                    : this.fltMinPercent + "%";

                this.colorAvailableInactive.style.left = this.fltMinPercent + "%";
            }
        } else {
            this.colorAvailableActive.style.left = "0%";
            this.colorAvailableInactive.style.left = "0%";
        }
    };

    SmartFilter.prototype.setMaxFilteredValue = function (fltMaxPrice) {
        this.fltMaxPrice = parseFloat(fltMaxPrice);
        if (this.fltMaxPrice <= this.maxPrice) {
            const priceDiff = this.maxPrice - this.fltMaxPrice;

            this.fltMaxPercent = (priceDiff * 100) / this.priceDiff;

            if (this.colorAvailableActive) {

                this.colorAvailableActive.style.right = (this.rightPercent > this.fltMaxPercent)
                    ? this.rightPercent + "%"
                    : this.fltMaxPercent + "%";

                this.colorAvailableInactive.style.right = this.fltMaxPercent + "%";
            }
        } else {
            this.colorAvailableActive.style.right = "0%";
            this.colorAvailableInactive.style.right = "0%";
        }
    };

    SmartFilter.prototype.getXCoord = function (elem) {
        const box = elem.getBoundingClientRect();
        const body = document.body;
        const docElem = document.documentElement;

        const scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
        const clientLeft = docElem.clientLeft || body.clientLeft || 0;
        const left = box.left + scrollLeft - clientLeft;

        return Math.round(left);
    };

    SmartFilter.prototype.getPageX = function (e) {
        e = e || window.event;
        let pageX = null;

        if (this.isTouch && event.targetTouches[0] != null) {
            pageX = e.targetTouches[0].pageX;
        } else if (e.pageX != null) {
            pageX = e.pageX;
        } else if (e.clientX != null) {
            const html = document.documentElement;
            const body = document.body;

            pageX = e.clientX + (html.scrollLeft || body && body.scrollLeft || 0);
            pageX -= html.clientLeft || 0;
        }

        return pageX;
    };

    SmartFilter.prototype.recountMinPrice = function () {
        const newMinPrice = (this.minPrice + (this.priceDiff * this.leftPercent) / 100).toFixed(this.precision);

        this.minInput.value = (newMinPrice !== this.minPrice)
            ? newMinPrice
            : "";

        /** @global JCSmartFilter smartFilter */
        smartFilter.keyup(this.minInput);
    };

    SmartFilter.prototype.recountMaxPrice = function () {
        const newMaxPrice = (this.maxPrice - (this.priceDiff * this.rightPercent) / 100).toFixed(this.precision);

        this.maxInput.value = (newMaxPrice !== this.maxPrice)
            ? newMaxPrice
            : "";

        /** @global JCSmartFilter smartFilter */
        smartFilter.keyup(this.maxInput);
    };

    SmartFilter.prototype.onInputChange = function () {
        let priceDiff;

        if (this.minInput.value) {
            let leftInputValue = this.minInput.value;

            if (leftInputValue < this.minPrice) {
                leftInputValue = this.minPrice;
            }

            if (leftInputValue > this.maxPrice) {
                leftInputValue = this.maxPrice;
            }

            priceDiff = leftInputValue - this.minPrice;
            this.leftPercent = (priceDiff * 100) / this.priceDiff;

            this.makeLeftSliderMove(false);
        }

        if (this.maxInput.value) {
            let rightInputValue = this.maxInput.value;

            if (rightInputValue < this.minPrice) {
                rightInputValue = this.minPrice;
            }

            if (rightInputValue > this.maxPrice) {
                rightInputValue = this.maxPrice;
            }

            priceDiff = this.maxPrice - rightInputValue;
            this.rightPercent = (priceDiff * 100) / this.priceDiff;

            this.makeRightSliderMove(false);
        }
    };

    SmartFilter.prototype.makeLeftSliderMove = function (recountPrice) {
        recountPrice = (recountPrice !== false);

        this.leftSlider.style.left = this.leftPercent + "%";
        this.colorUnavailableActive.style.left = this.leftPercent + "%";

        let areBothSlidersMoving = false;

        if (this.leftPercent + this.rightPercent >= 100) {
            areBothSlidersMoving = true;
            this.rightPercent = 100 - this.leftPercent;
            this.rightSlider.style.right = this.rightPercent + "%";
            this.colorUnavailableActive.style.right = this.rightPercent + "%";
        }

        if (this.leftPercent >= this.fltMinPercent && this.leftPercent <= (100 - this.fltMaxPercent)) {
            this.colorAvailableActive.style.left = this.leftPercent + "%";
            if (areBothSlidersMoving) {
                this.colorAvailableActive.style.right = 100 - this.leftPercent + "%";
            }
        } else if (this.leftPercent <= this.fltMinPercent) {
            this.colorAvailableActive.style.left = this.fltMinPercent + "%";
            if (areBothSlidersMoving) {
                this.colorAvailableActive.style.right = 100 - this.fltMinPercent + "%";
            }
        } else if (this.leftPercent >= this.fltMaxPercent) {
            this.colorAvailableActive.style.left = 100 - this.fltMaxPercent + "%";
            if (areBothSlidersMoving) {
                this.colorAvailableActive.style.right = this.fltMaxPercent + "%";
            }
        }

        if (recountPrice) {
            this.recountMinPrice();

            if (areBothSlidersMoving) {
                this.recountMaxPrice();
            }
        }
    };

    SmartFilter.prototype.countNewLeft = function (event) {
        const pageX = this.getPageX(event);
        const trackerXCoord = this.getXCoord(this.trackerWrap);
        const rightEdge = this.trackerWrap.offsetWidth;
        const newLeft = pageX - trackerXCoord;

        if (newLeft < 0) {
            return 0;
        } else if (newLeft > rightEdge) {
            return rightEdge;
        }

        return newLeft;
    };

    SmartFilter.prototype.onMoveLeftSlider = function (e) {
        if (!this.isTouch) {
            this.leftSlider.ondragstart = () => false;
        }

        if (!this.isTouch) {
            document.onmousemove = BX.proxy(function (event) {
                this.leftPercent = ((this.countNewLeft(event) * 100) / this.trackerWrap.offsetWidth);
                this.makeLeftSliderMove();
            }, this);

            document.onmouseup = () => {
                document.onmousemove = document.onmouseup = null;
            };
        } else {
            document.ontouchmove = BX.proxy(function (event) {
                this.leftPercent = ((this.countNewLeft(event) * 100) / this.trackerWrap.offsetWidth);
                this.makeLeftSliderMove();
            }, this);

            document.ontouchend = () => {
                document.ontouchmove = document.touchend = null;
            };
        }

        return false;
    };

    SmartFilter.prototype.makeRightSliderMove = function (recountPrice) {
        recountPrice = (recountPrice !== false);

        this.rightSlider.style.right = this.rightPercent + "%";
        this.colorUnavailableActive.style.right = this.rightPercent + "%";

        let areBothSlidersMoving = false;

        if (this.leftPercent + this.rightPercent >= 100) {
            areBothSlidersMoving = true;
            this.leftPercent = 100 - this.rightPercent;
            this.leftSlider.style.left = this.leftPercent + "%";
            this.colorUnavailableActive.style.left = this.leftPercent + "%";
        }

        if ((100 - this.rightPercent) >= this.fltMinPercent && this.rightPercent >= this.fltMaxPercent) {
            this.colorAvailableActive.style.right = this.rightPercent + "%";
            if (areBothSlidersMoving) {
                this.colorAvailableActive.style.left = 100 - this.rightPercent + "%";
            }
        } else if (this.rightPercent <= this.fltMaxPercent) {
            this.colorAvailableActive.style.right = this.fltMaxPercent + "%";
            if (areBothSlidersMoving) {
                this.colorAvailableActive.style.left = 100 - this.fltMaxPercent + "%";
            }
        } else if ((100 - this.rightPercent) <= this.fltMinPercent) {
            this.colorAvailableActive.style.right = 100 - this.fltMinPercent + "%";
            if (areBothSlidersMoving) {
                this.colorAvailableActive.style.left = this.fltMinPercent + "%";
            }
        }

        if (recountPrice) {
            this.recountMaxPrice();
            if (areBothSlidersMoving)
                this.recountMinPrice();
        }
    };

    SmartFilter.prototype.onMoveRightSlider = function (e) {
        if (!this.isTouch) {
            this.rightSlider.ondragstart = function () {
                return false;
            };
        }

        if (!this.isTouch) {
            document.onmousemove = BX.proxy(function (event) {
                this.rightPercent = 100 - (((this.countNewLeft(event)) * 100) / (this.trackerWrap.offsetWidth));
                this.makeRightSliderMove();
            }, this);

            document.onmouseup = () => {
                document.onmousemove = document.onmouseup = null;
            };
        } else {
            document.ontouchmove = BX.proxy(function (event) {
                this.rightPercent = 100 - (((this.countNewLeft(event)) * 100) / (this.trackerWrap.offsetWidth));
                this.makeRightSliderMove();
            }, this);

            document.ontouchend = function () {
                document.ontouchmove = document.ontouchend = null;
            };
        }

        return false;
    };

    return SmartFilter;
})();

function collapseFilter() {
    if (!document.querySelector('.mobile_filter_form .bx_filter')) {
        if ($(".bx_filter .bx_filter__title").hasClass("active")) {
            $(".bx_filter .bx_filter_section > form").slideUp(700);
        } else {
            $(".bx_filter .bx_filter_section > form").slideDown(700);
        }

        setTimeout(function () {
            $('.bx_filter .bx_filter_section > div').toggleClass("active");
        }, 600);
    }
}

$(document).ready(function () {
    $(document).on("click", ".popup_result__close", function () {
        $(this).parent().css("display", "none");
    });
});

window.addEventListener('DOMContentLoaded', () => {
    if (document.getElementById('set_filter')) {
        let submitBtn = document.getElementById('set_filter');

        submitBtn.addEventListener('click', function () {
            let openAll = document.querySelectorAll('.bx_filter_parameters_open_all');

            for (let i = 0; i < openAll.length; i++) {
                openAllToggle(openAll[i]);
            }
        })
    }
});

function openAllToggle(element) {
    const checkedItems = [],
        visibleContent = element.parentNode.querySelector('.blank_ul_wrapper'),
        itemsFilter = $(element).siblings(".blank_ul_wrapper").find(".hidden_filter_props").find(".bx_filter_parameters_box_checkbox "),
        blockFilter = $(element).siblings(".blank_ul_wrapper");

    const itemsFilter1 = $(element).siblings(".blank_ul_wrapper").find(".hidden_filter_props").find(".radio_container ");

    $(element).closest(".bx_filter .bx_filter_block").css({'height': 'auto'});
    $(element).toggleClass("open");

    if ($(element).hasClass("open")) {
        setTimeout(() => {
            $(element).closest(".bx_filter_parameters_box_container").find(".hidden_filter_props").show(300);
        }, 10)
    } else {
        setTimeout(() => {
            $(element).closest(".bx_filter_parameters_box_container").find(".hidden_filter_props").hide(300);
        }, 10)
    }

    itemsFilter.each((i, item) => {
        item.dataset.temp = i;

        if (item.querySelector('input:not([type="text"])').checked) {
            const temp = item.cloneNode(true);

            temp.removeChild(temp.querySelector('input'));
            temp.querySelector('label').classList.add('active');
            checkedItems.push(temp);
        }
    });

    itemsFilter1.each((i, item) => {
        item.dataset.temp = i;

        if (item.querySelector('input:not([type="text"])').checked) {
            const temp = item.cloneNode(true);

            temp.querySelector('label').classList.add('active');
            checkedItems.push(temp);
        }
    });

    const activeBlock = document.createElement('div');

    activeBlock.classList.add('temp_visible');

    checkedItems.forEach(item => {
        item.addEventListener('click', function () {
            this.querySelector('label').classList.toggle('active');
        });

        activeBlock.append(item);
    });

    if ($(element).hasClass("open")) {
        if (visibleContent.querySelector('.temp_visible')) {
            itemsFilter.each((i, item) => {
                if (item.querySelector('input').checked) {
                    item.style.display = 'block';
                }
            });

            itemsFilter1.each((i, item) => {
                if (item.style.display === 'none') {
                    item.style.display = 'block';
                    item.querySelector('input[type=radio]').checked = true;
                }
            });

            setTimeout(() => {
                $(visibleContent.querySelector('.temp_visible')).hide(200);
            }, 100);

            setTimeout(() => {
                visibleContent.removeChild(visibleContent.querySelector('.temp_visible'));
            }, 300)
        }
    } else {
        itemsFilter.each((i, item) => {
            if (item.querySelector('input').checked) {
                item.style.display = 'none';
            }
        });

        itemsFilter1.each((i, item) => {
            if (item.querySelector('input').checked) {
                item.style.display = 'none';
            }
        });
        blockFilter.prepend(activeBlock);
    }
}

function searchfieldRefresh() {
    $(".bx_filter_parameters_box").each(function () {
        if ($(this).find(".find_property_value").length) {
            filterList($(this).find(".find_property_value"), $(this).find(".blank_ul_wrapper"));
        }
    });
}

function filterList(header, list) {
    $(header).change(function () {
        const filter = $(header).val().toLowerCase();

        if (filter) {
            const $matches = $(list).find('label').parent();
            const array = [];

            for (let i = 0; i < $matches.length; i++) {

                if ($matches[i].querySelector('a')) {
                    if ($matches[i]
                        .querySelector('a')
                        .textContent
                        .toLowerCase()
                        .includes(filter)) {
                        array.push($matches[i]);
                    }
                } else {
                    if ($matches[i]
                        .querySelector('label')
                        .textContent
                        .toLowerCase()
                        .includes(filter)) {
                        array.push($matches[i]);
                    }
                }
            }

            $('.bx_filter_parameters_box_checkbox', list).not(array).slideUp();

        } else {
            $(list).find(".bx_filter_parameters_box_checkbox").slideDown();
        }

        return false;

    }).keyup(function () {
        $(this).change();
    });
}

function printValues() {
    $(".bx_filter .bx_filter_parameters_box[data-propid]").each(function (index, elem) {
        const propId = $(this).data('propid');
        const arrValues = [];
        let strValues;

        for (let value in smartFilter.inputValues) {
            if (value.indexOf('_' + propId + '_') !== -1) {
                if (value.indexOf("MIN") === -1 && value.indexOf("MAX") === -1) {
                    arrValues.push(smartFilter.inputValues[value]);
                } else {
                    if (value.indexOf("MIN") !== -1)
                        arrValues["MIN"] = smartFilter.inputValues[value];
                    if (value.indexOf("MAX") !== -1)
                        arrValues["MAX"] = smartFilter.inputValues[value];
                }
            }
        }

        strValues = (arrValues.hasOwnProperty("MIN") || arrValues.hasOwnProperty("MAX"))
            ? priceFormatFromTo(arrValues["MIN"], arrValues["MAX"])
            : arrValues.join(", ");

        if (strValues.length > 31) {
            strValues = strValues.substr(0, 31) + "...";
        }

        if (strValues) {
            $(elem).find("span.selected_items").html(strValues);
        } else {
            $(elem).find("span.selected_items").empty();
        }
    });
}

function priceFormatFromTo(fromPrice = "", toPrice = "") {
    let price_from = '';
    let price_to = '';

    if (window['trackBar' + window['filterSmartKey']].mess.price_from !== undefined) {
        price_from = window['trackBar' + window['filterSmartKey']].mess.price_from;
    }

    if (window['trackBar' + window['filterSmartKey']].mess.price_to !== undefined) {
        price_to = window['trackBar' + window['filterSmartKey']].mess.price_to;
    }

    if (fromPrice) {
        fromPrice = price_from + " " + fromPrice;
    }

    if (toPrice) {
        toPrice = price_to + " " + toPrice;
    }

    return fromPrice + " " + toPrice;
}

$(document).ready(function () {
    const filters = document.querySelector('.bx_filter');
    const filterBlock = filters.querySelectorAll('.bx_filter_parameters_box');

    for (let n = 0; filterBlock.length > n; n++) {
        filterBlock[n].addEventListener('click', function () {
            setTimeout(setState, 2000);
        });

        const filterBtn = filterBlock[n].querySelector('.bx_filter_parameters_open_all');

        if (filterBtn) {
            setActiveState(filterBtn);
        }
    }
});

function setActiveState(element) {
    const checkedItems = [];
    const itemsFilter = $(element).siblings(".blank_ul_wrapper").find(".hidden_filter_props").find(".bx_filter_parameters_box_checkbox ");
    const blockFilter = $(element).siblings(".blank_ul_wrapper");

    itemsFilter.each((i, item) => {
        item.setAttribute('data-temp', i);
        if (item.querySelector('input:not([type="text"])').checked) {
            const temp = item.cloneNode(true);
            temp.removeChild(temp.querySelector('input'));
            temp.querySelector('label').classList.add('active');
            checkedItems.push(temp);
        }
    });

    const activeBlock = document.createElement('div');

    activeBlock.classList.add('temp_visible');

    for (let i = 0; checkedItems.length > i; i++) {
        checkedItems[i].addEventListener('click', function () {
            this.querySelector('label').classList.toggle('active');
        });
        activeBlock.appendChild(checkedItems[i]);
    }

    if ($(element).hasClass("open")) {
        if (visibleContent.querySelector('.temp_visible')) {
            itemsFilter.each(function (i, item) {
                if (item.querySelector('input').checked) {
                    item.style.display = 'block';
                }
            });

            setTimeout(function () {
                $('.temp_visible').hide(200);
            }, 100);

            setTimeout(function () {
                visibleContent.removeChild(visibleContent.querySelector('.temp_visible'));
            }, 300)
        }
    } else {

        itemsFilter.each(function (i, item) {
            if (item.querySelector('input').checked) {
                item.style.display = 'none';
            }
        });

        blockFilter.prepend(activeBlock);
    }
}

function setState() {
    const filter = document.querySelector('.bx_filter');
    const filterItems = filter.querySelectorAll('.bx_filter_parameters_box');
    const stateFilter = [];

    for (let i = 0; filterItems.length > i; i++) {
        const filterState = {};
        filterState.state = filterItems[i].getAttribute('class');
        filterState.dataPropid = filterItems[i].getAttribute('data-propid');
        stateFilter.push(filterState);
    }

    let stateFilterJson = JSON.stringify(stateFilter);

    window.sessionStorage.setItem('item', stateFilterJson);
}

function getState() {
    if (window.sessionStorage.getItem('item')) {
        const stateFilter = JSON.parse(window.sessionStorage.getItem('item'));
        const filter = document.querySelector('.bx_filter');

        for (let i = 0; stateFilter.length > i; i++) {
            const data = '[data-propid =' + '"' + stateFilter[i].dataPropid + '"' + ']';
            const classItem = stateFilter[i].state;
            const item = filter.querySelector(data);

            if (item) {
                item.removeAttribute('class');
                item.setAttribute('class', classItem);
            }

        }
    }
}
