<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $sotbitSeoMetaBottomDesc;
global $sotbitSeoMetaTopDesc;
global $sotbitSeoMetaAddDesc;
global $sotbitSeoMetaFile;
global $issetCondition;
global ${$arParams["FILTER_NAME"]};

$this->setFrameMode(true);
use Sotbit\Origami\Helper\Config;
$hoverClass = implode(" ", Config::getArray("HOVER_EFFECT"));
$lazyLoad = (Config::get('LAZY_LOAD') == "Y");
?>

<?
if(isset($sotbitSeoMetaFile))
{
    ?>
    <div class="catalog_content__canvas">
        <?=$sotbitSeoMetaFile?>
    </div>
    <?
}elseif($arResult["SECTION"]["DETAIL_PICTURE"]){
    ?>
    <?
}

?>
<style>
.seo-tags-bottom {
    display: inline-block;    
    margin-right: 15px;
}
</style>
<span>Товар состоит в следующих категориях: </span>

<?
  $obSection = CIBlockSection::GetList(array(), array("=ID" => $arParams['SECT_AR'])); // где $arSectionID собранный вами массив
while($arSection = $obSection->Fetch()) {
   $arResult["SECTIONS"][] = $arSection;
}

?>
<ul>

        <?if (count($arResult['SECTIONS']) > 0):?>

        <?
        foreach ($arResult['SECTIONS'] as $section)
        {

            ?><li class="seo-tags-bottom">
			<a href="<?=$section['SECTION_PAGE_URL'] ?>" title="<?=$section['NAME']?>" class="">

				<p class=""><?=$section['NAME']?></p>
			</a>
            </li>
            <?
        }
        ?>
        <?endif;?>

</ul>
