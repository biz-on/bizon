<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

global $sotbitSeoMetaBottomDesc;
global $sotbitSeoMetaTopDesc;
global $sotbitSeoMetaAddDesc;
global $sotbitSeoMetaFile;
global $issetCondition;
global ${$arParams["FILTER_NAME"]};

$this->setFrameMode(true);
use Sotbit\Origami\Helper\Config;
$hoverClass = implode(" ", Config::getArray("HOVER_EFFECT"));
$lazyLoad = (Config::get('LAZY_LOAD') == "Y");
?>

<?
if(isset($sotbitSeoMetaFile))
{
    ?>
    <div class="catalog_content__canvas">
        <?=$sotbitSeoMetaFile?>
    </div>
    <?
}elseif($arResult["SECTION"]["DETAIL_PICTURE"]){
    ?>
    <div class="catalog_content__canvas">
        <img class="catalog_content__canvas_img"
             src="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['SRC']?>"
             width="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['WIDTH']?>"
             height="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['HEIGHT']?>"
             alt="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['ALT']?>"
             title="<?=$arResult["SECTION"]["DETAIL_PICTURE"]['TITLE']?>"
        >
    </div>

    <?
}

?>



<div class="catalog_content__category_block JS-catalog_content__category_block">
	<div class="catalog_content__category">

   <?
    $navChain = CIBlockSection::GetNavChain(25, $arResult['SECTION']['ID']); 
    while ($arNav=$navChain->GetNext()) {?>

        <a href="<?=$arNav['SECTION_PAGE_URL'] ?>" title="<?=$arNav['NAME']?>" class="catalog_content__category_item JS-catalog_content__category_item <?=$hoverClass?>">
                       <p class="catalog_content__category_img_title fonts__middle_text"><?=$arNav['NAME']?></p> 
        </a>

    <?}?>

        <?if (count($arResult['SECTIONS']) > 0):?>

        <?
        foreach ($arResult['SECTIONS'] as $section)
        {
            $this->AddEditAction($section['ID'], $section['EDIT_LINK'], $strSectionEdit);
            $this->AddDeleteAction($section['ID'], $section['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

            if($lazyLoad)
            {
                $strLazyLoad = 'src="'.SITE_TEMPLATE_PATH.'/assets/img/loader_lazy.svg" data-src="'.$section['PICTURE']['SRC'].'"';
                $lazyClass = 'lazy';
            }else{
                $strLazyLoad = 'src="'.$section['PICTURE']['SRC'].'"';
                $lazyClass = '';
            }


            ?>
			<a href="<?=$section['SECTION_PAGE_URL'] ?>" title="<?=$section['NAME']?>" class="catalog_content__category_item JS-catalog_content__category_item <?=$hoverClass?>">

				<p class="catalog_content__category_img_title fonts__middle_text"><?=$section['NAME']?></p>
			</a>
            <?
        }
        ?>
        <?endif;?>

	</div>

</div>



