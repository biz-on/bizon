<?
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Выводить дату элемента";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Выводить изображение для анонса";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Выводить текст анонса";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Отображать панель соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_HIDE"] = "Не раскрывать панель соц. закладок по умолчанию";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"] = "Шаблон компонента панели соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"] = "Используемые соц. закладки и сети";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"] = "Логин для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"] = "Ключ для для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_MAIN_TEL"] = "Свойство для вывода телефона в карточку";
$MESS["T_IBLOCK_DESC_NEWS_MAIN_EMAIL"] = "Свойство для вывода e-mail в карточку";
$MESS["T_IBLOCK_DESC_NEWS_PROPERTIES_SOCIAL_NETWORK"] = "Свойства для вывода социальных сетей";
$MESS["T_IBLOCK_DESC_NEWS_PROPERTIES_DISPLAYED_ON_DETAIL"] = "Свойства отображаемые на детальной странице";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELDS"] = "Выводимые поля с профиля пользователя";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_NAME"] = "Имя";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_LAST_NAME"] = "Фамилия";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_SECOND_NAME"] = "Отчество";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_EMAIL"] = "E-mail адрес";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_PROFESSION"] = "Профессия";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_WWW"] = "WWW-страница";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_ICQ"] = "ICQ";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_GENDER"] = "Пол";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_BIRTHDAY"] = "Дата рождения";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_PHONE"] = "Телефон";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_FAX"] = "Факс";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_MOBILE"] = "Мобильный телефон";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_PAGER"] = "Пэйджер";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_STREET"] = "Улица, дом";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_MAILBOX"] = "Почтовый ящик";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_CITY"] = "Город";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_STATE"] = "Область / край";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_ZIP"] = "Индекс";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_COUNTRY"] = "Страна";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_PERSONAL_NOTES"] = "Дополнительные заметки";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_COMPANY"] = "Наименование компании";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_DEPARTMENT"] = "Департамент / Отдел";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_POSITION"] = "Должность";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_WWW"] = "WWW-страница";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_PHONE"] = "Рабочий телефон";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_FAX"] = "Рабочий факс";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_PAGER"] = "Рабочий пэйджер";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_STREET"] = "Рабочий адрес";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_MAILBOX"] = "Рабочий почтовый ящик";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_CITY"] = "Рабочий почтовый ящик";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_STATE"] = "Рабочий область / край";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_ZIP"] = "Рабочий индекс";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_COUNTRY"] = "Страна работы";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_PROFILE"] = "Направления деятельности";
$MESS["T_IBLOCK_DESC_NEWS_USER_DISPLAYED_FIELD_WORK_NOTES"] = "Рабочие дополнительные заметки";
?>