<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// $this->createFrame()->begin();
?>
<div class="page">
<div class="blocks-wrapper">

<div id="blocks_main_1d" class="site-builder__hide">
    <div class="block-wrapper " data-id="13" data-copyof="0">
        <div class="block-wrapper-inner    " style="padding: 0px 0px 0px 0px ; ">

<div class="content homepage" style="max-width:1440px"><!-- можно page_pay вынести в div class="page"  -->


    <div class="welcome cl" style="padding-bottom: 0px;padding-top: 0px; " >
        <img src="<?=SITE_TEMPLATE_PATH?>/i/indexpic.png" alt="ordermade"  >
        
        <h1 style="font-size: 40px; font-weight:800; z-index:10;"><strong>Одежда, обувь </strong> оптом<br>от <strong>производителя</strong></h1>
        <p class="txt">Покупайте оптом онлайн в B2B маркетплейсе «Оптовая Миля». Сравнивайте предложения поставщиков и закупайте на сервисе по лучшим ценам с доставкой по Москве, и России. Входящая в госрегулирование обувь и одежда продаётся оптом с маркировкой, и соответствует требованиям системы «Честный ЗНАК».</p>
    </div>


    <div class="cat_slider_wrap">
        <h2>Ассортимент вещей на сайте «Оптовая Миля»</h2>


        <div id="cat_slider">
            <div>
            <a href="/internet-magazin/muzhskaya-odezhda-optom/" class="item">
                <span>Мужская</span>
                <img src="/local/templates/sotbit_origami/i/cat_slider_1.jpg" alt="cat_slider_1"  >
            </a>
            </div>
            <div>
            <a href="/internet-magazin/zhenskaya-odezhda-optom/" class="item">
                <span>Женская</span>
                <img src="/local/templates/sotbit_origami/i/cat_slider_2.jpg" alt="cat_slider_1"  >
            </a>
            </div>
            <div>
            <a href="/internet-magazin/detskaya-odezhda-optom/" class="item">
                <span>Детская</span>
                <img src="/local/templates/sotbit_origami/i/cat_slider_3.jpg" alt="cat_slider_1"  >
            </a>
            </div>
            <div>
            <a href="/internet-magazin/obuv-optom/" class="item">
                <span>Обувь</span>
                <img src="/local/templates/sotbit_origami/i/cat_slider_4.jpg" alt="cat_slider_1"  >
            </a>
            </div>
            <div>
            <a href="/internet-magazin/aksessuary-optom/" class="item">
                <span>Аксессуары</span>
                <img src="/local/templates/sotbit_origami/i/cat_slider_5.jpg" alt="cat_slider_1"  >
            </a>
            </div>
        </div>

    </div>
    
    <h2>Детская одежда для мальчиков оптом</h2>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "origami_section_slider",
            Array(
                "IBLOCK_ID" => 25,
                "SECTION_ID" => 646,
                "CACHE_TYPE" => 'N', 
                "CACHE_TIME" => 36000000, 
                "CACHE_FILTER" => 'Y',
                "CACHE_GROUPS" => 'Y',
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "ELEMENT_SORT_FIELD" => "rand",
            )
        );
        ?>
    <h2>Детская одежда для девочек оптом</h2>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "origami_section_slider",
            Array(
                "IBLOCK_ID" => 25,
                "SECTION_ID" => 647,
                "CACHE_TYPE" => 'N', 
                "CACHE_TIME" => 36000000, 
                "CACHE_FILTER" => 'Y',
                "CACHE_GROUPS" => 'Y',
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "ELEMENT_SORT_FIELD" => "rand",
            )
        );
        ?>

    <h2>Женская одежда оптом</h2>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "origami_section_slider",
            Array(
                "IBLOCK_ID" => 25,
                "SECTION_ID" => 551,
                "CACHE_TYPE" => 'N', 
                "CACHE_TIME" => 36000000, 
                "CACHE_FILTER" => 'Y',
                "CACHE_GROUPS" => 'Y',
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "ELEMENT_SORT_FIELD" => "rand",
                // "ELEMENT_SORT_ORDER" => $sortOrder,
            )
        );
    ?>

    <h2>Мужская одежда оптом</h2>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "origami_section_slider",
            Array(
                "IBLOCK_ID" => 25,
                "SECTION_ID" => 591,
                "CACHE_TYPE" => 'N', 
                "CACHE_TIME" => 36000000, 
                "CACHE_FILTER" => 'Y',
                "CACHE_GROUPS" => 'Y',
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "ELEMENT_SORT_FIELD" => "rand",
                // "ELEMENT_SORT_ORDER" => $sortOrder,
            )
        );
    ?>

    <h2>Обувь оптом</h2>

        <?
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section",
            "origami_section_slider",
            Array(
                "IBLOCK_ID" => 25,
                "SECTION_ID" => 614,
                "CACHE_TYPE" => 'N', 
                "CACHE_TIME" => 36000000, 
                "CACHE_FILTER" => 'Y',
                "CACHE_GROUPS" => 'Y',
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "ELEMENT_SORT_FIELD" => "rand",
            )
        );
        ?>

</div>

    <div class="suppliers_wedo_short">
                <h2>С нами у вас есть простор для развития!</h2>
                <div class="suppliers_wedo">
                    <div>
                        <span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo2.svg" alt="wedo1"></span>
                        <p>Выставим товар в нашем интернет-магазине optovayamilya.ru</p>
                    </div>
                    <div>
                        <span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo5.svg" alt="wedo1"></span>
                        <p>Проведем маркировку<br>любого товара</p>
                    </div>
                    <div>
                        <span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo4.svg" alt="wedo1"></span>
                        <p>Возьмем на себя все операционные расходы (перевозка, хранение, обслуживание)</p>
                    </div>
                    <div>
                        <span><img src="<?=SITE_TEMPLATE_PATH?>/i/wedo7.svg" alt="wedo1"></span>
                        <p>Выдадим всю необходимую бухгалтерию (у нас все по белому)</p>
                    </div>
                </div>
    </div>



    <div class="forsuppliers2 ltr">
            <div class="text_wrap">
                <h2>«Оптовая миля» — это маркетплейс <strong>оптовой торговли</strong>.</h2> <p>Размещайте свои товары и продавайте на самых выгодных условиях по всей стране. Работаем с маркированными вещами системы «Честный ЗНАК».</p><!-- или p class=h2 -->
                
                <p class="txt">Разместите свою продукцию у нас в крупнейшем центре оптовой торговли страны "ТЯК Москва". Для вашей компании это возможность показать товар тысячам оптовым покупателям прямо на месте их физического присутствия и не платить аренду. В вашем распоряжении 1000 м2 шоурума, складские помещения, а в ближайшем будущем и интернет-магазин с доставкой по России, Беларуси и Казахстану.</p>
            </div>
            <div class="img_wrap"><img src="<?=SITE_TEMPLATE_PATH?>/i/mixbig2.png" alt="mixbig"  ></div>
            
    </div>



</div>
</div>
</div>
</div>
</div>
