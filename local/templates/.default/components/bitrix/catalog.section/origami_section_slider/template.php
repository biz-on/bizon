<style>
.raz-ryad td {
  padding:0 !important;
}
</style>
<div class="prods_minislider_wr">
    <div class="prods_mini_slider">   

    <?    
        if($arResult['ITEMS'])
        {
            ?><!-- items-container --><?
            foreach($arResult['ITEMS'] as $item)
            {

                if($item['PROPERTIES']['NAZVANIE_PREDMETA']['VALUE']) {  
                    $measure  = "пару"; $measures  = "пар";
                } else {
                    $measure  =  "шт."; $measures  = "шт.";
                }

                $arPropsRazmer = [];
                $total_qnt_upak = 0;

                foreach ($item["OFFERS"] as $key => $value) {
                    $arCacheRaz = [$value["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'] => $value["PROPERTIES"]["RAZMER_ODEZHDY"]['VALUE']];
                    $arPropsRazmer[] = $arCacheRaz;
                    $total_qnt_upak += $value["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'];
                }

                $table = true;

                if($total_qnt_upak<2) { 
                     $total_qnt_upak = $item["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'];
                     $table = false;
                 }

                if($item["PROPERTIES"]["QNTUPAK"]['VALUE'] > 0 ) {
                    $total_qnt_upak = $item["PROPERTIES"]["QNTUPAK"]['VALUE'];
                    $item['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']['VALUE'] = $item['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']['VALUE'] / $total_qnt_upak;
                }


                if($total_qnt_upak == 0 ) $total_qnt_upak = 1;    



                if($item['PREVIEW_PICTURE']['SRC']) {   
                        $imgSrc = $item['PREVIEW_PICTURE']['SRC'];
                } else {
                        $imgSrc = $item['OFFERS'][0]['PREVIEW_PICTURE']['SRC'];
                }
                $sezon = $item['PROPERTIES']['SEZON']['VALUE'][0];
                $pcmPrice  = round($item['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']['VALUE']);
                $bPrice  = $pcmPrice * $total_qnt_upak;
                $bPrice = number_format($bPrice, 0, '', ' ');
                $pcmPrice = number_format($pcmPrice, 0, '', ' ');
                $hID = $item['ID'] + 1;   


                $strLazyLoad = 'src="'.SITE_TEMPLATE_PATH.'/assets/img/loader_lazy.svg" data-src="'.$imgSrc.'" class="lazy"'
                ?>

                <div>
                <a class="item" href="<?=$item['DETAIL_PAGE_URL']?>-<?=$hID?>/">
                    <span class="addToFav"><svg><use xlink:href="/local/templates/sotbit_origami/i/sprite.svg#ico_heart"></use></svg></span>
                    
                    <span class="img"><img <?=$strLazyLoad?>  alt="<?=$item['NAME']?>" ></span>
                    <span class="text_w">
                        <span class="name"><?=$item['NAME']?></span>
                        <span class="cat"><?=$sezon?></span>
                        <span class="price_lbl">Цена</span> 
                        <span class="price_min">за <?=$measure?> <?=$pcmPrice?> ₽ </span>   
                        <span class="price_max">за набор <?=$bPrice?> ₽ / <?=$total_qnt_upak?> <?=$measures?></span>    
                        <span class="rest">
                            <span class="store">Размерный ряд:</span>
                            <?if($table) {?>
                                 <table class="table raz-ryad">
                                    <tr>
                                    <?foreach ($arPropsRazmer as $arRaz) {
                                        foreach ($arRaz as $pcm => $razmer) {
                                        ?>
                                        <td>
                                            <?=$razmer?>
                                        </td>
                                     <?}}?>
                                    </tr>
                                    <tr>
                                     <?foreach ($arPropsRazmer as $arRaz) {
                                        foreach ($arRaz as $pcm => $razmer) {
                                         ?>
                                        <td>
                                            <?=$pcm?>
                                        </td>
                                     <?}}?>
                                    </tr>
                                 </table>
                            <?} else {?>
                                <?=$arPropsRazmer[0][1]?>
                            <?}?>
<!--                             
                            <span class="store_count">10</span>
                            <span class="store_e">упаковок</span> -->
                            <span class="readon">Подробнее</span>
                        </span>
                    </span>
                </a>
                </div>        
        <?}?>
    <?}?> 


    </div>
</div>