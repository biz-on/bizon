<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
Loader::includeModule('sotbit.origami');

$tmp = array();
$tmpD = array();
$tmpPhoto = array();

$arItems_dubl = [];
$arItem_dubl  = [];

global $USER;

// if ($USER->isAdmin()) {
    // foreach($arResult['ITEMS'] as $key => $item) {

    
    //  if ($arResult['ITEMS'][$key]['PROPERTIES']['OPEN_SKU']['VALUE'] == '1') {    
    //     foreach ($arResult['ITEMS'][$key]['OFFERS'] as $okey => $ovalue) {
    //             $arItem_dubl = $arResult['ITEMS'][$key];
    //             $arItem_dubl['nOffer'] = $ovalue;
    //             $arItems_dubl[] = $arItem_dubl;  
    //         } 
    //     } else {
    //             $arItem_dubl = $item;
    //             $arItem_dubl['nOffer'] = $key;
    //             $arItem_dubl['SKU_OPEN'] = 1;
    //             $arItems_dubl[] = $arItem_dubl;         
    //            }

    // }


    // $arResult['ITEMS'] = $arItems_dubl;

    // foreach($arResult['ITEMS'] as $key => $item) {
    //      if ($arResult['ITEMS'][$key]['SKU_OPEN'] == 1) {

    //      } else {
    //         unset($arResult['ITEMS'][$key]['OFFERS']);
    //         $arResult['ITEMS'][$key]['OFFERS'][0] = $item['nOffer'];          
    //      }
    // }
          

 // }


if(count($arResult['ITEMS']) < 10) $APPLICATION->AddHeadString('<meta name="robots" content="noindex, follow" />');

if($arResult['ITEMS'])
{
    foreach($arResult['ITEMS'] as $j => $item)
    {
        $tmp[$item['ID']] = $item['PREVIEW_PICTURE'];
        $tmpD[$item['ID']] = $item['DETAIL_PICTURE'];

        if(isset($item["DISPLAY_PROPERTIES"]["MORE_PHOTO"]))
        {
            if(isset($item["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"]["ID"]))
                $tmpPhoto[$item['ID']][] = $item["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"];
            else $tmpPhoto[$item['ID']] = $item["DISPLAY_PROPERTIES"]["MORE_PHOTO"]["FILE_VALUE"];
        }

        if($item['OFFERS'])
        {
            foreach($item['OFFERS'] as $offer)
            {
                $tmp[$offer['ID']] = $offer['PREVIEW_PICTURE'];
                $tmpD[$offer['ID']] = $offer['DETAIL_PICTURE'];
                //$tmpPhoto[$offer['ID']] = $offer['MORE_PHOTO'];
            }
        }
    }
}

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if($arResult['ITEMS'])
{
    foreach ($arResult['ITEMS'] as $j => $item)
    {
        $arResult['ITEMS'][$j]['PREVIEW_PICTURE'] = $tmp[$item['ID']];
        $arResult['ITEMS'][$j]['DETAIL_PICTURE'] = $tmpD[$item['ID']];
        $arResult['ITEMS'][$j]['MORE_PHOTO'] = $tmpPhoto[$item['ID']];

        if ($item['JS_OFFERS'])
        {
            foreach ($item['JS_OFFERS'] as $i => $offer)
            {
                $arResult['ITEMS'][$j]['JS_OFFERS'][$i]['PREVIEW_PICTURE'] = $tmp[$offer['ID']];
                $arResult['ITEMS'][$j]['JS_OFFERS'][$i]['DETAIL_PICTURE'] = $tmpD[$offer['ID']];
            }
        }
    }
}

unset($tmp, $tmpD, $tmpPhoto);

$arResult["ALL_PRICES_NAMES"] = \SotbitOrigami::getAllNamePrices($arResult);