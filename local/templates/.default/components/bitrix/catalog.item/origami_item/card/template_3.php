<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Sotbit\Origami\Helper\Config;
use \Sotbit\Origami\Helper\Prop;

$labelProps = unserialize(Config::get('LABEL_PROPS'));
$arParams['LABEL_PROP'] = $labelProps;

if(!$arParams['LABEL_PROP'])
{
    $arParams['LABEL_PROP'] = [];
}


if ($haveOffers)
{
	$showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
	$showProductProps = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $item['OFFERS_PROPS_DISPLAY'];
	$showPropsBlock = $showDisplayProps || $showProductProps;
	$showSkuBlock = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP']);
}
else
{
	$showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
	$showProductProps = $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES']);
	$showPropsBlock = $showDisplayProps || $showProductProps;
	$showSkuBlock = false;
}



$arPromotions =  CCatalogDiscount::GetDiscount($item['ID'], $item['IBLOCK_ID']);
$i = 1;
$dbProductDiscounts = array();
foreach ($arPromotions as $itemDiscount) {
    $dbProductDiscounts[$i] = $itemDiscount;
    $i++;
}
$blockID = randString(8);
?>

<?global $USER?>

<style>
.colorcube {
  width: 0;
  height: 0;
  cursor:pointer;
}

.colorcube-wrap {
  border:1px solid lightgray;  
  margin-left: 5px;
  cursor:pointer;
}
</style>

<div class="product_card__inner product_card__inner--two <?=( $arParams['MOBILE_VIEW_MINIMAL'] == 'Y' ? "product_card__inner--two-min" : "" )?>">
    <div class="product_card__inner-wrapper">
        <a class="product-card-inner__stickers" href="<?=$item['DETAIL_PAGE_URL']?>" <?if($dbProductDiscounts): ?> data-timer="timerID_<?=$blockID?>" <?endif;?> >
            <?
            $frame = $this->createFrame()->begin();
            if($item['PROMOTION'])
            {
            ?>
                <span class="product-card-inner__sticker"><?=Loc::getMessage('PROMOTION')?></span>
            <?
            }
            if($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
            {
                ?>
                <span class="product-card-inner__sticker" id="<?=$itemIds['DSC_PERC']?>" <?if(!$price['PERCENT']):?>style="display:none"<?endif?>>-<?=$price['PERCENT']?>%</span>
                <?

            }
            $frame->end();
            if($item['PROPERTIES'] && $arParams['LABEL_PROP'])
            {
                foreach($arParams['LABEL_PROP'] as $label){
                    if(Prop::checkPropListYes($item['PROPERTIES'][$label])){
                        $color = '#00b02a';
                        if($item['PROPERTIES'][$label]['HINT']){
                            $color = $item['PROPERTIES'][$label]['HINT'];
                        }
                        ?>
                        <span class="product-card-inner__sticker" style="background:<?=$color?>">
                            <?=$item['PROPERTIES'][$label]['NAME']?>
                        </span>
                        <?
                    }
                }
            }
            ?>
        </a>
        
        <?if($USER->isAdmin()) {
             echo "<a href='/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=25&type=sotbit_origami_catalog&ID=".$item['ID']." target='blank'>Редактировать</a>";
             print_r($item['PROPERTIES']['CML2_TRAITS']['VALUE'][3]);
            }
        ?>
        
<!--         <div class="product-card-inner__icons">
            <?if($haveOffers || $actualItem['CAN_BUY']):?>
                <?if($arParams["SHOW_DELAY"] && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')):?>
                    <span class="product-card-inner__icon" data-entity="wish" id="<?=$itemIds['WISH_LINK']?>" <?if($haveOffers && $actualItem['CAN_BUY']):?>style="display: none;"<?endif;?>>
                        <svg width="16" height="16">
                            <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_favourite"></use>
                        </svg>
                    </span>
                <?endif;?>
            <?endif;?>
            <?
            if ($arParams["SHOW_COMPARE"] && $arParams['DISPLAY_COMPARE'] && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y'))
            {
            ?>
                <span class="product-card-inner__icon" data-entity="compare-checkbox" id="<?=$itemIds['COMPARE_LINK']?>">
                    <svg width="16" height="16">
                        <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_compare"></use>
                    </svg>
                </span>
                <?
            }
            ?>
        </div> -->
        <?
        if($morePhoto[0]['SRC'])
        {

            ?>
            <div class="product-card-inner__img-wrapper">

                <a
                        href="<?=$item['DETAIL_PAGE_URL']?>"
                        onclick=""
                        class="product-card-inner__img-link <?=$arParams["HOVER_EFFECT"]?>"
                        data-entity="image-wrapper"
                        title="<?=$productTitle?>"
                >
                    <img
                            <?=$strLazyLoad?>
                            alt="Оптом <?=$item['NAME']?>"
                            id="<?=$itemIds['PICT']?>"
                            title="<?=$imgTitle?>"
                    >
                    <?if($arResult['LAZY_LOAD']):?>
                    <span class="loader-lazy"></span> <!--LOADER_LAZY-->
                    <?endif;?>
                </a>
            </div>
            <?
        }
        ?>


        <div class="text_w">
                            <a href="<?=$item['DETAIL_PAGE_URL']?>" class="name" title="<?=$item['NAME']?>"><?=$item['NAME']?></a>
        </div>



        <?
        if ($arParams['USE_VOTE_RATING'] === 'Y')
        {
        ?>

            <?php
                // $APPLICATION->IncludeComponent(
                //     'bitrix:iblock.vote',
                //     'origami_stars',
                //     [
                //         'CUSTOM_SITE_ID' => null,
                //         'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                //         'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                //         'ELEMENT_ID' => $item['ID'],
                //         'ELEMENT_CODE' => '',
                //         'MAX_VOTE' => '5',
                //         'VOTE_NAMES' => ['1', '2', '3', '4', '5'],
                //         'SET_STATUS_404' => 'N',
                //         'DISPLAY_AS_RATING' => 'vote_avg',
                //         'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                //         'CACHE_TIME' => $arParams['CACHE_TIME'],
                //         'READ_ONLY' => 'Y'
                //     ],
                //     $component,
                //     ['HIDE_ICONS' => 'Y']
                // );
            ?>

        <?
        }
        ?>



        <?
        $allProductPrices = \Bitrix\Catalog\PriceTable::getList([
          "select" => ["*"],
          "filter" => [
               "=PRODUCT_ID" => $item['OFFERS'][0]["ID"],
          ],
           "order" => ["CATALOG_GROUP_ID" => "ASC"]
        ])->fetchAll();
        ?>
        <? 



        if($arResult['ITEM']['PROPERTIES']['NAZVANIE_PREDMETA']['VALUE']) {  
            $measure  = "пару"; $measures  = "пар";
        } else {
            $measure  =  "шт."; $measures  = "шт.";
        }

            $table = true;
            $total_qnt_upak = $arParams['TOTAL_QNT_SKU'];
            if($arParams['TOTAL_QNT_SKU']<2) { 
                 $total_qnt_upak = $arResult['ITEM']["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'];
                 $table = false;
            }

            if($arResult['ITEM']["PROPERTIES"]["QNTUPAK"]['VALUE'] > 0) {
                $total_qnt_upak = $arResult['ITEM']["PROPERTIES"]["QNTUPAK"]['VALUE'];
                $allProductPrices[0]['PRICE'] = $allProductPrices[0]['PRICE'] / $total_qnt_upak;
            }

            if($total_qnt_upak == 0 ) $total_qnt_upak = 1;   

            $pcmPrice  = round($allProductPrices[0]['PRICE']);
            $bPrice = round($allProductPrices[0]['PRICE'] * $total_qnt_upak);
            $bPrice = number_format($bPrice, 0, '', ' ');
            $pcmPrice = number_format($pcmPrice, 0, '', ' ');


        ?>




    <span class="price_min" style="font-size: 16px;line-height: 2rem" >Цена за <?=$measure?> <?=$pcmPrice;?> ₽</span>





<? ob_start();?>

<a href="<?=$item['DETAIL_PAGE_URL']?>" onclick="" title="<?=$productTitle?>" class="addToFav" style="display:flex">
    <svg><use xlink:href="/local/templates/sotbit_origami/i/sprite.svg#ico_heart"></use></svg>
</a>

<?if($item['PROPERTIES'][Config::get('ARTICUL')]['VALUE']):?>
                <p class="product-card-inner__vendor-code">
                    <?=$item['PROPERTIES'][Config::get('ARTICUL')]['NAME']?>:
                    <span class="product-card-inner__vendor-code-value">
                        <?=$item['PROPERTIES'][Config::get('ARTICUL')]['VALUE']?>
                    </span>
                </p>
<?endif;?>


           <span class="price_max" style="color:#6d6d6d">Цена за набор <?=$bPrice;?> ₽ / <?=$total_qnt_upak?> <?=$measures?></span>


                     <p class="product-card-inner__option-title">Состав набора:</p>
                     <?if($table) {?>
                         <table class="table raz-ryad">
                            <tr>
                            <?foreach ($arParams['RAZMERI'] as $arRaz) {
                                foreach ($arRaz as $pcm => $razmer) {
                                ?>
                                <td>
                                    <?=$razmer?>
                                </td>
                             <?}}?>
                            </tr>
                            <tr>
                             <?foreach ($arParams['RAZMERI'] as $arRaz) {
                                foreach ($arRaz as $pcm => $razmer) {
                                 ?>
                                <td>
                                    <?=$pcm?>
                                </td>
                             <?}}?>
                            </tr>
                         </table>
                          <?} else {?>
                                <?=$arParams['RAZMERI'][0][1]?>
                          <?}?>

        <form  method="post" class="product-card-inner__form"  id="check_offer_basket_<?=$item['ID']?>">
            <div class="product-card-inner__option" id="<?= $itemIds['PROP_DIV'] ?>">
                <?
                foreach ($arParams['SKU_PROPS'] as $code => $skuProperty)
                {
                    $propertyId = $skuProperty['ID'];
                    $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                    if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                    {
                        continue;
                    }

                    if ($code == Config::get('COLOR'))
                    {
                        ?>
                        <div class="product-card-inner__option-color">
                            <p class="product-card-inner__option-title">Цвета:</p>
                            <div class="product-card-inner__option-list" >
                                <?
                                foreach ($skuProperty['VALUES'] as $fkey => $value)
                                {
                                    if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                    {
                                        continue;
                                    }
                                    ?>
                                        <span class="product-card-inner__option-item-content" style="margin-right: 10px;">
                                            <?=mb_strtolower($value['NAME']); ?>
                                        </span>
                              
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <?
                    }
                  }
                ?>
            </div>

            <div class="product-card-inner__buttons-block" data-entity="buttons-block" >
                <div class="product-card-inner__buy">
                    <div id="<?=$itemIds['BASKET_ACTIONS']?>" style="width:90%">
                        <a href="<?=$item['DETAIL_PAGE_URL']?>">
                            <button id="about_prod" class="readon-cat" type="button">Подробнее
                            </button>
                        </a>
                    </div>
                </div>
            </div>
       </form>

<?
    $includeHTML = htmlentities(ob_get_contents());
    ob_end_clean();
?>
    
     <div class="product_card_info"></div>
     <span class="bottom-data" data-text="<?=$includeHTML?>"></span>

</div>

</div>

<?

$arOffersJson = [];

// foreach ($item['OFFERS'] as $val) {
//     $arOffersJson[] ='{"@type": "Offer", "url": "https://optovayamilya.ru'.$val['DETAIL_PAGE_URL'].'"}';
// }

$arOffersJsonStr = implode(",", $arOffersJson);

?>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Product",
  "url" : "https://optovayamilya.ru<?=$item['DETAIL_PAGE_URL']?>",
  "name": "<?=$item['NAME']?>",
  "description" : "Артикул: <?=$item['PROPERTIES'][Config::get('ARTICUL')]['VALUE']?>. Цена за набор <?=$bPrice;?> ₽ / <?=$total_qnt_upak?> <?=$measures?>", 
  "image": "https://optovayamilya.ru<?=$strImageSchema?>",
  "offers": {
    "@type": "Offer",
    "availability" : "https://schema.org/InStock",
    "price": "<?=round($bPrice);?>",
    "priceCurrency": "RUB",
    "url": "https://optovayamilya.ru<?=$item['DETAIL_PAGE_URL']?>"
  }
}
</script>


