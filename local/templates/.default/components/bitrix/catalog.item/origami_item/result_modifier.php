<?

use Sotbit\Origami\Helper\Config;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
try {
    \Bitrix\Main\Loader::includeModule('sotbit.origami');
} catch (\Bitrix\Main\LoaderException $e) {
    print_r($e->getMessage());
}

$showSkuBlock = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y';

if($showSkuBlock && (!isset($arResult['TEMPLATE']) || $arResult['TEMPLATE'] == "template_1" || $arResult['TEMPLATE'] == "template_2" || $arResult['TEMPLATE'] == "template_3" || $arResult['TYPE'] == "list"))
{
    if (!$arResult['ITEM']['MORE_PHOTO'] && $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE'])
    {
        $arResult['ITEM']['MORE_PHOTO'] = [];
        $rs = CFile::GetList(
            [],
            [
                '@ID' => implode(',',
                    $arResult['ITEM']['PROPERTIES']['MORE_PHOTO']['VALUE']),
            ]
        );
        while ($file = $rs->Fetch())
        {
            $file['SRC'] = CFile::GetFileSRC($file);
            $arResult['ITEM']['MORE_PHOTO'][] = $file;
        }
    }

    $arResult['ITEM'] = \SotbitOrigami::changeColorImages($arResult['ITEM'], 'preview');

    $Item = new \Sotbit\Origami\Image\Item();
    $arResult['ITEM'] = $Item->prepareImages($arResult['ITEM']);

    $color = \Sotbit\Origami\Helper\Color::getInstance(SITE_ID);
    $arParams = $color::changePropColorView($arResult, $arParams)['PARAMS'];
}else{
    $arResult['ITEM'] = \SotbitOrigami::changeColorImages($arResult['ITEM'], 'preview', false);
    $Item = new \Sotbit\Origami\Image\Item();
    $arResult['ITEM'] = $Item->prepareImages($arResult['ITEM']);
}

$offerID = $arResult["ITEM"]["OFFERS"][0]["ID"];

$arResult["ITEM"]['DETAIL_PAGE_URL'] = $arResult["ITEM"]['DETAIL_PAGE_URL']."-".$offerID."/";



if (in_array($arResult["ITEM"]["IBLOCK_SECTION_ID"], [514, 551, 591, 647, 646])) { // если одежда
 
    if ($arResult["ITEM"]["PROPERTIES"]["O_VERHNYAJA_ODEZHDA"]["VALUE"] != null) { 
        $arResult["ITEM"]["NAME"] = $arResult["ITEM"]["PROPERTIES"]["O_VERHNYAJA_ODEZHDA"]["VALUE"][0];
    } else {
        $arResult["ITEM"]["NAME"] = $arResult["ITEM"]["PROPERTIES"]["O_BELYE"]["VALUE"][0];
    }

    if ($arResult["ITEM"]["PROPERTIES"]["O_KOMPLEKTACIA"]["VALUE"] != null) { 

        $komplekt_separated = implode(", ", $arResult["ITEM"]["PROPERTIES"]["O_KOMPLEKTACIA"]["VALUE"]);
        $arResult["ITEM"]["NAME"].=" ".$komplekt_separated;

    }

    $colorOf = $arResult["ITEM"]["PROPERTIES"]["VARIANT_TSVETA"]["VALUE"];

    $resRaz = CIBlockSection::GetByID($arResult["ITEM"]["IBLOCK_SECTION_ID"]);
    $ar_resRaz = $resRaz->GetNext();

    $arResult["ITEM"]["NAME"].=" | ".$ar_resRaz['DESCRIPTION']." | ".$colorOf." #".$offerID;

}


$arSect = [ 614, 651, 652, 
            680, 679, //туфли, натуральные
            684, //вид женской обуви...
            688,
            690,
            686,
            685,
            689,
            687,
            683,
            682,
            681,
           ];

// $arSelectType  = [686]; //УБИРАЕМ НАЗВАНИЕ ПРЕДМЕТА

$arSectLevel_1 = [679, 690]; 
 
$arSelectSep   = [686, 681, 682, 683, 687, 689, 685, 684 ]; // УБИРАЕМ МЕПАРАТОР |

if (in_array($arResult["ITEM"]["IBLOCK_SECTION_ID"], $arSect)) { // если обувь
 

     if ($arResult["ITEM"]["PROPERTIES"]["NAZVANIE_PREDMETA"]["VALUE"] != null) { 
        $arResult["ITEM"]["NAME"] = $arResult["ITEM"]["PROPERTIES"]["NAZVANIE_PREDMETA"]["VALUE"];
     }

    if (in_array($arResult["ITEM"]["IBLOCK_SECTION_ID"], $arSelectType)) { $arResult["ITEM"]["NAME"] = '';}

    if (!in_array($arResult["ITEM"]["IBLOCK_SECTION_ID"], $arSectLevel_1)) {
        if ($arResult["ITEM"]["PROPERTIES"]["VID"]["VALUE"] != null) { 

            $komplekt_separated = implode(", ", $arResult["ITEM"]["PROPERTIES"]["VID"]["VALUE"]);         
            $arResult["ITEM"]["NAME"].=" | ".$komplekt_separated;

        }
    }

    $colorOf = $arResult["ITEM"]["PROPERTIES"]["VARIANT_TSVETA"]["VALUE"];

    $resRaz = CIBlockSection::GetByID($arResult["ITEM"]["IBLOCK_SECTION_ID"]);
    $ar_resRaz = $resRaz->GetNext();

    if($ar_resRaz['DESCRIPTION']) {
       $arResult["ITEM"]["NAME"].=" | ".$ar_resRaz['DESCRIPTION'];
    }


    if (in_array($arResult["ITEM"]["IBLOCK_SECTION_ID"], $arSelectSep)) {
      $arResult["ITEM"]["NAME"] = str_replace("|", "", $arResult["ITEM"]["NAME"]);
    }

    $arResult["ITEM"]["NAME"].=" | ".$colorOf." #".$offerID;

}







\SotbitOrigami::checkPriceDiscount($arResult['ITEM']);

if (Bitrix\Main\Loader::includeModule("sotbit.price")) {
    //$arResult['ITEM'] = SotbitPrice::ChangeMinPrice($arResult['ITEM']);
}
if (Bitrix\Main\Loader::includeModule("sotbit.regions")) {
    //$arResult['ITEM'] = \Sotbit\Regions\Sale\Price::change($arResult['ITEM']);
}
?>