<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Sotbit\Origami\Helper\Config;
use \Sotbit\Origami\Helper\Prop;

$labelProps = unserialize(Config::get('LABEL_PROPS'));
$arParams['LABEL_PROP'] = $labelProps;

if(!$arParams['LABEL_PROP'])
{
    $arParams['LABEL_PROP'] = [];
}

if ($haveOffers)
{
	$showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
	$showProductProps = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $item['OFFERS_PROPS_DISPLAY'];
	$showPropsBlock = $showDisplayProps || $showProductProps;
	$showSkuBlock = $arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && !empty($item['OFFERS_PROP']);
}
else
{
	$showDisplayProps = !empty($item['DISPLAY_PROPERTIES']);
	$showProductProps = $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES']);
	$showPropsBlock = $showDisplayProps || $showProductProps;
	$showSkuBlock = false;
}




//print_r($arResult['ITEM']['OFFERS'][0]['PROPERTIES']);
/// Без фото не показываем 

//if(is_array($item['OFFERS'][0]['PREVIEW_PICTURE'])) {


if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !empty($item['PRODUCT_PROPERTIES']))
{
	?>
	<div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
		<?
		if (!empty($item['PRODUCT_PROPERTIES_FILL']))
		{
			foreach ($item['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo)
			{
				?>
				<input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propID?>]"
				       value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
				<?
				unset($item['PRODUCT_PROPERTIES'][$propID]);
			}
		}

		if (!empty($item['PRODUCT_PROPERTIES']))
		{
			?>
			<table>
				<?
				foreach ($item['PRODUCT_PROPERTIES'] as $propID => $propInfo)
				{
					?>
					<tr>
						<td><?=$item['PROPERTIES'][$propID]['NAME']?></td>
						<td>
							<?
							if (
								$item['PROPERTIES'][$propID]['PROPERTY_TYPE'] === 'L'
								&& $item['PROPERTIES'][$propID]['LIST_TYPE'] === 'C'
							)
							{
								foreach ($propInfo['VALUES'] as $valueID => $value)
								{
									?>
									<label>
										<? $checked = $valueID === $propInfo['SELECTED'] ? 'checked' : ''; ?>
										<input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propID?>]"
										       value="<?=$valueID?>" <?=$checked?>>
										<?=$value?>
									</label>
									<br />
									<?
								}
							}
							else
							{
								?>
								<select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propID?>]">
									<?
									foreach ($propInfo['VALUES'] as $valueID => $value)
									{
										$selected = $valueID === $propInfo['SELECTED'] ? 'selected' : '';
										?>
										<option value="<?=$valueID?>" <?=$selected?>>
											<?=$value?>
										</option>
										<?
									}
									?>
								</select>
								<?
							}
							?>
						</td>
					</tr>
					<?
				}
				?>
			</table>
			<?
		}
		?>
	</div>
	<?
}
?>
<?

$arPromotions =  CCatalogDiscount::GetDiscount($item['ID'], $item['IBLOCK_ID']);
$i = 1;
$dbProductDiscounts = array();
foreach ($arPromotions as $itemDiscount) {
    $dbProductDiscounts[$i] = $itemDiscount;
    $i++;
}
$blockID = randString(8);
?>

<?global $USER?>
<div class="product_card__inner product_card__inner--two <?=( $arParams['MOBILE_VIEW_MINIMAL'] == 'Y' ? "product_card__inner--two-min" : "" )?>">
    <div class="product_card__inner-wrapper">
        <a class="product-card-inner__stickers" href="<?=$item['DETAIL_PAGE_URL']?>" <?if($dbProductDiscounts): ?> data-timer="timerID_<?=$blockID?>" <?endif;?> >
            <?
            $frame = $this->createFrame()->begin();
            if($item['PROMOTION'])
            {
            ?>
                <span class="product-card-inner__sticker"><?=Loc::getMessage('PROMOTION')?></span>
            <?
            }
            if($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
            {
                ?>
                <span class="product-card-inner__sticker" id="<?=$itemIds['DSC_PERC']?>" <?if(!$price['PERCENT']):?>style="display:none"<?endif?>>-<?=$price['PERCENT']?>%</span>
                <?

            }
            $frame->end();
            if($item['PROPERTIES'] && $arParams['LABEL_PROP'])
            {
                foreach($arParams['LABEL_PROP'] as $label){
                    if(Prop::checkPropListYes($item['PROPERTIES'][$label])){
                        $color = '#00b02a';
                        if($item['PROPERTIES'][$label]['HINT']){
                            $color = $item['PROPERTIES'][$label]['HINT'];
                        }
                        ?>
                        <span class="product-card-inner__sticker" style="background:<?=$color?>">
                            <?=$item['PROPERTIES'][$label]['NAME']?>
                        </span>
                        <?
                    }
                }
            }
            ?>
        </a>
        
        <?if($USER->isAdmin()) {
             echo "<a href='/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=25&type=sotbit_origami_catalog&ID=".$item['ID']." target='blank'>Редактировать</a>";
             print_r($item['PROPERTIES']['CML2_TRAITS']['VALUE'][3]);
            }
        ?>
        
        <div class="product-card-inner__icons">
            <?if($haveOffers || $actualItem['CAN_BUY']):?>
                <?if($arParams["SHOW_DELAY"] && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')):?>
                    <span class="product-card-inner__icon" data-entity="wish" id="<?=$itemIds['WISH_LINK']?>" <?if($haveOffers && $actualItem['CAN_BUY']):?>style="display: none;"<?endif;?>>
                        <svg width="16" height="16">
                            <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_favourite"></use>
                        </svg>
                    </span>
                <?endif;?>
            <?endif;?>
            <?
            if ($arParams["SHOW_COMPARE"] && $arParams['DISPLAY_COMPARE'] && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y'))
            {
            ?>
                <span class="product-card-inner__icon" data-entity="compare-checkbox" id="<?=$itemIds['COMPARE_LINK']?>">
                    <svg width="16" height="16">
                        <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_compare"></use>
                    </svg>
                </span>
                <?
            }
            ?>
        </div>
        <?
        if($morePhoto[0]['SRC'])
        {

            ?>
            <div class="product-card-inner__img-wrapper">
                <?if(Config::get('QUICK_VIEW') == 'Y'):?>
<!--                     <span class="product-card-inner__img-view" onclick="quickView('<?=$item['DETAIL_PAGE_URL']?>');return false;">
                        <?=Loc::getMessage('QUICK_PREVIEW')?>
                    </span> -->
                <?endif;?>
                <a
                        href="<?=$item['DETAIL_PAGE_URL']?>"
                        onclick=""
                        class="product-card-inner__img-link <?=$arParams["HOVER_EFFECT"]?>"
                        data-entity="image-wrapper"
                        title="<?=$productTitle?>"
                >
                    <img
                            <?=$strLazyLoad?>
                            alt="<?=$imgAlt?>"
                            id="<?=$itemIds['PICT']?>"
                            title="<?=$imgTitle?>"
                    >
                    <?if($arResult['LAZY_LOAD']):?>
                    <span class="loader-lazy"></span> <!--LOADER_LAZY-->
                    <?endif;?>
                </a>
            </div>
            <?
        }
        ?>
        <div class="ciname">
            <a href="<?=$item['DETAIL_PAGE_URL']?>" onclick="" title="<?=$productTitle?>" class="product-card-inner__title-link">
                <?=$item['NAME']?>
            </a>
        </div>
        <?
        if ($arParams['USE_VOTE_RATING'] === 'Y')
        {
        ?>





            <?php
                // $APPLICATION->IncludeComponent(
                //     'bitrix:iblock.vote',
                //     'origami_stars',
                //     [
                //         'CUSTOM_SITE_ID' => null,
                //         'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                //         'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                //         'ELEMENT_ID' => $item['ID'],
                //         'ELEMENT_CODE' => '',
                //         'MAX_VOTE' => '5',
                //         'VOTE_NAMES' => ['1', '2', '3', '4', '5'],
                //         'SET_STATUS_404' => 'N',
                //         'DISPLAY_AS_RATING' => 'vote_avg',
                //         'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                //         'CACHE_TIME' => $arParams['CACHE_TIME'],
                //         'READ_ONLY' => 'Y'
                //     ],
                //     $component,
                //     ['HIDE_ICONS' => 'Y']
                // );
            ?>

        <?
        }
        ?>
        <div class="product-card-inner__info">
            <?
            if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
            {
                if ($haveOffers)
                {
                    if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
                    {
                        ?>
                        <div
                            id="<?=$itemIds['QUANTITY_LIMIT']?>"
                            style="display: none;"
                            data-entity="quantity-limit-block">
                                <span data-entity="quantity-limit-value"></span>
                        </div>
                        <?
                    }
                }
                else
                {
                    if (
                        $measureRatio
                        && (float)$actualItem['CATALOG_QUANTITY'] > 0
                        && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y'
                        && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N'
                    )
                    {
                        if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
                        {
                            if ((float)$actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
                            {
                                ?>
                                <span class="product-card-inner__quantity product-card-inner__quantity--lot">
                                    <?=$arParams['MESS_RELATIVE_QUANTITY_MANY']?>
                                </span>
                                <?
                            }
                            else
                            {
                                ?>
                                <span class="product-card-inner__quantity product-card-inner__quantity--few">
                                    <?=$arParams['MESS_RELATIVE_QUANTITY_FEW']?>
                                </span>
                                <?
                            }
                        }
                        else
                        {
                            if($actualItem['CATALOG_QUANTITY'] > 0){
                                ?>
                                <span class="product-card-inner__quantity product-card-inner__quantity--lot">
                                    <?php
                                    echo $actualItem['CATALOG_QUANTITY'].' '.$actualItem['ITEM_MEASURE']['TITLE'];
                                    ?>
                                </span>
                                <?
                            }
                            else{
                                ?>
                                <span class="product-card-inner__quantity product-card-inner__quantity--none">
                                    <?php
                                    echo $actualItem['CATALOG_QUANTITY'].' '.$actualItem['ITEM_MEASURE']['TITLE'];
                                    ?>
                                </span>
                                <?
                            }
                        }
                    }
                }
            }
            if($item['PROPERTIES'][Config::get('ARTICUL')]['VALUE']):
            ?>
                <p class="product-card-inner__vendor-code">
                    <?=$item['PROPERTIES'][Config::get('ARTICUL')]['NAME']?>:
                    <span class="product-card-inner__vendor-code-value">
                        <?=$item['PROPERTIES'][Config::get('ARTICUL')]['VALUE']?>
                    </span>
                </p>
            <?
            endif;
            ?>

        </div>

		<?
		$allProductPrices = \Bitrix\Catalog\PriceTable::getList([
		  "select" => ["*"],
		  "filter" => [
		       "=PRODUCT_ID" => $item['OFFERS'][0]["ID"],
		  ],
		   "order" => ["CATALOG_GROUP_ID" => "ASC"]
		])->fetchAll();
		?>
	    <? if($arResult['ITEM']['OFFERS'][0]['PROPERTIES']['CML2_ATTRIBUTES']['DESCRIPTION'][1] == "Размер обуви") {  
	        $measure  = "пару"; $measures  = "пар";
	    } else {
	        $measure  =  "шт."; $measures  = "шт.";
	    }
        	$pcmPrice  = round($allProductPrices[0]['PRICE']);
        	$bPrice = round($allProductPrices[0]['PRICE'] * $arParams['TOTAL_QNT_SKU']);
        	$bPrice = number_format($bPrice, 0, '', ' ');
        	$pcmPrice = number_format($pcmPrice, 0, '', ' ');


        ?>

<!--          <span class="price_lbl">В упаковке <strong><?=$item['OFFERS'][0]['PROPERTIES']['KOLICHESTVO_V_UPAKOVKE']['VALUE'];?></strong> <?=$measures?>  </span> -->
        <div class="product-card-inner__title">

             <span class="price_min" style="font-size: 16px;line-height: 2rem" >Цена за <?=$measure?> <?=$pcmPrice;?> ₽</span>
                </br>
             <span class="price_max" style="color:#6d6d6d">Цена за набор <?=$bPrice;?> ₽ / <?=$arParams['TOTAL_QNT_SKU']?> шт.</span>
        </div>

                     <p class="product-card-inner__option-title">Состав набора:</p>
      <?//print_r($arParams['RAZMERI']);?>
         <table class="table raz-ryad">
            <tr>
            <?foreach ($arParams['RAZMERI'] as $arRaz) {
                foreach ($arRaz as $pcm => $razmer) {
                ?>
                <td>
                    <?=$razmer?>
                </td>
             <?}}?>
            </tr>
            <tr>
             <?foreach ($arParams['RAZMERI'] as $arRaz) {
                foreach ($arRaz as $pcm => $razmer) {
                 ?>
                <td>
                    <?=$pcm?>
                </td>
             <?}}?>
            </tr>
         </table>


<style>
.colorcube {
  width: 0;
  height: 0;
  cursor:pointer;
}

.colorcube-wrap {
  border:1px solid lightgray;  
  margin-left: 5px;
  cursor:pointer;
}

</style>

<?
    require("/home/bitrix/ext_www/optovayamilya.ru/include/colors.php");
?>

        <form  method="post" class="product-card-inner__form"  id="check_offer_basket_<?=$item['ID']?>">
            <div class="product-card-inner__option" id="<?= $itemIds['PROP_DIV'] ?>">
                <?
                foreach ($arParams['SKU_PROPS'] as $code => $skuProperty)
                {
                    $propertyId = $skuProperty['ID'];
                    $skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
                    if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
                    {
                        continue;
                    }

                    if ($code == Config::get('COLOR'))
                    {
                        ?>
                        <div class="product-card-inner__option-color">
                            <p class="product-card-inner__option-title">Цвета:</p>
                            <div class="product-card-inner__option-list" >
                                <?
                                foreach ($skuProperty['VALUES'] as $value)
                                {
                                    if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                    {
                                        continue;
                                    }
                                    ?>
                                        <span class="product-card-inner__option-item-content" >
                                            <?
                                             $colName = mb_strtolower($value['NAME']); 
                                            ?>
                                   

                                            <? if ($colorcube[$colName]) {
                                                $color1 = $colorcube[$colName];
                                                $colorString = "border-top: 10px solid $color1; border-right: 10px solid $color1; border-bottom: 10px solid $color1; border-left: 10px solid $color1"; 
                                               } else {
                                                $color1 = $colorcube1[$colName][0];
                                                $color2 = $colorcube1[$colName][1];
                                                $colorString = "border-top: 10px solid $color1; border-right: 10px solid $color1; border-bottom: 10px solid $color2; border-left: 10px solid $color2"; 
                                               }
                                           ?>
   <!--                                         <div class="colorcube-wrap">
                                               <div class="colorcube" style="<?=$colorString?>;" title="<?=$colName;?>">
                                               </div>
                                           </div> -->
                                           <!-- <img src="<?=$value['PICT']['SRC']?>" alt="<?=$skuProperty['NAME']?>: <?=$value['NAME']?>" title="<?=$skuProperty['NAME']?>:  --><?=$value['NAME']?>
                                           <!--  <input type="radio" name="color" id="<?=$item['ID']?>_<?=$propertyId ?>_<?=$value['ID'] ?>">  -->
                                        </span>
                              
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <?
                    }
                    else{
                        ?>
<!--                         <div class="product-card-inner__option-memory" >
                            <p class="product-card-inner__option-title">Размерный ряд:</p>
                            <div class="product-card-inner__option-list" >
                            <?
                            // print_r($skuProperty);
                            foreach ($skuProperty['VALUES'] as $value)
                            {
                                if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
                                {
                                    continue;
                                }
                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                                ?>
                                    <span class="product-card-inner__option-item-content">
                                        <span title="<?=$skuProperty['NAME']?>: <?=$value['NAME']?>"><?= $value['NAME'] ?></span>
                                
                                    </span>
                                  <?
                            }
                            ?>
                            </div>
                        </div> -->
                        <?
                    }
                }
                ?>
            </div>



            <?if($arParams["SHOW_BUY"] && (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')):?>
            <div class="product-card-inner__buttons-block" data-entity="buttons-block" >
                <div class="product-card-inner__buy">
                    <div id="<?=$itemIds['BASKET_ACTIONS']?>" style="width:90%" <?=($actualItem['CAN_BUY'] ? '' : 'style="display: none;"')?>>
                        <a href="<?=$item['DETAIL_PAGE_URL']?>">
                            <button id="about_prod" class="readon-cat" type="button">Подробнее
                                <?//=($arParams['ADD_TO_BASKET_ACTION'] === 'BUY' ? $arParams['MESS_BTN_BUY'] : $arParams['MESS_BTN_ADD_TO_BASKET'])?>
                            </button>
                        </a>
                    </div>
                </div>
                <?if ($actualItem['CAN_BUY'] == 'Y'):?>
                    <div class="product-card-inner__product-basket" style="display: none">
                        <a class="product-card-inner__product-basket-btn" href="<?=Config::get('BASKET_PAGE')?>">
                            <?=Loc::getMessage('PRODUCT_IN_BASKET')?>
                        </a>
                    </div>
                <?endif?>
                <?
                if ($showSubscribe)
                {
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.product.subscribe',
                        'origami_recommend',
                        array(
                            'PRODUCT_ID' => $item['ID'],
                            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                            'BUTTON_CLASS' => 'product-card-inner__subscribe',
                            'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                }
                ?>
            </div>
            <?else:?>
            <div class="product-card-inner__buttons-block product-mode-N" data-entity="buttons-block">
                <div class="product-card-inner__buttom-more" title="<?=Loc::getMessage('ITEM_MORE')?> <?=$item["NAME"]?>">
                    <a class="product-card-inner__buttom-more-btn" href="<?=$item['DETAIL_PAGE_URL']?>">
                        <?=Loc::getMessage('ITEM_MORE')?>
                    </a>
                </div>
            </div>
            <?endif;?>
        </form>
    </div>
    <?
    $boolViewTimer = array(
        'view' => false,
        'number' => 0
    );
    foreach ($dbProductDiscounts as $key => $discount) {
        if (isset($discount['ACTIVE_TO']) && !empty($discount['ACTIVE_TO']))
            $boolViewTimer = array(
                'view' => true,
                'number' => $key
            );

    }
    if ($boolViewTimer['view']) {
        if (Config::get('TIMER_PROMOTIONS') == 'Y') {
            $APPLICATION->IncludeComponent(
                "sotbit:origami.timer",
                "origami_default",
                array(
                    "COMPONENT_TEMPLATE" => "origami_default",
                    "ID" => $item["ID"],
                    "BLOCK_ID" => $blockID,
                    "ACTIVATE" => "Y",
                    "TIMER_SIZE" => "md",
                    "MOBILE_DESTROY" => $arParams['MOBILE_VIEW_MINIMAL'],
                    "TIMER_DATE_END" => $dbProductDiscounts[$boolViewTimer['number']]['ACTIVE_TO']
                ),
                $component
            );
        }
    }
//}
    ?>
    <script>
        if(timerFive) {
            clearTimeout(timerFive);
            var timerFive = setTimeout(window.calcHeight, 300);
        } else {
            var timerFive = setTimeout(window.calcHeight, 300);
        }
    </script>
</div>
