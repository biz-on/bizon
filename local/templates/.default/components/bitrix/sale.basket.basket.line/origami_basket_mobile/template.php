<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
use Sotbit\Origami\Helper\Config;
?>
            <div class="container_menu_mobile__item fonts__small_text">
                <a class="container_menu_mobile__item_link" href="<?=Config::get('COMPARE_PAGE')?>">
                    <span class="mobile_icon_chart-bar">
                        <svg class="" width="14" height="14">
                            <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_compare"></use>
                        </svg>
                    </span>
                    Сравнение 
                    <span class="container_menu_mobile__item_link_col"><?=$arResult["NUM_PRODUCTS_COMPARE"]?></span>
                </a>
            </div>


            <div class="container_menu_mobile__item fonts__small_text">
                <a class="container_menu_mobile__item_link" href="<?=Config::get('BASKET_PAGE')?>">
                    <span class="mobile_icon_heart">
                        <svg class="" width="14" height="14">
                            <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_favourite"></use>
                        </svg>
                    </span>
                    Избранное
                    <span class="container_menu_mobile__item_link_col"><?=$arResult["NUM_PRODUCTS_DELAY"]?></span>
                </a>
            </div>


            <div class="container_menu_mobile__item fonts__small_text">
                <a class="container_menu_mobile__item_link" href="<?=Config::get('BASKET_PAGE')?>">
                    <span class="mobile_icon_shopping-basket">
                        <svg class="" width="14" height="14">
                            <use xlink:href="/local/templates/sotbit_origami/assets/img/sprite.svg#icon_cart"></use>
                        </svg>
                    </span>
                    Корзина
                    <span class="container_menu_mobile__item_link_col"><?=$arResult['NUM_PRODUCTS']?></span>
                </a>
            </div>