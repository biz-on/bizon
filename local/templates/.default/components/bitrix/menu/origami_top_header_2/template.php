<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
$this->setFrameMode(true);
Loc::loadMessages(__FILE__);
$page = $APPLICATION->GetCurPage(false);

// подмена ссылок

$tg_link = "a";
$tg_href = "href";
$tg_click = "";

if ($page != "/") {
    $tg_link = "span";
    $tg_href = "data-href";
    $tg_click = "onclick='dataHref(this);'";
}
?>

<?if(!empty($arResult)):?>

<style>
.dd_catalog_ul {
    padding-left:0;
    list-style:none;
}

.dd_catalog_ul li {
    padding:0;
    margin:0;
}

.dd_catalog_in {
    width:285px;
}
</style>

<nav class="mainmenu">

    <div id="catalog_wrapper">
        <a class="btn show_catalog" style="color:#fff"><svg><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#ico_cat"></use></svg>Каталог</a>
        
        <div id="dd_catalog" style="display: none;">
        <div class="dd_catalog_in">
            <ul class="dd_catalog_ul">
                <li>
                    <a href="/internet-magazin/aksessuary-optom/"><svg class="cat_footw"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#cat_accs"></use></svg>
                    Аксессуары оптом</a>
                </li>
                <li>
                    <a href="/internet-magazin/detskaya-odezhda-optom/"><svg class="cat_kids"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#cat_kids"></use></svg>
                    Детская одежда оптом</a>
                </li>
                <li>
                    <a href="/internet-magazin/zhenskaya-odezhda-optom/"><svg class="cat_woman"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#cat_woman"></use></svg>
                    Женская одежда оптом </a>
                </li>
                <li>
                    <a href="/internet-magazin/muzhskaya-odezhda-optom/"><svg class="cat_man"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#cat_man"></use></svg>
                    Мужская одежда оптом</a>
                </li>
                <li>
                    <a href="/internet-magazin/obuv-optom/"><svg class="cat_footw"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#cat_footw"></use></svg>
                    Обувь оптом</a>
                </li>
            </ul>
        </div>
        </div>
    </div>

            
<?
    $arIco[] = 'ico_status'; 
    $arIco[] = 'ico_request'; 
    $arIco[] = 'ico_code'; 
    $arIco[] = 'ico_photo'; 
?>

    <?foreach($arResult as $i => $item):?>
        <<?=$tg_link?> class="btn bg_gray btn_status" <?=$tg_href?>="<?=$item['LINK']?>" <?=$tg_click?>><svg class="svg_ico_status"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/i/sprite.svg#<?=$arIco[$i]?>"></use></svg> <?=$item['TEXT']?> </<?=$tg_link?>> 
	<?endforeach;?>
<?endif?>

</nav>