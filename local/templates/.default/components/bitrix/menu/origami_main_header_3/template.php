<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$page = $APPLICATION->GetCurPage(false);
?>

<?if(!empty($arResult)):?>

<?endif;?>

<script>
    jQuery(document).ready(function( $ ) {

        $("#menu").mmenu({
            "extensions": [
                "pagedim-black"
            ]
        });

    });

</script>
