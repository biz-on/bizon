<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
$page = $APPLICATION->GetCurPage(false);

// подмена ссылок

$tg_link = "a";
$tg_href = "href";
$tg_click = "";

if ($page != "/") {
    $tg_link = "span";
    $tg_href = "data-href";
    $tg_click = "onclick='dataHref(this);'";
}
?>

<?if(!empty($arResult)):?>
<nav id="topmenu">
    <ul class="erty">
    <?
    $previousLevel = 0;
    foreach($arResult as $arItem):?>
         <li><<?=$tg_link?> <?=$tg_href?>=<?=$arItem['LINK']?> <?=$tg_click?>> <?=$arItem['TEXT']?> </<?=$tg_link?>> </li>
   <?endforeach;?>
   </ul>    
</nav>
<?endif?>
