<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
    die();
}

$this->setFrameMode(true);
$page = $APPLICATION->GetCurPage(false);

// подмена ссылок

$tg_link = "a";
$tg_href = "href";
$tg_click = "";

if ($page != "/") {
    $tg_link = "span";
    $tg_href = "data-href";
    $tg_click = "onclick='dataHref(this);'";
}

if(!empty($arResult)):?>
<style>
.footer-block__item_name_link {
    cursor: pointer;
}

</style>
<ul class="footer-block__item">

<?
if($arParams["MAX_ITEMS"])
{
    $i = 0;
    $maxItems = $arParams["MAX_ITEMS"];
}

foreach($arResult as $arItem)
{
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
        continue;
?>
	<?if($arItem["SELECTED"]):?>
        <li class="footer-block__item_name">
            <span class="footer-block__item_name_link fonts__small_text selected"><?=$arItem["TEXT"]?></span>
        </li>
	<?else:?>
        <li class="footer-block__item_name">
            <? if($arItem["LINK"] != $page): ?>
                <<?=$tg_link?> class="footer-block__item_name_link fonts__small_text" <?=$tg_href?>=<?=$arItem['LINK']?> <?=$tg_click?>><?=$arItem["TEXT"]?></<?=$tg_link?>>
            <? else: ?>
                <span class="footer-block__item_name_link fonts__small_text"><?=$arItem["TEXT"]?></span>
            <? endif ?>
        </li>
	<?endif?>
<?
    if($arParams["MAX_ITEMS"])
    {
        $i++;
        if($i == $maxItems) break;
    }
}
?>

</ul>

<?endif?>
