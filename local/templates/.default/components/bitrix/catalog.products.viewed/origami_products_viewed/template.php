<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Sotbit\Origami\Helper\Config;

$this->setFrameMode(true);
$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
	'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
	'~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	'~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);
$sliderButtons = "";
if (\Sotbit\Origami\Helper\Config::get('SLIDER_BUTTONS') == 'square') {
    $sliderButtons = "btn-slider-main--one";
} else if (\Sotbit\Origami\Helper\Config::get('SLIDER_BUTTONS') == 'circle') {
    $sliderButtons = "btn-slider-main--two";
}
if($arResult['ITEMS'])
{
	?>
    <div class="small-product">
        <p class="small-product__title fonts__middle_title"><?=$arParams["SECTION_NAME"]?></p>



<div class="prods_minislider_wr">
    <div class="prods_mini_slider">   

    <?    
        if($arResult['ITEMS'])
        {
            ?><!-- items-container --><?
            foreach($arResult['ITEMS'] as $item)
            {
                $arPropsRazmer = [];
                $total_qnt_upak = 0;
                foreach ($item["OFFERS"] as $key => $value) {
                    $arCacheRaz = [$value["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'] => $value["PROPERTIES"]["RAZMER_ODEZHDY"]['VALUE']];
                    $arPropsRazmer[] = $arCacheRaz;
                    $total_qnt_upak += $value["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'];
                }

                $table = true;
                if($total_qnt_upak<2) { 
                     $total_qnt_upak = $item["PROPERTIES"]["KOLICHESTVO_V_UPAKOVKE"]['VALUE'];
                     $table = false;
                 }


                if($item['PREVIEW_PICTURE']['SRC']) {   
                        $imgSrc = $item['PREVIEW_PICTURE']['SRC'];
                } else {
                        $imgSrc = $item['OFFERS'][0]['PREVIEW_PICTURE']['SRC'];
                }
                $sezon = $item['PROPERTIES']['SEZON']['VALUE'][0];
                $pcmPrice  = round($item['DISPLAY_PROPERTIES']['MAXIMUM_PRICE']['VALUE']);
                $bPrice  = $pcmPrice * $total_qnt_upak;
                $bPrice = number_format($bPrice, 0, '', ' ');
                $pcmPrice = number_format($pcmPrice, 0, '', ' ');
                   
                ?>

                <div>
                <a class="item" href="<?=$item['DETAIL_PAGE_URL']?>-$<?=$item['ID']?>">
                    <span class="addToFav"><svg><use xlink:href="/local/templates/sotbit_origami/i/sprite.svg#ico_heart"></use></svg></span>
                    
                    <span class="img"><img src="<?=$imgSrc?>" alt="prod1_minislider" ></span>
                    <span class="text_w">
                        <span class="name"><?=$item['NAME']?></span>
                        <span class="cat"><?=$sezon?></span>
                        <span class="price_lbl">Цена</span> 
                        <span class="price_min">за шт. <?=$pcmPrice?> ₽ </span>   
                        <span class="price_max">за набор <?=$bPrice?> ₽ / <?=$total_qnt_upak?> шт.</span>    
                        <span class="rest">
                            <span class="store">Размерный ряд:</span>
                            <?if($table) {?>
                                 <table class="table raz-ryad">
                                    <tr>
                                    <?foreach ($arPropsRazmer as $arRaz) {
                                        foreach ($arRaz as $pcm => $razmer) {
                                        ?>
                                        <td>
                                            <?=$razmer?>
                                        </td>
                                     <?}}?>
                                    </tr>
                                    <tr>
                                     <?foreach ($arPropsRazmer as $arRaz) {
                                        foreach ($arRaz as $pcm => $razmer) {
                                         ?>
                                        <td>
                                            <?=$pcm?>
                                        </td>
                                     <?}}?>
                                    </tr>
                                 </table>
                            <?} else {?>
                                <?=$arPropsRazmer[0][1]?>
                            <?}?>
<!--                             
                            <span class="store_count">10</span>
                            <span class="store_e">упаковок</span> -->
                            <span class="readon">Подробнее</span>
                        </span>
                    </span>
                </a>
                </div>        
        <?}?>
    <?}?> 


    </div>
</div>






    </div>
    <script>
        window.addEventListener('DOMContentLoaded', () => {
            let container = document.querySelector('.small-product');
        });


    </script>
<?
}
?>
