<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CatalogSectionComponent $component
 */

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_DIR . "local/templates/sotbit_origami/assets/plugin/swiper5.2.0/js/swiper.js");
Asset::getInstance()->addJs(SITE_DIR . "local/templates/sotbit_origami/assets/js/custom-slider.js");
Asset::getInstance()->addCss(SITE_DIR . "local/templates/sotbit_origami/assets/plugin/swiper5.2.0/css/swiper.min.css");
Asset::getInstance()->addCss(SITE_DIR . "local/templates/sotbit_origami/assets/css/style-swiper-custom.css");



 $strTitle = $arResult["NAME"]." #".$arResult["ID"]." купите оптом в интернет-магазине";
 $strDesc = $arResult["NAME"]." #".$arResult["ID"]." купите оптом в интернет-магазине Оптовая Миля по цене поставщика с доставкой по Москве и России.";

 $APPLICATION->SetPageProperty('title',  $strTitle);
 $APPLICATION->SetPageProperty('description', $strDesc);

if(Bitrix\Main\Loader::includeModule('sotbit.opengraph')) {
    OpengraphMain::setImageMeta('og:image', $templateData["ITEM"]["JS_OFFERS"][0]["DETAIL_PICTURE"]["SRC"]);
    OpengraphMain::setImageMeta('twitter:image', $templateData["ITEM"]["JS_OFFERS"][0]["DETAIL_PICTURE"]["SRC"]);
    OpengraphMain::setMeta('og:type', 'article');
    OpengraphMain::setMeta('og:title', $arResult["NAME"]);
    OpengraphMain::setMeta('og:description',  $strDesc);
    OpengraphMain::setMeta('twitter:title', $arResult["NAME"]);
    OpengraphMain::setMeta('twitter:description', $strDesc);
}

CHTTP::SetStatus("200");