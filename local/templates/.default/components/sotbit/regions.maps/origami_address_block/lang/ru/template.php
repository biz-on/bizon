<?php
$MESS["ORIGAMI_ADDRESS_TITLE"] = 'Адреса магазинов';
$MESS["ORIGAMI_ADDRESS_CONTACTS_PAGE"] = 'Перейти к контактам';

$MESS["ORIGAMI_ADDRESS_SHOW_LIST"] = 'Списком';
$MESS["ORIGAMI_ADDRESS_SHOW_MAP"] = 'На карте';

$MESS["ORIGAMI_ADDRESS_SEARCH_PLACEHOLDER"] = 'Ваш город';
$MESS["ORIGAMI_ADDRESS_TITLE_CONTACTS_INFO"] = 'Контактная информация';
$MESS["ORIGAMI_ADDRESS_TITLE_SCHEDULE"] = 'Режим работы';

?>