"use strict";

document.addEventListener("DOMContentLoaded", function () {
  var city_list = document.querySelector(".sidebar-address-block-city-list");
  var parent_city = Array.prototype.slice.call(document.querySelectorAll(".sidebar-address-block-city-list__js-show-child"));
  var child_item = Array.prototype.slice.call(document.querySelectorAll(".sidebar-address-block-city-list__js-show-popup"));
  var popup_wrap = document.querySelector(".sidebar-address-block__popup-wrap");
  var tab_item = Array.prototype.slice.call(document.querySelectorAll(".address-block-wrap__tab-item"));
  var tab_content = Array.prototype.slice.call(document.querySelectorAll(".address-block__tab-content"));
  var search = document.querySelector(".sidebar-address-block__search");
  var city_item = Array.prototype.slice.call(document.querySelectorAll(".sidebar-address-block-city-list__item-wrap"));
  var reset_search = document.querySelector(".sidebar-address-block__cancel-icon-wrap");
  var btn_open_map = Array.prototype.slice.call(document.querySelectorAll(".popup-info-item-address-block__btn-open-map"));
  var event_input = new Event("input");
  var search_string;
  search.addEventListener("input", function () {
    search_string = new RegExp(search.value, "i");
    city_item.forEach(function (elem) {
      var name_city = elem.querySelector(".sidebar-address-block-city-list__name-city").textContent;
      var child_block = elem.querySelector(".sidebar-address-block-city-list__sub-items-block");

      if (child_block) {
        child_block.style.height = 0;
      }

      elem.classList.remove("opened");

      if (search_string.test(name_city)) {
        elem.style.display = "block";
      } else {
        elem.style.display = "none";
      }
    });
  });
  reset_search.addEventListener("mousedown", function (e) {
    search.value = "";
    search.dispatchEvent(event_input);
  });
  window.addEventListener("resize", function () {
    parent_city.forEach(function (el) {
      var child_block = el.querySelector(".sidebar-address-block-city-list__sub-items-block");
      el.classList.remove("opened");
      child_block.style.height = 0;
    });
  });
  btn_open_map.forEach(function (el) {
    el.addEventListener("click", function () {
      tab_content[0].classList.remove("active");
      tab_content[1].classList.add("active");
      tab_item[0].classList.remove("active");
      tab_item[1].classList.add("active");
    });
  });
  tab_item.forEach(function (el, numEl) {
    el.addEventListener("click", function () {
      tab_item.forEach(function (elem, num) {
        elem.classList.remove("active");
        tab_content[num].classList.remove("active");
      });
      el.classList.add("active");
      tab_content[numEl].classList.add("active");
    });
  });
  parent_city.forEach(function (el) {
    var title_city = el.querySelector(".sidebar-address-block-city-list__item");
    var child_block = el.querySelector(".sidebar-address-block-city-list__sub-items-block");
    var child_list = el.querySelector(".sidebar-address-block-city-list__sub-items-list");
    title_city.addEventListener("click", function () {
      el.classList.toggle("opened");

      if (el.classList.contains("opened")) {
        child_block.style.height = child_list.offsetHeight + "px";
      } else {
        child_block.style.height = 0;
      }
    });
  });
  child_item.forEach(function (el) {
    var popup_window = document.querySelector(".popup-info-item-address-block[data-popup=\"".concat(el.getAttribute("data-popup"), "\"]"));
    var close_popup = popup_window.querySelector(".popup-info-item-address-block__close-wrap");
    el.addEventListener("click", function () {
      popup_wrap.style.display = "flex";
      popup_window.classList.add("showed");
    });
    close_popup.addEventListener("click", function () {
      popup_wrap.style.display = "none";
      popup_window.classList.remove("showed");
    });
  });
  new PerfectScrollbar(city_list, {
    wheelSpeed: 0.5,
    wheelPropagation: true,
    minScrollbarLength: 20,
    typeContainer: 'li'
  });
  new PerfectScrollbar(popup_wrap, {
    wheelSpeed: 0.5,
    wheelPropagation: true,
    minScrollbarLength: 20
  });
  popup_wrap.addEventListener("wheel", function (e) {
    e.preventDefault();
  });
});

function addEventOnMarker(marker) {
  marker.forEach(function (el) {
    var item_mark = document.querySelector("[data-marker=\"".concat(el.geometry.getCoordinates()[0] + el.geometry.getCoordinates()[1], "\"]"));

    if (item_mark) {
      item_mark.addEventListener("click", function () {
        el.balloon.open();
      });
    }
  });
}

function addEventOnMarkerGoogle(marker) {
  marker.forEach(function (el) {
    var item_mark = document.querySelector("[data-marker=\"".concat(el.getPosition().lat()).concat(el.getPosition().lng(), "\"]"));

    if (item_mark) {
      item_mark.addEventListener("click", function () {
        el.infowin.open(el.map, el);
      });
    }
  });
}