<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;

$templateFolder = $this->GetFolder();

Asset::getInstance()->addJs($templateFolder . "/js/jquery.min.js");
Asset::getInstance()->addJs($templateFolder . "/js/jquery.maskedinput.min.js");
Asset::getInstance()->addJs($templateFolder . "/js/vue.js");

Asset::getInstance()->addCss("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css");

