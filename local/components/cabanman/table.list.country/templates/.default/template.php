<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
use Bitrix\Main\Localization\Loc;
?>
<div class="container" id="infoCountry">
    <div class="row">
        <div class="col-md-10">
            <table class="table">
                <thead>
                    <tr>
                        <th>Позиция</th>
                        <th>Страна</th>
                        <th>Уровень жизни</th>
                        <th>Безопасность</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="item in items">
                        <template v-if="item.status!=='edit'">
                            <td v-on:click="editItem(item, $index, $event)">{{item.position}}</td>
                            <td v-on:click="editItem(item, $index, $event)">{{item.name}}</td>
                            <td v-on:click="editItem(item, $index, $event)">{{item.levelLife}}</td>
                            <td v-on:click="editItem(item, $index, $event)">{{item.safe}}</td>

                            <td>
                                <a v-on:click="editItem(item, $index, $event)" href="#" title="редактировать">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                            </td>
                        </template>

                        <template v-if="item.status=='edit'">
                            <td><input class="form-control" v-model="edit.position"></td>
                            <td><input class="form-control" v-model="edit.name"></td>
                            <td><input class="form-control" v-model="edit.levelLife"></td>
                            <td><input class="form-control" v-model="edit.safe"></td>

                            <td>
                                <a v-on:click="successEdit(item, $index, $event)" href="#" title="Применить">
                                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                </a>
                                <a v-on:click="cancelEdit(item, $index, $event)" href="#" title="Отмена">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </a>
                            </td>
                        </template>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
    <div>
        <button @click="prevPage">
            Назад
        </button>
        <button @click="nextPage">
            Вперед
        </button>
    </div>
</div>
