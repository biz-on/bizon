<script>
var infoCountry = new Vue({
    el:'#infoCountry',
    data: {
        edit: {},
        editIndex: undefined,
        items: [],
        pageIndex: 0,
        pageIndexMax: <?=$arResult["ALL_COUNT"]?> - 5 
    },
    ready:function(){
        var items = <?=$arResult['ITEMS_STR']?>
        this.setDefaultItems(items);
    },
    watch:{
        edit: function(){
            $('.date').mask("99.99.9999");
        }
    },
    methods: {
        setDefaultItems: function (items){ 

            this.$set('items', items);
        },
        editItem: function(item, index, e){
            for(var i = 0, len = this.items.length; i < len ; i++) {
                this.items[i].status = '';
            }

            this.$data.edit = Vue.util.extend({}, item);
            this.$data.editIndex = index;

            item.status = 'edit';
        },

        successEdit: function (item, index, e) {
            this.$data.items.$set(index, this.$data.edit);
            params = { "typeAction" : "update", 
                       "data"       :  this.$data.edit 
            };

            this.getAjax(params);
        },

        cancelEdit: function (item, index, e) {
            item.status = '';
        },
        prevPage: function() {
            this.pageIndex = this.pageIndex - 5;
            if (this.pageIndex < 0 ) this.pageIndex = 0;


            params = { "typeAction" : "paginator", 
                       "offset"     :  this.pageIndex 
                     };

            this.getAjax(params); 
        },
        nextPage: function() {
            if (this.pageIndex < this.pageIndexMax) {
                this.pageIndex = this.pageIndex + 5;

                params = { "typeAction" : "paginator", 
                           "offset"     :  this.pageIndex 
                         };

                this.getAjax(params); 
            }   
        },

        getAjax: function(arParams) {

            var query = {
                 action: 'cabanman:table.api.actions.example',
            };
             
            var data = {
                params : arParams,
                SITE_ID: 's1',
                //sessid: BX.message('bitrix_sessid')
            };
             
            var request = $.ajax({
                url: '/bitrix/services/main/ajax.php?' + $.param(query),
                method: 'POST',
                data: data
            });
             
            var t = this;
             
            request.done(function (response) {
                if (response.data.json != '') 
                 json = JSON.parse(response.data.json);
                t.setDefaultItems(json);
            })
        
        },    


    }
});
</script>