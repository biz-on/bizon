<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('cabanman.table');

$fields = [ 
            'select' => array('*'),
            'order'  => array('POSITION'),
            'limit'  => 5,
            'offset' => 0
          ];

$res = Cabanman\Table\Test::get($fields);

foreach ($res as $key => $value) {

	$id        = $value['ID'];
	$position  = $value['POSITION'];
	$name      = $value['NAME'];
	$levelLife = $value['LEVEL'];
	$safe      = $value['SAFE'];

	$str .= "{id:'$id', position: '$position', name:'$name', levelLife:'$levelLife', safe:'$safe', status:''},";
}

$arResult['ITEMS_STR'] = '['.$str.'];'; 

// Вычисляем сколько строк в таблице

$arResult["ALL_COUNT"] = Cabanman\Table\Test::count()["COUNT(*)"];

$this->includeComponentTemplate($componentPage);


