<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
// åñëè íå óñòàíîâëåí ìîäóëü
if (!CModule::IncludeModule("streamtelecom.sms"))
{
	ShowError(GetMessage("SMSC_SMS_MODULE_NOT_INSTALLED"));
	return;
}

// âõîäÿùèå ïàðàìåòðû
$arParams["PHONE_NUMBER"]	= isset($_REQUEST["PHONE_NUMBER"]) ? trim($_REQUEST["PHONE_NUMBER"]) : '';

$arParams["MIN_PASS"]		= intval($arParams["MIN_PASS"]);
$arParams["MAX_PASS"]		= intval($arParams["MAX_PASS"]);
$arParams["SMS"]		= trim($arParams["SMS"]);
$arParams["TIME"]		= intval($arParams["TIME"])*60;
$arParams['CODE']		= (isset($_REQUEST['CODE']))?intval($_REQUEST['CODE']):0;
$arParams['BACKURL']		= (isset($_REQUEST['backurl']) && strlen(urldecode($_REQUEST['backurl']))>0)?urldecode($_REQUEST['backurl']):$APPLICATION->GetCurPage();
$arParams['AUTH']		= CUser::IsAuthorized();
$arParams['USER']		= $GLOBALS['USER']->GetID();
$arParams['EMAIL']		= (check_email(trim($arParams['EMAIL'])))?trim($arParams['EMAIL']):'';
$arParams['FORGOT']		= (isset($_POST['FORGOT']))?true:false;

$arResult['VALIDATION'] = array(
	'VALID'		=> false,
	'CODE'		=> (isset($_SESSION['SMSC_VALIDATION']['CODE']) && strlen($_SESSION['SMSC_VALIDATION']['CODE'])>0)?$_SESSION['SMSC_VALIDATION']['CODE']:'',
	'PHONE'		=> (isset($_SESSION['SMSC_VALIDATION']['PHONE']) && strlen($_SESSION['SMSC_VALIDATION']['PHONE'])>0)?$_SESSION['SMSC_VALIDATION']['PHONE']:'',
	'USER_GOT'	=> false,
	'SHOW_PASS'	=> false,
	'USER'		=> array()
);

if($arParams['FORGOT']) {
	$code1 = rand(0,9);$code2 = rand(0,9);$code3 = rand(0,9);$code4 = rand(0,9);
	$code = (string) $code1.$code2.$code3.$code4; 
	$arResult['VALIDATION']['CODE'] = $code;
	$arResult['VALIDATION']['PHONE'] = $arParams["PHONE_NUMBER"];
	$arParams["PHONE_NUMBER"] = preg_replace("/[^0-9]/", '', $arParams["PHONE_NUMBER"]);
	$arResult['VALIDATION']['SHOW_PASS'] = true;
	$arResult['VALIDATION']['FORGOT'] = true;
	// $message = str_replace('#CODE#',$code,$arParams["SMS"]);
	// $sms = new SMSC_Send;
	// $sms->Send_SMS($arParams["PHONE_NUMBER"], "Ваш код авторизации: ".$code, 0, 0, 0, "optmilya", $encoding = LANG_CHARSET);
	
	$obSmsServ = new \StreamTelecom\Main();
	$userPhone = $arParams["PHONE_NUMBER"];
	$obSmsServ->send(array($userPhone), "Ваш код авторизации: ".$code);
	
	$arResult["CODE_VALID"] = true;
} 

if($arParams['CODE'] != 0) {
   if($arParams['CODE'] == $arResult['VALIDATION']['CODE']) {
   // && $arResult['VALIDATION']['PHONE'] == $arParams["PHONE_NUMBER"]) {
	$arResult['VALIDATION']['VALID'] = true;


// Проверка логина
	$filter = Array
	(
		"LOGIN"	=> $arParams["PHONE_NUMBER"],
	);

	$rsUsers = CUser::GetList(($by="login"), ($order="desc"), $filter);
	if($arUser = $rsUsers->Fetch()) {

		if($arUser['LOGIN'] == $arParams["PHONE_NUMBER"]) {
		//Update
			$arResult['VALIDATION']['USER_GOT'] = true;
			$arResult['VALIDATION']['SHOW_PASS'] = true;
			$arResult['VALIDATION']['USER'] = $arUser;
				$user = new CUser;
				$user->Update($arUser['ID'], array('PASSWORD'=>$arParams['CODE'],'CONFIRM_PASSWORD'=>$arParams['CODE']));
				if($user->Authorize($arUser['ID']) && $arParams['BACKURL']) LocalRedirect($arParams['BACKURL']);
			} 	
	} else {

		//Регистрация пользователя
			 $user = new CUser;
			 $randCode = randString(8); // генерируем случайную строку 
			 $arFields = array(
			   "LOGIN"             => $arParams["PHONE_NUMBER"],
			   "GROUP_ID"          => 17,
			   "PASSWORD"          => "00".$arParams['CODE'],
			   "CONFIRM_PASSWORD"  => "00".$arParams['CODE'],
			   "CONFIRM_CODE"      => $randCode
			 );

		 	$ID = $user->Add($arFields);

		    if(intval($ID) > 0){

		        if($USER->Authorize($ID)){
		        	LocalRedirect($arParams['BACKURL']);
		        }
		     }
	}

	} else {
		// Неправильный код
		$arResult["CODE_VALID"] = true;
		$arResult["ERROR_CODE"] = true;
		$arResult['VALIDATION']['SHOW_PASS'] = true;
		$arResult['VALIDATION']['FORGOT'] = true;
	}
}

// if(SMSC_Send::CheckPhoneNumber($arParams["PHONE_NUMBER"])) {
// 	echo "OK";
	// $filter = Array
	// (
	// 	"PERSONAL_MOBILE"	=> $arParams["PHONE_NUMBER"],
	// );
	// $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $filter); // âûáèðàåì ïîëüçîâàòåëåé
	// if($arUser = $rsUsers->Fetch()) {
	// 	if($arUser['PERSONAL_MOBILE'] == $arParams["PHONE_NUMBER"]) {
	// 		$arResult['VALIDATION']['USER_GOT'] = true;
	// 		$arResult['VALIDATION']['SHOW_PASS'] = true;
	// 		$arResult['VALIDATION']['USER'] = $arUser;
	// 		if($arResult['VALIDATION']['VALID']) {
	// 			$user = new CUser;
	// 			$user->Update($arUser['ID'], array('PASSWORD'=>$arParams['CODE'],'CONFIRM_PASSWORD'=>$arParams['CODE']));
	// 			if($user->Authorize($arUser['ID']) && $arParams['BACKURL']) LocalRedirect($arParams['BACKURL']);
	// 		}
	// 	} 
	// } elseif($arParams['EMAIL']) {
	// 	$code = rand($arParams["MIN_PASS"],$arParams["MAX_PASS"]);
	// 	$arResult['VALIDATION']['CODE'] = $code;
	// 	$arResult['VALIDATION']['PHONE'] = $arParams["PHONE_NUMBER"];
	// 	$message = str_replace('#CODE#',$code,$arParams["SMS"]);
	// 	$sms = new SMSC_Send;
	// 	$sms->Send_SMS($arParams["PHONE_NUMBER"],$message);

	// 	$user = new CUser;
	// 	$arFields = Array(
	// 		"LOGIN"             	=> $arParams["PHONE_NUMBER"],
	// 		"LID"               	=> SITE_ID,
	// 		"ACTIVE"            	=> "Y",
	// 		"PASSWORD"          	=> $code,
	// 		"CONFIRM_PASSWORD"  	=> $code,
	// 		"PERSONAL_MOBILE"    	=> $arParams["PHONE_NUMBER"],
	// 		"EMAIL"			=> $arParams["PHONE_NUMBER"].'@gmail.com'
	// 	);
		
	// 	$ID = $user->Add($arFields);
	// 	if(intval($ID) > 0) {
	// 		$user->Update($ID, array('EMAIL'=>$arParams['EMAIL']));
	// 	}
	// 	$arResult['VALIDATION']['SHOW_PASS'] = true;
	// }
//}

if ($this->StartResultCache())
{
	if($arParams['AUTH']) {
		$rsUser = CUser::GetByID($arParams['USER']);
		$arUser = $rsUser->Fetch();
		$arResult['VALIDATION']['USER'] = $arUser;
	} else {
		$this->AbortResultCache();
	}
	$this->IncludeComponentTemplate();
}

// ïîäêëþ÷àåì øàáëîí

$_SESSION['SMSC_VALIDATION'] = $arResult['VALIDATION'];
?>