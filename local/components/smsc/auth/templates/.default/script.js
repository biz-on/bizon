function authSmscOpen() {
	$("#p_auth").fadeIn(200);
   $(".popup_layout").fadeIn(200);
}

function authSmscClose() {
	$("#p_auth").fadeOut(200);
   $(".popup_layout").fadeOut(200);
}

var mainCode = [];

function authCodeAdd(i) {
  // name="CODE"
  var ind = $(i).attr("code-data");
  mainCode[ind] = $(i).val();
  var strMainCode = mainCode.join('');
  $('#maincode').attr("value", strMainCode);
}

function submitAuthForm(e) {
            e.preventDefault();
            var phNumber = $('input[name="PHONE_NUMBER"]').val();
            var forgot   = $('input[name="FORGOT"]').val();
            var form_data = $('#auth-form').serialize(); //собераем все данные из формы

            if (phNumber.length == null) phNumber = 'ppp';
               console.log(phNumber.length);

            if (phNumber.length < 10 )  {
              alert("Неверно указан номер телефона!");
              return ;
            }

            $.ajax({
            type: "POST", //Метод отправки
            url: "/local/components/smsc/auth/ajax.php", //путь до php фаила отправителя
            data: form_data,
            success: function(d) {
                   //код в этом блоке выполняется при успешной отправке сообщения
                  $('#p_auth').html(d);
               }
            });
}

