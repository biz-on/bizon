<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<? if(!$arParams['AUTH']):?>
	<? $uri = str_replace('forgot','',urldecode(POST_FORM_ACTION_URI));?>

<?if(!$arResult['CODE_VALID']) {
	$headerForm = 'Войдите или зарегистрируйтесь, чтобы продолжить';
	$footerFrom = 'Получить код';
} else {
	$headerForm = 'Введите код';
	$footerFrom = 'Войти';
}
?>

    <style>
        #auth-form a {
            color:blue !important;
  /*          font-size: 12px;*/
            text-decoration: underline !important;
        }
        .auth-error {
            color:red !important;
            font-size: 10px;
        }
    </style>
  <div class="popup_in ">
    <div class="close_popup" onclick="authSmscClose();"></div>
    <p class="ttl"><?=$headerForm?></p>
	<form id="auth-form" name="smsvalid" enctype="application/x-www-form-urlencoded" onsubmit="submitAuthForm(event);return false;">
		<div class="aBlock">
<!-- 			<img class="auth-close" src="/local/components/smsc/auth/templates/.default/i/close.svg" onclick="authSmscClose();" title="Закрыть"/> -->
			
			<?if(!$arResult['CODE_VALID']):?> 
				<input type="text" class="text_input bor_gray phonemask" placeholder="+7" name="PHONE_NUMBER" value="<?=$arParams["PHONE_NUMBER"]?>"><br/>
				<input type="hidden" name="FORGOT" value="true">
			<?endif?>

			<? if($arResult['VALIDATION']['SHOW_PASS'] && $arResult['VALIDATION']['USER_GOT'] && !$arParams['FORGOT']):?>
				<div class="input passwd">
					<label><?=GetMessage("SMSC_SMS_VVEDITE_PAROLQ")?></label>
				</div>
				<input type="text" class="text" name="CODE"><br/>

			<? elseif($arResult['VALIDATION']['SHOW_PASS']):?>

			<div id="codeinputs" class=""><!-- можно error указать в #codeinputs через class=error -->
				<!-- можно error указать в инпутах через class=error -->
				 <input type="text" placeholder="0" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" code-data="1" onkeyup="authCodeAdd(this)"/>
				 <input type="text" placeholder="0" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" code-data="2" onkeyup="authCodeAdd(this)"/>
				 <input type="text" placeholder="0" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" code-data="3" onkeyup="authCodeAdd(this)"/>
				 <input type="text" placeholder="0" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" code-data="4" onkeyup="authCodeAdd(this)"/>
			</div>
				
			 <p class="info">Мы отравили код авторизации на номер <?=$arParams["PHONE_NUMBER"]?> <a class="open_popup" data-popupid="p_auth">изменить</a></p>   

                <p class="newcodetime" >Получить новый код можно через <span id="auth-timer">60</span> </p>

                <script>
                    var tickCounter = 0;
                    function tick() {
                        tickCounter++;
                        tickResult = 60 - tickCounter;
                        if (tickResult > 1) {
                            $('#auth-timer').html(tickResult);
                        } else {
                            $('#header-auth-timer').html("<a href='#'>Получить</a> новый код");
                        }
                    }

                    let timerId = setInterval(tick, 1000);
                </script>

                <div class="mychbx orange"><label for="yes1">Соглашаюсь с <a href="/help/oferta/">условиями</a> продажи товара и <a href="/help/confidentiality/">обработки</a> персональных данных</label><input type="checkbox" id="yes1"></div>

                <?if ($arResult["ERROR_CODE"]):?>
                <p class = "error">
                    Вы ввели неверный код подтверждения,<br> попробуйте ещё раз
                </p>    
                
                <?endif;?>
				<input type="hidden" name="CODE" id="maincode">
				<input type="hidden" name="PHONE_NUMBER" value="<?=$arParams["PHONE_NUMBER"]?>">
				
			<? endif;?>

			<? if($arResult['VALIDATION']['USER_GOT']):?>
				<? $forgot = (strpos(POST_FORM_ACTION_URI,'?') === false)?POST_FORM_ACTION_URI.'?forgot&PHONE_NUMBER='.$arParams["PHONE_NUMBER"]:POST_FORM_ACTION_URI.'&forgot&PHONE_NUMBER='.$arParams["PHONE_NUMBER"];?>
				<p><a href="<?=$forgot?>"><?=GetMessage("SMSC_SMS_ZABYLI_SVOY_PAROLQ")?></a></p>
			<? endif;?>

				<input type="submit" name="save" class="btn bg_orange open_popup" value="<?=$footerFrom?>">

				<p class="nosms"><a href="#">Не приходит смс?</a></p>
				<div>
					<span class="web web2"><?=$arParams['CHECK']?></span>
				</div>
		</div>
	</form>
	</div>

<script>
    $(document).ready(function() {
		$(".mychbx label:not('.disabled')").click( function() {
			$(this).toggleClass("active");
		});
		$("div.radios label").click(function(){
		    $(this).closest("div.radios").find("label").removeClass('active');
		    $(this).addClass('active');
		});


			// ввод кода из смс
		var codebody = $('#codeinputs');
		function goToNextInput(e) {
			var key = e.which,t = $(e.target), sib = t.next('input');
			if (key != 9 && (key < 48 || key > 57)) {e.preventDefault(); return false;}
			if (key === 9) {return true;}
			if (!sib || !sib.length) {}
			sib.select().focus();
		}
		function onKeyDown(e) {
			var key = e.which; if (key === 9 || (key >= 48 && key <= 57)) { return true;} e.preventDefault();return false;
		}
		function onFocus(e) {$(e.target).select();}
		codebody.on('keyup', 'input', goToNextInput);
		codebody.on('keydown', 'input', onKeyDown);
		codebody.on('click', 'input', onFocus);
		//-----------------

   });
</script>
<? else:?>
<?//print_r($arResult['VALIDATION']['USER']['LOGIN']);?>
  <div class="popup_in ">
    <div class="close_popup" onclick="authSmscClose();"></div>
    <p class="ttl">Добро пожаловать в маркетплейс "Оптовая миля"!</p>
    <a href="/internet-magazin/">Перейти в каталог"</a>
 </div>


<script>
    $(document).ready(function() {
        $("#auth-title").html("<?=$USER->GetLogin();?>");
        $(".btn_login").attr("onclick","window.location.href='/personal/'");
   });
</script>

<? endif;?>
