<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
if (!CModule::IncludeModule("smsc.sms")) return;


$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"PHONE_NUMBER" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("PHONE_NUMBER"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"MIN_PASS" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MIN_PASS"),
			"TYPE" => "STRING",
			"DEFAULT" => "10000",
		),
		"MAX_PASS" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("MAX_PASS"),
			"TYPE" => "STRING",
			"DEFAULT" => "999999",
		),
		"SMS" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("SMS"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("SMSC_SMS_PROVEROCNYY_KOD")." #CODE#",
		),
		"TIME" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("TIME"),
			"TYPE" => "STRING",
			"DEFAULT" => "3",
		),
		"CHECK" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CHECK"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("SMSC_SMS_PODTVERDITQ"),
		),
		"REPEAT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("REPEAT"),
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("SMSC_SMS_POVTORITQ"),
		),
		"AJAX_MODE" => array(),
		"CACHE_TIME"  =>  Array("DEFAULT" => 3600),
	),
);
?>