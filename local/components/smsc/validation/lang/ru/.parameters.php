<?
$MESS["NO_SELECTED"] = "(не выбрано)";
$MESS["PHONE_NUMBER"] = "Номер телефона";
$MESS["MIN_PASS"] = "Минимальное число для кода";
$MESS["MAX_PASS"] = "Максимальное число для кода";
$MESS["SMS"] = "Текст сообщения(шаблон #CODE# - код верификации)";
$MESS["TIME"] = "Время допустимого повтора в минутах";
$MESS["CHECK"] = "Кнопка подтверждения";
$MESS["REPEAT"] = "Кнопка повтора";
$MESS["SMSC_SMS_PROVEROCNYY_KOD"] = "Проверочный код";
$MESS["SMSC_SMS_PODTVERDITQ"] = "Подтвердить";
$MESS["SMSC_SMS_POVTORITQ"] = "Повторить";
?>