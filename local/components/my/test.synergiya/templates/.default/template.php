<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

<?
function vardump($input)
{
  if(!$input){return false;}
      if(gettype($input)=="boolean")
      {
          echo var_dump($input);
      }
      else
      {
          echo "<pre>".print_r($input,true)."</pre>";
      }
}
?>

<div>
    <?vardump($arResult)?>
</div>
