<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TEST"),
	"DESCRIPTION" => GetMessage("TEST_ORM"),
	"ICON" => "",
	"PATH" => array(
        "ID" => "cabanman",
        "NAME" => GetMessage("CABANMAN_COMPONENTS_TITLE")
	),
);

?>