<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


\Bitrix\Main\Loader::includeModule('iblock');

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

CModule::IncludeModule('highloadblock');

function GetEntityDataClass($HlBlockId) 
{
    if (empty($HlBlockId) || $HlBlockId < 1)
    {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}



/// Выводим несколько елементов инфоблока с несколькими свойстами через d7 ORM
///
///

//echo \Bitrix\Iblock\Iblock::wakeUp(25)->getEntityDataClass(); //25 - ID инфоблока каталога 

const PREFIX = 'IBLOCK_ELEMENTS_ELEMENT_CATALOG_';

$products = \Bitrix\Iblock\Elements\ElementCatalogTable::getList([
    'select' => [
                 'ID', 
                 'NAME', 
                 'NAZVANIE_PREDMETA.ITEM',
                 'VID.ITEM',
                 'POL.ITEM',
                 'SEZON.ITEM',
                 'MORE_PHOTO.FILE',
                ],
    'filter' => [
                 '=ID' => $arParams["ID_ELEMENTS"], //[30270, 30272, 29129],
                 '=ACTIVE' => 'Y'
                ],
])->fetchAll();




foreach ($products as $product) {

    $arCatalogElement[$product['ID']]["ID"]   = $product['ID'];
    $arCatalogElement[$product['ID']]["NAME"] = $product['NAME'];

    ///Свойства множественные, типа файл, типа список   
    $arCatalogElement[$product['ID']]["MORE_PHOTO"][$product[PREFIX.'MORE_PHOTO_FILE_EXTERNAL_ID']] = $product[PREFIX.'MORE_PHOTO_FILE_SUBDIR'].$product[PREFIX.'MORE_PHOTO_FILE_FILE_NAME']; 
    $arCatalogElement[$product['ID']]["VID"][$product[PREFIX.'VID_ITEM_XML_ID']] = $product[PREFIX.'VID_ITEM_VALUE'];
    $arCatalogElement[$product['ID']]["SEZON"][$product[PREFIX.'SEZON_ITEM_XML_ID']] = $product[PREFIX.'SEZON_ITEM_VALUE'];

}

// выводим результат
 $arResult["ELEMENTS"] = $arCatalogElement;


/// **************************************************************



/// Выводим 10 разделов из инфоблока 
///
///


$query = new Bitrix\Main\Entity\Query(

    Bitrix\Iblock\SectionTable::getEntity()

);


$query->setSelect(array('ID', 'CODE', 'NAME'))
      ->setFilter(array('IBLOCK_ID' => $arParams["IBLOCK_ID"]))
      ->setOrder(array('ID' => 'ASC'))
      ->setLimit(10);

// echo $query->getQuery();


$result = $query->exec();

// выводим результат
while ($row = $result->fetch()) {
    $arResult["SECTIONS"][] = $row;
}



/// **************************************************************



/// Выводим данные из HL блока 
///
///


$entity_data_class = GetEntityDataClass($arParams["HL_ID"]); // 9 id HL block

$arFilter = Array(
   Array(
      "LOGIC"=>"OR",
      Array(
         "UF_NAME"=>'5%'
      ),
      Array(
         "UF_NAME"=>'6%'
      )
   )
);

$rsData = $entity_data_class::getList(array(
   'select' => array('UF_NAME'),
   'filter' => $arFilter
));

while($el = $rsData->fetch()){
    // выводим результат
    $arResult["HL_BLOCKS"][] = $el;
}


$this->IncludeComponentTemplate();

?>