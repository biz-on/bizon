<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Нужны качественные фотографии для интернет-магазина? Фотостудия Moscontent оказывает услуги по фотосъемке товаров для интернет-магазина по выгодной цене.");
$APPLICATION->SetPageProperty("title", "Услуги фотостудии для интернет-магазина и маркетплейса");
$APPLICATION->SetTitle("Фотостудия Moscontent");
?>

	<div class="content">
		
		<div class="welcome photostd cl">
			<img src="<?=SITE_TEMPLATE_PATH?>/i/photostudio.png" alt="photostd"  >
			
			<h1><strong>Качественная фотография,</strong> быстрее продаёт</h1>
			<p class="txt">В нашей фотостудии профессиональные фотографы отснимут ваши товары с лучших ракурсов.</p>
		</div>
		

		<div class="photostd_info">
			<p class="ttl"><strong>Наша фотостудия Moscontent</strong> находится на территории ТЯК Москва и предоставляет услуги предметных фотосессий товаров.</p>
			<p class="txt">Для размещения в интернет-магазине всем поставщикам необходимо иметь качественные уникальные фотографии своих товаров, поэтому мы решили упростить процесс размещения их товаров на маркетплейсе <a href="/"><strong>optovayamilya.ru</strong></a> и составили специальное предложение со скидкой от нашего Шоурума. </p>

		</div>
		
		<div class="photostd_details">
			<div class="flex">
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/photostudio_p1.svg" alt="photostd_p1" ></div>
					Предметное фото от 100 руб./фото
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/photostudio_p2.svg" alt="photostd_p2" ></div>
					Модельное фото (фото одежды на модели) - 200 руб./фото + оплата работы модели от 2000 руб/час либо прислать свою модель
				</div>
				<div class="col">
					<div class="img"><img src="<?=SITE_TEMPLATE_PATH?>/i/photostudio_p3.svg" alt="photostd_p3" ></div>
					Фото на манекене - 250 руб./фото (маникены от нас)
				</div>
			</div>
			<p class="small">*Цены действительны только для товаров поставщиков, представленных в шоурум «Оптовая Миля»</p>
		</div>

<?
use Sotbit\Origami\Helper\Config;
global $APPLICATION, $settings;

CModule::IncludeModule('form');
$FORM_SID = "LIKEOFFER";
$rsForm = CForm::GetBySID($FORM_SID);
$arForm = $rsForm->Fetch();
$APPLICATION->IncludeComponent(
    "bitrix:form.result.new",
    "sotbit_webform_1",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_URL" => "",
        "AJAX_MODE" => 'Y',
        "AJAX_OPTION_ADDITIONAL" => \Bitrix\Main\Security\Random::getString(3),
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "VARIABLE_ALIASES" => array(
            "RESULT_ID" => "RESULT_ID",
            "WEB_FORM_ID" => "WEB_FORM_ID"
        ),
        "WEB_FORM_ID" => (isset($settings['fields']['webform_id']['value'])) ? $settings['fields']['webform_id']['value'] : $arForm['ID']
    )
);
?>	

</div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>